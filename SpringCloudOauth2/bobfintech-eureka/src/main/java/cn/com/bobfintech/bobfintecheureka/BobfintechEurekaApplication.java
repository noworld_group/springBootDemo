package cn.com.bobfintech.bobfintecheureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class BobfintechEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(BobfintechEurekaApplication.class, args);
    }

}
