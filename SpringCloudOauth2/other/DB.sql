/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50729
Source Host           : localhost:3306
Source Database       : mysqltest

Target Server Type    : MYSQL
Target Server Version : 50729
File Encoding         : 65001

Date: 2020-07-10 18:07:55
*/


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `bob_auth_info`
-- ----------------------------
DROP TABLE IF EXISTS `bob_auth_info`;
CREATE TABLE `bob_auth_info` (
  `ID` varchar(32) NOT NULL COMMENT '菜单ID,UUID生成',
  `RESOURCE_NAME` varchar(32) NOT NULL COMMENT '资源名称',
  `RESOURCE_LCON` varchar(32) NOT NULL COMMENT '资源图标',
  `RESOURCE_TYPE` varchar(50) NOT NULL COMMENT '资源类型',
  `ACCESS_PATH` varchar(1024) DEFAULT NULL COMMENT '访问路径',
  `PARENT_ID` varchar(32) DEFAULT NULL COMMENT '父id',
  `ORDER_NUM` int(4) NOT NULL COMMENT '排序字段',
  `ISDISPLAY` int(4) NOT NULL COMMENT '是否显示',
  `AUTH_DESCRIPTION` varchar(1024) DEFAULT NULL COMMENT '权限描述',
  `JUMP_PAGE` varchar(32) DEFAULT NULL COMMENT '跳转页面名称',
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bob_auth_info
-- ----------------------------
INSERT INTO `bob_auth_info` VALUES ('0001', 'test_btn', 'ico-primary', '1', '/testPath/hello', null, '1', '1', '测试按钮权限，针对guopf用户', '/path/aoth', '2020-07-07 17:00:40', '0000-00-00 00:00:00');
INSERT INTO `bob_auth_info` VALUES ('0002', 'test_btn', 'ico-warning', '1', '/api/form', null, '2', '1', '测试普通管理员权限,admin', '/path/march', '2020-07-07 17:01:54', '0000-00-00 00:00:00');
INSERT INTO `bob_auth_info` VALUES ('0003', '测试uri', '/api/from', '/api/from', '/api/from', null, '1', '1', '测试访问权限', null, '2020-07-10 17:58:31', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `bob_role_auth_relation`
-- ----------------------------
DROP TABLE IF EXISTS `bob_role_auth_relation`;
CREATE TABLE `bob_role_auth_relation` (
  `ID` varchar(32) NOT NULL COMMENT 'ID,UUID生成',
  `ROLE_ID` varchar(32) DEFAULT NULL COMMENT '角色id',
  `AUTH_ID` varchar(32) DEFAULT NULL COMMENT '权限id',
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bob_role_auth_relation
-- ----------------------------
INSERT INTO `bob_role_auth_relation` VALUES ('0001', '0001', '0001', '2020-07-07 17:03:07', '0000-00-00 00:00:00');
INSERT INTO `bob_role_auth_relation` VALUES ('0002', '0001', '0002', '2020-07-07 17:03:40', '0000-00-00 00:00:00');
INSERT INTO `bob_role_auth_relation` VALUES ('0003', '0002', '0002', '2020-07-07 17:03:55', '0000-00-00 00:00:00');
INSERT INTO `bob_role_auth_relation` VALUES ('0004', '0002', '0003', '2020-07-10 17:34:01', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `bob_role_info`
-- ----------------------------
DROP TABLE IF EXISTS `bob_role_info`;
CREATE TABLE `bob_role_info` (
  `ID` varchar(32) NOT NULL COMMENT '角色ID,UUID生成',
  `ROLE_TYPE` varchar(32) NOT NULL COMMENT '用户类型',
  `ROLE_DESCRIPTION` varchar(1024) DEFAULT NULL COMMENT '角色描述',
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bob_role_info
-- ----------------------------
INSERT INTO `bob_role_info` VALUES ('0001', '01', '超级管理员', '2020-07-07 16:52:52', '0000-00-00 00:00:00');
INSERT INTO `bob_role_info` VALUES ('0002', '02', '普通管理员', '2020-07-07 16:53:08', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `bob_user_info`
-- ----------------------------
DROP TABLE IF EXISTS `bob_user_info`;
CREATE TABLE `bob_user_info` (
  `ID` varchar(32) NOT NULL COMMENT '用户ID,UUID生成',
  `USER_NAME` varchar(64) NOT NULL COMMENT '用户名称',
  `PASSWORD` varchar(256) NOT NULL COMMENT '密码',
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`USER_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bob_user_info
-- ----------------------------
INSERT INTO `bob_user_info` VALUES ('00002', 'admin', '$2a$10$pwONFL3ofw0fS0BxaP5TNugQCaFQON1p.8jygmPI3A03mFuJHT44q', '2020-07-07 16:52:31', '0000-00-00 00:00:00');
INSERT INTO `bob_user_info` VALUES ('00001', 'guopf', '$2a$10$hKVJY4VZ4hzycZs6rPOTcuL0ACVOcuxGPz3xPcTTMhZkOjP6wxESK', '2020-07-07 16:51:50', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `bob_user_role_relation`
-- ----------------------------
DROP TABLE IF EXISTS `bob_user_role_relation`;
CREATE TABLE `bob_user_role_relation` (
  `ID` varchar(32) NOT NULL,
  `USER_ID` varchar(32) DEFAULT NULL COMMENT '用户id',
  `ROLE_ID` varchar(32) DEFAULT NULL COMMENT '角色id',
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bob_user_role_relation
-- ----------------------------
INSERT INTO `bob_user_role_relation` VALUES ('0001', '00001', '0001', '2020-07-09 15:26:42', '0000-00-00 00:00:00');
INSERT INTO `bob_user_role_relation` VALUES ('0002', '00002', '0002', '2020-07-09 15:26:47', '0000-00-00 00:00:00');
