package cn.com.bobfintech.bobfintechconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@EnableConfigServer
public class BobfintechConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(BobfintechConfigApplication.class, args);
    }

}
