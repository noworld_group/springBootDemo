package cn.com.bobfintech.commons.constant;

/**
 * @project: 北银金科
 * @description: 公共的常量
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-03 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
public class BobfintechContant
{

    /**
     * 数字0
     */
    public static final int NUMBER_ZERO                       = 0;
    /**
     * 数字1
     */
    public static final int NUMBER_ONE                        = 1;
    /**
     * 数字2
     */
    public static final int NUMBER_TWO                        = 2;
    /**
     * 数字3
     */
    public static final int NUMBER_THREE                      = 3;
    /**
     * 数字4
     */
    public static final int NUMBER_FOUR                       = 4;
    /**
     * 数字5
     */
    public static final int NUMBER_FIVE                       = 5;
    /**
     * 数字6
     */
    public static final int NUMBER_SIX                        = 6;
    /**
     * 数字7
     */
    public static final int NUMBER_SEVEN                      = 7;
    /**
     * 数字8
     */
    public static final int NUMBER_EIGHT                      = 8;
    /**
     * 数字10
     */
    public static final int NUMBER_TEN                        = 10;
    /**
     * 数字12
     */
    public static final int NUMBER_TWELVE                     = 12;
    /**
     * 数字13
     */
    public static final int NUMBER_THIRTEEN                   = 13;
    /**
     * 数字14
     */
    public static final int NUMBER_FOURTEEN                   = 14;
    /**
     * 数字16
     */
    public static final int NUMBER_SIXTEEN                    = 16;
    /**
     * 数字24
     */
    public static final int NUMBER_TWENTY_FOUR                = 24;
    /**
     * 数字25
     */
    public static final int NUMBER_TWENTY_FIVE                = 25;
    /**
     * 数字26
     */
    public static final int NUMBER_TWENTY_SIX                 = 26;
    /**
     * 数字60
     */
    public static final int NUMBER_SIXTY                      = 60;
    /**
     * 数字100
     */
    public static final int NUMBER_ONE_HUNDRED                = 100;
    /**
     * 数字255
     */
    public static final int NUMBER_255                        = 255;
    /**
     * 数字400
     */
    public static final int NUMBER_FOUR_HUNDRED               = 400;
    /**
     * 数字500
     */
    public static final int NUMBER_FIVE_HUNDR                 = 500;
    /**
     * 数字1000
     */
    public static final int NUMBER_ONE_THOUSAND               = 1000;
    /**
     * 数字2000
     */
    public static final int NUMBER_2000                       = 2000;
    /**
     * 数字3600
     */
    public static final int NUMBER_THREE_THOUSAND_SIX_HUNDRED = 3600;
    /**
     * 数字10000
     */
    public static final int NUMBER_TEN_THOUSAND               = 10000;
    
    // 默认全局前缀
    public static final String COM_GLOBALID_PREFIX = "bobfintech";
    
    /**
     * token的KEY
     */
    public static final String CLIENT_TOKEN_KEY = "X-Token";
    // 存入redis的token key
    public static final String TOKEN_REDIS_KEY = "innovation:login:";
    // 登录后续约超时时间，单位秒
    public static final long USER_TOKEN_TIME = 3600;
    // 请求时间戳与当前时间戳比较 单位 分钟
    public static final long MINUTES_5 = 60 * 60 * 1000L;
    // 登录后续约超时时间，单位秒
    public static final long CAPTCHA_EXPIRE_TIME = 180;
    
    // map中value为list的KEy
    public static final String DATA_LIST = "datas";
    // map中value为总条数的KEy
    public static final String DATA_SIZE = "total";
    
    // redis持久化sentinel的限流kEY
    public static final String SENTINEL_REDIS_RULE_FLOW_KEY = "sentinel_rule_flow_bobfintech-gateway";;
    // channel
    public static final String SENTINEL_REDIS_RULE_FLOW_CHANNEL = "bobfintech-gateway";
    
    
}
