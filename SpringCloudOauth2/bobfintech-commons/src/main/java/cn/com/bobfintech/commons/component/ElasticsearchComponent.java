package cn.com.bobfintech.commons.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.lucene.search.TotalHits;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.MultiSearchRequest;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.replication.ReplicationResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.GetSourceRequest;
import org.elasticsearch.client.core.GetSourceResponse;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import cn.com.bobfintech.commons.config.ElasticsearchConfig;
import cn.com.bobfintech.commons.constant.BobfintechContant;
import cn.com.bobfintech.commons.constant.ElasticsearchContant;
import cn.com.bobfintech.commons.pojo.EsMultiQueryInfo;
import cn.com.bobfintech.commons.pojo.QueryInfo;
import cn.com.bobfintech.commons.utils.BeanUtils;
import cn.com.bobfintech.commons.utils.IdUtils;

/**
 * @project: 北银金科
 * @description: 中文类名。
 *               类功能简介。
 *               关系数据库 ⇒ 数据库 ⇒ 表 ⇒ 行 ⇒ 列(Columns)
 *               Elasticsearch ⇒ 索引(Index) ⇒ 类型(type) ⇒ 文档(Docments) ⇒ 字段(Fields)
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-01 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
public class ElasticsearchComponent
{
    Logger logger = LoggerFactory.getLogger(ElasticsearchComponent.class);
    
    private RestHighLevelClient client;
    
    // @Autowired
    // private ElasticsearchConfig elasticsearchConfig;
    
    /**
     * Java High Level REST Client 初始化
     */
    private void initClient(ElasticsearchConfig elasticsearchConfig)
    {
        String[] esUris = elasticsearchConfig.getEsUris();
        HttpHost[] httpHosts = new HttpHost[esUris.length];
        // 将地址转换为http主机数组，未配置端口则采用默认9200端口，配置了端口则用配置的端口
        for (int i = 0; i < httpHosts.length; i++) {
            if (!StringUtils.isEmpty(esUris[i])) {
                if (esUris[i].contains(":")) {
                    String[] uris = esUris[i].split(":");
                    String url = null;
                    if (uris[1].contains("/")) {
                        url = uris[1].substring(uris[1].lastIndexOf("/") + 1);
                        System.out.println("剪切后的urils" + url);
                    }
                    else {
                        url = uris[1];
                    }
                    httpHosts[i] = new HttpHost(url, Integer.parseInt(uris[2]), uris[0]);
                }
                else {
                    httpHosts[i] = new HttpHost(esUris[i], 9200, "http");
                }
            }
        }
        // 判断，如果未配置用户名，则进行无用户名密码连接，配置了用户名，则进行用户名密码连接
        if (StringUtils.isEmpty(elasticsearchConfig.getUserName())) {
            client = new RestHighLevelClient(RestClient.builder(httpHosts));
        }
        else {
            final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(elasticsearchConfig.getUserName(), elasticsearchConfig.getPassword()));
            client = new RestHighLevelClient(
                RestClient.builder(httpHosts).setHttpClientConfigCallback((httpClientBuilder) -> {
                    // 这里可以设置一些参数，比如cookie存储、代理等等
                    httpClientBuilder.disableAuthCaching();
                    return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                    }).setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback()
                    {
                        @Override
                        public RequestConfig.Builder customizeRequestConfig(RequestConfig.Builder requestConfigBuilder)
                        {
                            return requestConfigBuilder.setConnectTimeout(elasticsearchConfig.getTimeout())
                                .setSocketTimeout(elasticsearchConfig.getTimeout());
                        }
                    }));
        }
    }
    
    /**
     * 关闭ES客户端
     */
    public void closeClient()
    {
        try {
            if (client != null) {
                client.close();
            }
            client = null;
        }
        catch (Exception e2) {
            e2.printStackTrace();
            System.out.println("关闭客户端失败");
        }
    }
    
    /**
     * 添加索引
     *
     * @param indexName
     *            索引名稱，必須為小寫
     * @param paramMap
     *            參數內容
     * @throws Exception
     */
    public void addData(ElasticsearchConfig elasticsearchConfig, String indexName,
                        Map<String, Object> paramMap, String id) throws Exception
    {
        if (cn.com.bobfintech.commons.utils.StringUtils.isBlank(id)) {
            id = IdUtils.getUuid();
        }
        delWithAddData(elasticsearchConfig, indexName, id,
            paramMap,
            ElasticsearchContant.DATA_TYPE_MAP);
    }
    
    /**
     * 添加索引，自定义bean
     *
     * @param indexName
     *            索引名称
     * @param object
     *            bean 实体类
     * @throws Exception
     */
    public void addData(ElasticsearchConfig elasticsearchConfig, String indexName, Object object,
                        String id) throws Exception
    {
        if (cn.com.bobfintech.commons.utils.StringUtils.isBlank(id)) {
            id = IdUtils.getUuid();
        }
        delWithAddData(elasticsearchConfig, indexName, id, object,
            ElasticsearchContant.DATA_TYPE_BEAN);
    }
    
    /**
     * 新增，修改文档
     * 
     * @param indexName
     *            索引
     * @param type
     *            mapping type
     * @param id
     *            文档id
     * @param jsonStr
     *            文档数据
     * @throws Exception
     */
    public void addData(ElasticsearchConfig elasticsearchConfig, String indexName, String jsonStr,
                        String id) throws Exception
    {
        if (cn.com.bobfintech.commons.utils.StringUtils.isBlank(id)) {
            id = IdUtils.getUuid();
        }
        delWithAddData(elasticsearchConfig, indexName, id,
            jsonStr,
            ElasticsearchContant.DATA_TYPE_JSON);
    }

    /**
     * 同步查询索引下的文档是否存在，如存在，返回true,否则返回false
     *
     * @param id
     *            文档编号
     * @param index
     *            索引
     * @return
     * @throws IOException
     */
    public boolean existDoc4Sync(ElasticsearchConfig elasticsearchConfig, String id, String index) throws IOException
    {
        GetRequest getRequest = new GetRequest();
        getRequest.index(index);
        if (cn.com.bobfintech.commons.utils.StringUtils.isNotBlank(id)) {
            getRequest.id(id);
        }
        // 禁用获取source内容
        getRequest.fetchSourceContext(new FetchSourceContext(false));
        // 禁用获取存储的字段
        getRequest.storedFields("_none_");
        
        if (client == null) {
            initClient(elasticsearchConfig);
        }
        boolean isExist = client.exists(getRequest, RequestOptions.DEFAULT);
        closeClient();
        return isExist;
    }
    
    /**
     * 异步查询索引下的文档是否存在，调用成功返回true(不等于文档存在，只有等待异步反馈结果才能知道是否成功)
     * 异步方法不会阻塞并立即返回。完成ActionListener后，onResponse如果执行成功完成，则使用onFailure方法进行调用；如果执行失败，则使用方法进行调用。故障情况和预期的异常与同步执行情况相同。
     * 
     * @param id
     *            文档编号
     * @param index
     *            索引
     * @return
     * @throws Exception
     */
    public boolean existDoc4Async(ElasticsearchConfig elasticsearchConfig, String id, String index)
    {
        GetRequest getRequest = new GetRequest(index, id);
        // 禁用获取source内容
        getRequest.fetchSourceContext(new FetchSourceContext(false));
        // 禁用获取存储的字段
        getRequest.storedFields("_none_");
        
        if (client == null) {
            initClient(elasticsearchConfig);
        }
        ActionListener<Boolean> listener = new ActionListener<Boolean>()
        {
            @Override
            public void onResponse(Boolean exists)
            {
                logger.info("异步查询索引:[{}],文档id:[{}]返回的查询结果为:[{}]", index, id, exists);
            }
            
            @Override
            public void onFailure(Exception e)
            {
                logger.error("异步查询失败:[{}]", e.getMessage(), e);
            }
        };
        client.existsAsync(getRequest, RequestOptions.DEFAULT, listener);
        closeClient();
        return true;
        
    }
    
    /**
     * 同步查询source
     *
     * @param index
     *            索引
     * @param id
     *            文档编号
     * @return
     * @throws IOException
     */
    public GetSourceResponse getEsSource4Sync(ElasticsearchConfig elasticsearchConfig, String index,
                                              String id) throws IOException
    {
        GetSourceRequest getSourceRequest = new GetSourceRequest(index, id);
        // 可选参数可以选择提供以下参数，用来控制是否返回_source 或者返回什么样的source
        // String[] includes = Strings.EMPTY_ARRAY; // 必填
        // String[] excludes = new String[] { "postDate" }; // 上下文参数
        // getSourceRequest.fetchSourceContext(new FetchSourceContext(true, includes, excludes));
        getSourceRequest.refresh(true);
        
        if (client == null) {
            initClient(elasticsearchConfig);
        }
        GetSourceResponse getSourceResponse = client.getSource(getSourceRequest, RequestOptions.DEFAULT);
        closeClient();
        return getSourceResponse;
    }
    
    /**
     * 异步查询source
     *
     * @param index
     *            索引
     * @param id
     *            文档编号
     * @return
     * @throws Exception
     */
    public boolean getEsSource4Async(ElasticsearchConfig elasticsearchConfig, String index, String id)
    {
        GetSourceRequest getSourceRequest = new GetSourceRequest(index, id);
        // 可选参数可以选择提供以下参数，用来控制是否返回_source 或者返回什么样的source
        // String[] includes = Strings.EMPTY_ARRAY; // 必填
        // String[] excludes = new String[] { "postDate" }; // 上下文参数
        // getSourceRequest.fetchSourceContext(new FetchSourceContext(true, includes, excludes));
        getSourceRequest.refresh(true);
        
        if (client == null) {
            initClient(elasticsearchConfig);
        }
        ActionListener<GetSourceResponse> listener = new ActionListener<GetSourceResponse>()
        {
            @Override
            public void onResponse(GetSourceResponse getResponse)
            {
                Map<String, Object> resultMap = getResponse.getSource();
                logger.info("异步查询索引:[],文档id:[]返回的查询source结果为:[]", index, id, resultMap);
            }
            
            @Override
            public void onFailure(Exception e)
            {
                logger.error("异步查询source失败:[]", e.getMessage(), e);
            }
        };
        client.getSourceAsync(getSourceRequest, RequestOptions.DEFAULT, listener);
        closeClient();
        return true;
    }

   /**
    * 获取索引下的数据
    *
    * @param elasticsearchConfig
    *            配置
    * @param index
    *            索引
    * @param from
    *            开始条数
    * @param size
    *            查询条数（最多不能超过10000）
    * @return
    * @throws IOException
    */
   public Map<String, Object> getAllIndexData(ElasticsearchConfig elasticsearchConfig,
                                                    String index,
                                         int from,
                                          int size) throws IOException
    {
        SearchRequest request = new SearchRequest(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        request.source(searchSourceBuilder);
        if (client == null) {
            initClient(elasticsearchConfig);
        }
        SearchResponse searchResponse = client.search(request, RequestOptions.DEFAULT);
        closeClient();
        SearchHits hits = searchResponse.getHits();
        TotalHits totalHits = hits.getTotalHits();
        long totalSize = totalHits.value;
        SearchHit[] searchHits = hits.getHits();
        
        Map<String, Object> resultMap = new HashMap<String, Object>();
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        Map<String, Object> sourceMap = null;
        for (SearchHit searchHit : searchHits) {
            sourceMap = searchHit.getSourceAsMap();
            resultList.add(sourceMap);
        }
        resultMap.put(BobfintechContant.DATA_LIST, resultList);
        resultMap.put(BobfintechContant.DATA_SIZE, totalSize);
        return resultMap;
    }
    

    /**
     * 向ES添加/编辑索引
     *
     * @param indexName
     *            索引名称，必须为小写
     * @param type
     *            索引类型，create,index
     * @param id
     *            索引id
     * @param object
     *            内容对象
     * @param contentType
     *            内容类型
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    private void delWithAddData(ElasticsearchConfig elasticsearchConfig, String indexName, String id, Object object,
                                String contentType) throws Exception
    {
        
        // 1、创建索引请求 //索引 // mapping type //文档id
        IndexRequest request = new IndexRequest(indexName);
        // 2、設置id
        request.id(id);
        // 设置操作类型
        request.opType(ElasticsearchContant.ELASTICSEARCH_DATA_TYPE_INDEX);
        // 2、准备文档数据
        // 直接给JSON串
        switch (contentType) {
        case ElasticsearchContant.DATA_TYPE_MAP:
            Map<String, Object> map = (Map<String, Object>) object;
            request.source(map);
            break;
        case ElasticsearchContant.DATA_TYPE_JSON:
            String jsonString = (String) object;
            request.source(jsonString, XContentType.JSON);
            break;
        case ElasticsearchContant.DATA_TYPE_BEAN:
            // instance a json mapper
            Map<String, Object> bean2Map = BeanUtils.bean2Map(object);
            // ObjectMapper mapper = new ObjectMapper(); // create once, reuse
            // generate json
            // byte[] json = mapper.writeValueAsBytes(object);
            request.source(bean2Map);
            break;
        default:
            System.out.println("未识别的ES数据类型");
            throw new Exception();
        }
        if (client == null) {
            initClient(elasticsearchConfig);
        }
        // 4、发送请求
        IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);
        closeClient();
        // 5、处理响应
        if (indexResponse != null) {
            String index1 = indexResponse.getIndex();
            String id1 = indexResponse.getId();
            long version1 = indexResponse.getVersion();
            if (indexResponse.getResult() == DocWriteResponse.Result.CREATED) {
                System.out.println("新增文档成功!" + index1 + id1 + version1);
            }
            else if (indexResponse.getResult() == DocWriteResponse.Result.UPDATED) {
                System.out.println("修改文档成功!");
            }
            // 分片处理信息
            ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();
            if (shardInfo.getTotal() != shardInfo.getSuccessful()) {
                System.out.println("分片处理信息.....");
            }
            // 如果有分片副本失败，可以获得失败原因信息
            if (shardInfo.getFailed() > 0) {
                for (ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()) {
                    String reason = failure.reason();
                    System.out.println("副本失败原因：" + reason);
                }
            }
        }
    }
    
    /**
     * 单条件分页查询
     *
     * @param elasticsearchConfig
     *            配置
     * @param indices
     *            索引（非必填）
     * @param sortField
     *            排序字段
     * @param fieldKey
     *            查询条件(KEY)
     * @param fieldVal
     *            查询条件(value)
     * @param from
     *            开始条数
     * @param size
     *            要查询的条数
     * @throws IOException
     */
    public Map<String, Object> searchOnParamByPage(ElasticsearchConfig elasticsearchConfig,
                             String[] indices,
                             String sortField,
                             String fieldKey,
                             String fieldVal,
                             int from, int size) throws IOException
    {
        SearchRequest searchRequest = new SearchRequest(indices);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.matchPhraseQuery(fieldKey, fieldVal));
        sourceBuilder.from(from);
        sourceBuilder.size(size);
        sourceBuilder.timeout(new TimeValue(1000));
        sourceBuilder.trackTotalHits(true);
        sourceBuilder.sort(new FieldSortBuilder(sortField + ".keyword").order(SortOrder.DESC));
        searchRequest.source(sourceBuilder);
        
        if (client == null) {
            initClient(elasticsearchConfig);
        }
        
        SearchResponse searchResponse = client
            .search(searchRequest,
            RequestOptions.DEFAULT);
        SearchHits hits = searchResponse.getHits();
        TotalHits totalHits = hits.getTotalHits();
        long totalSize = totalHits.value;
        SearchHit[] searchHits = hits.getHits();
        
        Map<String, Object> resultMap = new HashMap<String, Object>();
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        Map<String, Object> sourceMap = null;
        for (SearchHit searchHit : searchHits) {
            sourceMap = searchHit.getSourceAsMap();
            resultList.add(sourceMap);
        }
        closeClient();
        logger.info("单条件分页查询时，查询出的总条数为：【{}】", totalSize);
        resultMap.put(BobfintechContant.DATA_LIST, resultList);
        resultMap.put(BobfintechContant.DATA_SIZE, totalSize);
        return resultMap;
    }
    
    /**
     * 单条件分页查询
     *
     * @param elasticsearchConfig
     *            配置
     * @param indices
     *            索引（非必填）
     * @param sortField
     *            排序字段
     * @param fieldKey
     *            查询条件(KEY)
     * @param fieldVal
     *            查询条件(value)
     * @param from
     *            开始条数
     * @param size
     *            要查询的条数
     * @throws IOException
     */
    public Map<String, Object> testQueryByPage(ElasticsearchConfig elasticsearchConfig, String[] indices,
                                               String sortField,
                                String fieldKey, String fieldVal, int from, int size) throws IOException
    {
        BoolQueryBuilder boolQuery = new BoolQueryBuilder();
        RangeQueryBuilder rangeQuery = QueryBuilders.rangeQuery(sortField).gte(null); // "count"
        boolQuery.filter(rangeQuery);
        
        MatchQueryBuilder matchQuery = new MatchQueryBuilder(fieldKey, fieldVal);
        boolQuery.must(matchQuery);
        
        if (client == null) {
            initClient(elasticsearchConfig);
        }
        
        SearchResponse searchResponse = client
            .search(
            new SearchRequest(
                indices)
                    .source(new SearchSourceBuilder().query(boolQuery).from(from).size(size)
                        .trackTotalHits(true)),
            RequestOptions.DEFAULT);
        
        // System.out.println(response.getHits().getTotalHits());
        // System.out.println(response.toString());
        // closeClient();
        
        SearchHits hits = searchResponse.getHits();
        TotalHits totalHits = hits.getTotalHits();
        long totalSize = totalHits.value;
        SearchHit[] searchHits = hits.getHits();
        
        Map<String, Object> resultMap = new HashMap<String, Object>();
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        Map<String, Object> sourceMap = null;
        for (SearchHit searchHit : searchHits) {
            sourceMap = searchHit.getSourceAsMap();
            resultList.add(sourceMap);
        }
        closeClient();
        logger.info("单条件分页查询时，查询出的总条数为：【{}】", totalSize);
        resultMap.put(BobfintechContant.DATA_LIST, resultList);
        resultMap.put(BobfintechContant.DATA_SIZE, totalSize);
        return resultMap;
    }
    
    
    /**
     * 多条件查询ES
     *
     * @param elasticsearchConfig
     *            配置
     * @param paramList
     *            参数列表
     */
    public void testMultiSearch(ElasticsearchConfig elasticsearchConfig, List<EsMultiQueryInfo> queryInfos)
    {
        
        MultiSearchRequest request = new MultiSearchRequest();
        if (CollectionUtils.isEmpty(queryInfos)) {
            // return null;
        }
        SearchRequest firstSearchRequest = null;
        SearchSourceBuilder searchSourceBuilder = null;
        for (EsMultiQueryInfo esMultiQueryInfo : queryInfos) {
            firstSearchRequest = new SearchRequest();
            searchSourceBuilder = new SearchSourceBuilder();
            firstSearchRequest.indices(esMultiQueryInfo.getIndices());
            List<QueryInfo> queryInfoList = esMultiQueryInfo.getQueryInfos();
            if (CollectionUtils.isNotEmpty(queryInfoList)) {
                for (QueryInfo queryInfo : queryInfoList) {
                    searchSourceBuilder
                        .query(QueryBuilders.matchQuery(queryInfo.getParamKey(), queryInfo.getParamVal()));
                }
            }
            firstSearchRequest.source(searchSourceBuilder);
            request.add(firstSearchRequest);
        }
        try {
            if (client == null) {
                initClient(elasticsearchConfig);
            }
            MultiSearchResponse response = client.msearch(request, RequestOptions.DEFAULT);
            Map<String, Object> resultMap = new HashMap<String, Object>();
            List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
            response.forEach(t -> {
                // Map<String, Object> sourceMap = null;
                SearchResponse resp = t.getResponse();
                Arrays.stream(resp.getHits().getHits()).forEach(searchHit -> {
                    Map<String, Object> sourceMap = searchHit.getSourceAsMap();
                    resultList.add(sourceMap);
                });
            });
            
            closeClient();
            resultMap.put(BobfintechContant.DATA_LIST, resultList);
            resultMap.put(BobfintechContant.DATA_SIZE, resultList.size());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
