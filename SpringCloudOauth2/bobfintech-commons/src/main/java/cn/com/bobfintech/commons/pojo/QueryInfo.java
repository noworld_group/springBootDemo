package cn.com.bobfintech.commons.pojo;

import lombok.Data;

/**
 * @project: 北银金科
 * @description: 查询条件
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-14 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Data
public class QueryInfo
{
    // 字段
    private String paramKey;
    
    // 值
    private String paramVal;
}
