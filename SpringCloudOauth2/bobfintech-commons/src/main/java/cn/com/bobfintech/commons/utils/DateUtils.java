package cn.com.bobfintech.commons.utils;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cn.com.bobfintech.commons.constant.BobfintechContant;
import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.exception.BusinessException;

/**
 * @project: 北银金科
 * @description: 日期工具。
 *               注意事项: 类似其他相关功能，可参考 org.apache.commons.lang.time
 *               包下面的DateUtils、DateFormatUtils、FastDateFormat<br>
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-03 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
public class DateUtils
{
    
    /** 模式 :yyyyMMddHHmmss */
    private static final String   YYYYMMDD_HHMMSS     = "yyyyMMddHHmmss";
    /** 模式 :yyyyMMdd */
    private static final String   YYYYMMDD            = "yyyyMMdd";
    /** 模式 :HHmmss */
    private static final String   HHMMSS              = "HHmmss";
    /** 秒与毫秒转化的单位 */
    public static final int       SECOND_UNIT         = 1000;
    /** 季度常量 */
    private final static int      QUARTERS_EVERY_YEAR = 4;
    /** 每个季度的第三个月20号 */
    private final static String[] QUARTERS            = { "{0}0320", "{0}0620", "{0}0920", "{0}1220" };
    
    /**
     * 方法说明：long类型转换为日期类型.
     * 
     * @param date
     *            14为整型
     * @return
     */
    public static Date long2Date(Long date)
    {
        return null == date ? null : string2Date(date.toString());
    }
    
    /**
     * 方法说明：字符串转日期类型.
     * 
     * @param date
     *            日期字符串
     * @return
     */
    public static Date string2Date(String date)
    {
        try {
            if (date.length() != BobfintechContant.NUMBER_SIXTEEN) {
                return getDateFormat().parse(date);
            }
            else {
                return getDateFormat(YYYYMMDD_HHMMSS).parse(date);
            }
        }
        catch (Exception e) {
            throw new BusinessException(
                BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR
                .getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "日期转换出错"), e);
        }
    }
    
    /**
     * 日期转换
     *
     * @param srcStr
     *            日期字符串
     * @param pattern
     *            日期格式
     * @return
     */
    public static String stringFormat(String srcStr, String pattern)
    {
        Date date = string2Date(srcStr);
        return date2String(date, pattern);
    }
    
    /**
     * 方法说明：日期类型转成yyyyMMdd格式字符串.
     * 
     * @param date
     *            日期
     * @return
     */
    public static String date2String(Date date)
    {
        return date2String(date, YYYYMMDD);
    }
    
    /**
     * 方法说明：日期类型按照指定格式转成字符串.
     * 
     * @param date
     *            日期
     * @param pattern
     *            日期格式
     * @return
     */
    public static String date2String(Date date, String pattern)
    {
        if (null == date) {
            date = new Date();
        }
        if (StringUtils.isBlank(pattern)) {
            pattern = "yyyy-MM-dd HH:mm:ss";
        }
        try {
            return getDateFormat(pattern).format(date);
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "日期转换出错"), e);
        }
    }
    
    /**
     * 方法说明：将日期类型转为Long类型
     * 
     * @param date
     *            如果为null,则取当前服务器日期.
     * @return
     */
    public static Long date2Long(Date date)
    {
        if (null == date) {
            date = new Date();
        }
        try {
            return Long.valueOf(getDateFormat(YYYYMMDD_HHMMSS).format(date));
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "日期转换出错"), e);
        }
    }
    
    /**
     * 方法说明：将日期类型转为Long类型
     * 
     * @param date
     *            如果为null,则取当前服务器日期.
     * @return
     */
    public static Long date2LongYyyymmdd(Date date)
    {
        if (null == date) {
            date = new Date();
        }
        try {
            return Long.valueOf(getDateFormat(YYYYMMDD).format(date));
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "日期转换出错"), e);
        }
    }
    
    /**
     * 方法说明：将时间格式化.
     * 
     * @param time
     *            如果为null,则取当前服务器日期.
     * @return
     */
    public static Long timeFormat(Long time)
    {
        if (null == time) {
            time = new Date().getTime();
        }
        try {
            return Long.valueOf(getDateFormat(HHMMSS).format(time));
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "日期转换出错"), e);
        }
    }
    
    /**
     * 方法说明：输入符合要求的日期型长整数，将其转化成Calendar.
     * 
     * @param dateLong
     *            日期格式 yyyyMMdd
     * @return
     */
    public static Calendar longToCalendar(Long dateLong)
    {
        if (null == dateLong) {
            return null;
        }
        try {
            Date date = getDateFormat().parse(dateLong.toString());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar;
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "日期转换出错"), e);
        }
    }
    
    /**
     * 方法说明：输入Calendar,转化为符合要求的日期型长整数:yyyyMMdd.
     * 
     * @param calendarDate
     *            如果为null,则取当前服务器日期.
     * @return
     */
    public static Long calendarToLong(Calendar calendarDate)
    {
        if (calendarDate == null) {
            calendarDate = Calendar.getInstance();
        }
        try {
            return Long.valueOf(getDateFormat().format(calendarDate.getTime()));
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "日期转换出错"), e);
        }
    }
    
    /**
     * 方法说明：获取指定模式pattern的SimpleDateFormat对象.
     * 
     * @param pattern
     *            日期格式
     * @return
     */
    private static SimpleDateFormat getDateFormat(String pattern)
    {
        return new SimpleDateFormat(pattern);
    }
    
    /**
     * 方法说明：获取默认模式"yyyyMMdd"的SimpleDateFormat对象.
     * 
     * @return
     */
    private static SimpleDateFormat getDateFormat()
    {
        return new SimpleDateFormat(YYYYMMDD);
    }
    
    /**
     * 以date为基准获取n天后的日期，
     * 
     * @param date
     *            String型日期
     * @param n
     *            -1表示date的前一天，1表示date的后一天。
     * @return String型日期 @ 业务异常
     */
    public static Long getNextDate(long date, int n)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(long2Date(date));
        int newDay = c.get(Calendar.DAY_OF_MONTH) + n;
        c.set(Calendar.DAY_OF_MONTH, newDay);
        return date2Long(c.getTime());
    }
    
    /**
     * 以date为基准获取n天后的日期，
     * 
     * @param date
     *            String型日期
     * @param n
     *            -1表示date的前一天，1表示date的后一天。
     * @return String型日期 @ 业务异常
     */
    public static String getNextDate(String date, int n)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(string2Date(date));
        int newDay = c.get(Calendar.DAY_OF_MONTH) + n;
        c.set(Calendar.DAY_OF_MONTH, newDay);
        return date2String(c.getTime());
    }
    
    /**
     * 以date为基准获取n天后的日期，
     * 
     * @param date
     *            String型日期
     * @param n
     *            -1表示date的前一天，1表示date的后一天。
     * @return String型日期 @ 业务异常
     */
    public static String getNextDay(String date, int n)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(string2Date(date));
        int newDay = c.get(Calendar.DAY_OF_MONTH) + n;
        c.set(Calendar.DAY_OF_MONTH, newDay);
        return date2String(c.getTime());
    }
    
    /**
     * 以date为基准获取n月后的日期
     * 
     * @param date
     *            String型日期
     * @param n
     *            -1表示date的前一月，1表示date的后一月。
     * @return String型日期
     */
    public static String getNextMonth(String date, int n)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(string2Date(date));
        c.add(Calendar.MONTH, n);
        return date2String(c.getTime());
    }
    
    /**
     * 以date为基准获取n年后的日期，
     * 
     * @param date
     *            String型日期
     * @param n
     *            -1表示date的前一年，1表示date的后一年。
     * @return String型日期
     */
    public static String getNextYear(String date, int n)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(string2Date(date));
        c.add(Calendar.YEAR, n);
        return date2String(c.getTime());
    }
    
    /**
     * 以date为基准获取n天后的日期，
     * 
     * @param date
     *            long型日期
     * @param n
     *            -1表示date的前一天，1表示date的后一天。
     * @return long型日期 @ 业务异常
     */
    public static Long getNextDateYyyymmdd(long date, int n)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(long2Date(date));
        int newDay = c.get(Calendar.DAY_OF_MONTH) + n;
        c.set(Calendar.DAY_OF_MONTH, newDay);
        return date2LongYyyymmdd(c.getTime());
    }
    
    /**
     * 得到两个日期的间隔天数
     * 
     * @param begindate
     *            起始日期
     * @param enddate
     *            结束日期
     * @return 间隔天数 @ 统一业务异常
     */
    public static Long getDays(Long begindate, Long enddate)
    {
        Calendar begin = longToCalendar(begindate);
        Calendar end = longToCalendar(enddate);
        return diffDays(end.getTime(), begin.getTime());
    }
    
    /**
     * 计算两个日期相差天数。 用第一个日期减去第二个。如果前一个日期小于后一个日期，则返回负数
     * 
     * @param one
     *            第一个日期数，作为基准
     * @param two
     *            第二个日期数，作为比较
     * @return 两个日期相差天数
     */
    public static Long diffDays(Date one, Date two)
    {
        return (one.getTime() - two.getTime())
               / (BobfintechContant.NUMBER_TWENTY_FOUR
                  * BobfintechContant.NUMBER_SIXTY * BobfintechContant.NUMBER_SIXTY
                  * BobfintechContant.NUMBER_ONE_THOUSAND);
    }
    
    /**
     * 计算两个日期的计息天数。 用第一个日期减去第二个。如果前一个日期小于后一个日期，则返回负数
     * 
     * @param endDate
     *            结束日期
     * @param startDate
     *            开始日期
     * @return 两个日期的计息期天数
     */
    public static Long calcAccrualDays(String endDate, String startDate)
    {
        Date oneDate = DateUtils.string2Date(endDate);
        Date twoDate = DateUtils.string2Date(startDate);
        return (oneDate.getTime() - twoDate.getTime())
               / (BobfintechContant.NUMBER_TWENTY_FOUR * BobfintechContant.NUMBER_SIXTY * BobfintechContant.NUMBER_SIXTY
                  * BobfintechContant.NUMBER_ONE_THOUSAND);
    }
    
    /**
     * 计算两个日期相差天数。 用第一个日期减去第二个。如果前一个日期小于后一个日期，则返回负数
     * 
     * @param one
     *            第一个日期数，作为基准
     * @param two
     *            第二个日期数，作为比较
     * @return 两个日期相差天数
     */
    public static Long diffDays(String one, String two)
    {
        Date oneDate = DateUtils.string2Date(one);
        Date twoDate = DateUtils.string2Date(two);
        return (oneDate.getTime() - twoDate.getTime())
               / (BobfintechContant.NUMBER_TWENTY_FOUR * BobfintechContant.NUMBER_SIXTY * BobfintechContant.NUMBER_SIXTY
                  * BobfintechContant.NUMBER_ONE_THOUSAND);
    }
    
    /**
     * 获取两个日期之间的相差天数
     * 
     * @param beginDate
     *            开始日期
     * @param endDate
     *            结算日期
     * @return
     * @throws Exception
     *             异常
     */
    public static Long getDateDiff(Long beginDate, Long endDate)
    {
        Date dateEnd = null; // 定义时间类型
        Date dateBegin = null; // 定义时间类型
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyyMMdd");
        try {
            dateEnd = inputFormat.parse(endDate.toString()); // 将字符型转换成日期型
            dateBegin = inputFormat.parse(beginDate.toString()); // 将字符型转换成日期型
            return (dateEnd.getTime() - dateBegin.getTime()) / BobfintechContant.NUMBER_ONE_THOUSAND
                   / BobfintechContant.NUMBER_THREE_THOUSAND_SIX_HUNDRED / BobfintechContant.NUMBER_TWENTY_FOUR; // 返回天数
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "日期转换出错"), e);
        }
    }
    
    /**
     * 获取Long型日期 YYYYMMDD
     * 
     * @param date
     *            日期
     * @return
     */
    public static Long getLongDateTime(Date date)
    {
        SimpleDateFormat sd = new SimpleDateFormat("yyyyMMddHHmmss");
        return Long.valueOf(sd.format(date));
    }
    
    /**
     * 根据Date返回格式如20120901的long型日期数值
     * 
     * @param date
     *            日期
     * @return long型日期
     */
    public static Long getLongDate(Date date)
    {
        SimpleDateFormat sd = new SimpleDateFormat("yyyyMMdd");
        return Long.valueOf(sd.format(date));
    }
    
    /**
     * 根据Date返回格式如120801的long型时间数值，表示12点零8分1秒 40411表示4点04分11秒
     * 
     * @param date
     *            日期
     * @return long型时间
     */
    public static Long getLongTime(Date date)
    {
        SimpleDateFormat sd = new SimpleDateFormat("HHmmss");
        return Long.valueOf(sd.format(date));
    }
    
    /**
     * 指定时间、偏移量(以秒为单位)，获取偏移后的date
     * 
     * @param date
     *            指定时间
     * @param offset
     *            偏移秒数，负数表示向前，正数表示向后。
     * @return 偏移后的时间。
     */
    public static Date getOffsetDate(Date date, long offset)
    {
        long baseTime = date.getTime();
        long targetTime = baseTime + offset * SECOND_UNIT;
        Date resultDate = new Date();
        resultDate.setTime(targetTime);
        return resultDate;
    }
    
    /**
     * 获取当前日期时间
     * 
     * @param format
     *            日期格式
     * @return
     */
    public static String getDateTime(String format)
    {
        Date dt = new Date();
        return formatDate(dt, format);
    }
    
    /**
     * 格式化日期时间为指定格式的字符串
     *
     * @param dt
     *            日期
     * @param format
     *            日期格式
     * @return
     */
    public static String formatDate(Date dt, String format)
    {
        if (StringUtils.isBlank(format.trim())) {
            format = "yyyy-MM-dd HH:mm:ss";
        }
        if (dt == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(dt);
    }
    
    /**
     * 判断日期是否为周末
     * 
     * @param date
     *            日期
     * @return true 是周末
     * @throws BusinessException
     *             业务异常
     */
    public static boolean isWeekend(long date) throws BusinessException
    {
        Calendar c = Calendar.getInstance();
        c.setTime(DateUtils.long2Date(date));
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == Calendar.SUNDAY || dayOfWeek == Calendar.SATURDAY) {
            return true;
        }
        return false;
    }
    
    /**
     * 日期补零
     *
     * @param str
     *            日期
     * @param length
     *            长度
     * @return
     */
    private static String addZero(String str, long length)
    {
        String temp = str;
        if (length > 0) {
            temp = addZero("0" + str, length - 1);
        }
        return temp;
    }
    
    /**
     * 格式化long型日期或时间
     * 
     * @param param
     *            日期long
     * @return
     */
    public static String format(long param)
    {
        String result = String.valueOf(param);
        StringBuilder sb;
        if (result.length() == BobfintechContant.NUMBER_EIGHT) {// yyyyMMddd
            // sb = new StringBuilder(result);
            sb = new StringBuilder(addZero(result, BobfintechContant.NUMBER_EIGHT - result.length()));
            result = sb.insert(BobfintechContant.NUMBER_FOUR, "-").insert(BobfintechContant.NUMBER_SEVEN, "-")
                .toString();
        }
        else if (result.length() <= BobfintechContant.NUMBER_SIX) {// HHmmss
            sb = new StringBuilder(addZero(result, BobfintechContant.NUMBER_SIX - result.length()));
            result = sb.insert(BobfintechContant.NUMBER_TWO, ":").insert(BobfintechContant.NUMBER_FIVE, ":").toString();
        }
        else {
            // do nothing
        }
        return result;
    }
    
    /**
     * 日期格式化
     *
     * @param gmtDate
     *            日期
     * @return
     */
    public static String formatGmtDate(Date gmtDate)
    {
        if (null == gmtDate) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(gmtDate);
    }
    
    /**
     * 把日期形式20120928转化成2012-09-28的形式。
     * 
     * @param dateNum
     *            long型日期格式
     * @return 日期字符串
     * @throws BusinessException
     *             日期格式不正确时抛出格式异常
     */
    public static String getFormatDate(long dateNum) throws BusinessException
    {
        SimpleDateFormat sd1 = new SimpleDateFormat("yyyyMMdd");
        Date date;
        if (0 == dateNum) {
            return "0";
        }
        try {
            date = sd1.parse(dateNum + "");
            SimpleDateFormat sd2 = new SimpleDateFormat("yyyy-MM-dd");
            return sd2.format(date);
        }
        catch (ParseException e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "日期转换出错"), e);
        }
    }
    
    /**
     * 把日期形式20120928转化成2012-09-28的形式。
     * 
     * @param dateStr
     *            String型日期格式
     * @return 日期字符串
     * @throws BusinessException
     *             日期格式不正确时抛出格式异常
     */
    public static String getFormatDate(String dateStr) throws BusinessException
    {
        return getFormatDate(dateStr, "yyyy-MM-dd");
    }
    
    /**
     * 把日期形式20120928转化成2012-09-28的形式。
     * 
     * @param dateStr
     *            String型日期格式
     * @param pattern
     *            日期格式
     * @return 日期字符串
     * @throws BusinessException
     *             日期格式不正确时抛出格式异常
     */
    public static String getFormatDate(String dateStr, String pattern) throws BusinessException
    {
        SimpleDateFormat sd1 = new SimpleDateFormat("yyyyMMdd");
        Date date;
        if (StringUtils.isBlank(dateStr) || StringUtils.isEmpty(pattern)) {
            return "";
        }
        try {
            date = sd1.parse(dateStr);
            SimpleDateFormat sd2 = new SimpleDateFormat(pattern);
            return sd2.format(date);
        }
        catch (ParseException e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "日期转换出错"), e);
        }
    }
    
    /**
     * 把日期形式20120928转化成2012-09-28的形式。
     * 
     * @param dateNum
     *            long型日期格式
     * @return 日期字符串
     * @throws BusinessException
     *             日期格式不正确时抛出格式异常
     */
    public static String getFormatTime(long dateNum) throws BusinessException
    {
        SimpleDateFormat sd1 = new SimpleDateFormat("HHmmss");
        Date date;
        if (0 == dateNum) {
            return "0";
        }
        String temp = dateNum + "";
        while (temp.length() < BobfintechContant.NUMBER_SIX) {
            temp = "0" + temp;
        }
        try {
            date = sd1.parse(temp);
            SimpleDateFormat sd2 = new SimpleDateFormat("HH:mm:ss");
            return sd2.format(date);
        }
        catch (ParseException e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "日期转换出错"), e);
        }
        
    }
    
    /**
     * 时间格式化
     *
     * @param dateStr
     *            时间字符串
     * @param pattern
     *            日期格式化
     * @return
     * @throws BusinessException
     *             异常
     */
    public static String getFormatTime(String dateStr, String pattern) throws BusinessException
    {
        SimpleDateFormat sd1 = new SimpleDateFormat("HHmmss");
        Date date;
        if (StringUtils.isBlank(dateStr)) {
            return "0";
        }
        String temp = dateStr + "";
        while (temp.length() < BobfintechContant.NUMBER_SIX) {
            temp = "0" + temp;
        }
        try {
            date = sd1.parse(temp);
            SimpleDateFormat sd2 = new SimpleDateFormat(pattern);
            return sd2.format(date);
        }
        catch (ParseException e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "日期转换出错"), e);
        }
        
    }
    
    /**
     * 获得当前季度
     * 
     * @return 数组0 - 当前季度的第三个月20号，数组1 - 当前季度的上一季度的第三个月21号
     */
    public static Long[] getQuarter()
    {
        
        // 获取年月
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + BobfintechContant.NUMBER_ONE;
        
        // 根据月获取季度数
        int quarter = getQuarter(month);
        
        // 当前季度的第三个月20号
        String date = MessageFormat.format(QUARTERS[quarter - BobfintechContant.NUMBER_ONE], String.valueOf(year));
        long currentQuarter = Long.parseLong(date);
        
        // 当前季度的上一季度的第三个月21号
        int y = (QUARTERS_EVERY_YEAR + BobfintechContant.NUMBER_ONE - quarter) / QUARTERS_EVERY_YEAR;
        year = year - y;
        quarter = y * QUARTERS_EVERY_YEAR + (quarter - BobfintechContant.NUMBER_ONE);
        String d = MessageFormat.format(QUARTERS[quarter - BobfintechContant.NUMBER_ONE], String.valueOf(year));
        long previousQuarter = Long.parseLong(d) + BobfintechContant.NUMBER_ONE;
        
        return new Long[] { currentQuarter, previousQuarter };
    }
    
    /**
     * 获得指定日期所在的季度
     * 
     * @param date
     *            日期
     * @return 数组0 - 当前季度的第三个月20号，数组1 - 当前季度的上一季度的第三个月21号
     */
    public static Long[] getQuarter(Long date)
    {
        // 获取年月
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getDate(date, "yyyyMMdd"));
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + BobfintechContant.NUMBER_ONE;
        // 根据月获取季度数
        int quarter = getQuarter(month);
        // 当前季度的第三个月20号
        String d = MessageFormat.format(QUARTERS[quarter - BobfintechContant.NUMBER_ONE], String.valueOf(year));
        long currentQuarter = Long.parseLong(d);
        // 当前季度的上一季度的第三个月21号
        int y = (QUARTERS_EVERY_YEAR + BobfintechContant.NUMBER_ONE - quarter) / QUARTERS_EVERY_YEAR;
        year = year - y;
        quarter = y * QUARTERS_EVERY_YEAR + (quarter - BobfintechContant.NUMBER_ONE);
        String dt = MessageFormat.format(QUARTERS[quarter - BobfintechContant.NUMBER_ONE], String.valueOf(year));
        long previousQuarter = Long.parseLong(dt) + BobfintechContant.NUMBER_ONE;
        return new Long[] { currentQuarter, previousQuarter };
    }
    
    /**
     * 通过月份计算季度
     * 
     * @param month
     *            月
     * @return
     */
    private static int getQuarter(int month)
    {
        

        if (month < BobfintechContant.NUMBER_ONE || month > BobfintechContant.NUMBER_TWELVE) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(), StringUtils
                .formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "月份无效！"));
        }
        return (month - BobfintechContant.NUMBER_ONE) / BobfintechContant.NUMBER_THREE + BobfintechContant.NUMBER_ONE;
    }
    
    /**
     * 获取日期
     * 
     * @param dateNum
     *            格式:yyyyMMdd、yyyy-MM-dd等自定义
     * @param format
     *            日期格式。
     * @return 日期Date
     * @throws BusinessException
     *             业务异常
     */
    public static Date getDate(long dateNum, String format) throws BusinessException
    {
        SimpleDateFormat sd1 = new SimpleDateFormat(format);
        Date date;
        try {
            date = sd1.parse(dateNum + "");
            return date;
        }
        catch (ParseException e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "日期转换出错"), e);
            
        }
    }
    
    /**
     * 判断输入日期所对应的年份是否为闰年
     *
     * @param date
     *            日期
     * @return
     */
    public static boolean isLeapYear(Long date)
    {
        
        // 如果输入的年份为空，则返回false
        if (null == date) {
            return false;
        }
        
        try {
            // 获取年份
            long y = getYear(date);
            if ((y % BobfintechContant.NUMBER_FOUR == BobfintechContant.NUMBER_ZERO
                 && y % BobfintechContant.NUMBER_ONE_HUNDRED != BobfintechContant.NUMBER_ZERO)
                || (y % BobfintechContant.NUMBER_FOUR_HUNDRED == BobfintechContant.NUMBER_ZERO)) {
                return true;
                
            }
        }
        catch (Exception e) {
            return false; // 如果输入非法的年份，则返回false
        }
        return false;
    }
    
    /**
     * 通过日期提取年份
     * 
     * @param dateNum
     *            日期
     * @return 年份
     * @throws BusinessException
     *             业务异常
     */
    public static long getYear(long dateNum) throws BusinessException
    {
        Calendar c = Calendar.getInstance();
        c.setTime(DateUtils.getDate(dateNum));
        return Long.valueOf(c.get(Calendar.YEAR));
    }
    
    /**
     * 通过日期提取年份
     * 
     * @param dateStr
     *            日期
     * @return 年份
     * @throws BusinessException
     *             业务异常
     */
    public static String getYear(String dateStr) throws BusinessException
    {
        Calendar c = Calendar.getInstance();
        c.setTime(string2Date(dateStr));
        return String.valueOf(c.get(Calendar.YEAR));
    }
    
    /**
     * 通过日期提取月日
     * 
     * @param dateStr
     *            日期yyyymmdd
     * @return 月日
     * @throws BusinessException
     *             业务异常
     */
    public static String getMonthDay(String dateStr) throws BusinessException
    {
        if (StringUtils.isBlank(dateStr) || dateStr.length() != BobfintechContant.NUMBER_EIGHT) {
            return "";
        }
        return dateStr.substring(BobfintechContant.NUMBER_FOUR);
    }
    
    /**
     * 获取日期
     * 
     * @param dateNum
     *            格式:yyyyMMdd
     * @return 日期Date
     * @throws BusinessException
     *             格式转化异常
     */
    public static Date getDate(long dateNum) throws BusinessException
    {
        return getDate(dateNum, "yyyyMMdd");
    }
    
    /**
     * 获取当前时间后N分钟后的时间
     *
     * @param minute
     *            分钟
     * @param formatString
     *            格式化
     * @return
     */
    public static String getCurrentAfterMinute(long minute, String formatString)
    {
        long current = System.currentTimeMillis();
        current += minute * BobfintechContant.NUMBER_SIXTY * BobfintechContant.NUMBER_ONE_THOUSAND;
        Date date = new Date(current);
        SimpleDateFormat dateFormat = new SimpleDateFormat(formatString);
        return dateFormat.format(date);
    }
    
    /**
     * 按照转换规则将日期字符串转换为Date类型的时间
     *
     * @param dateString
     *            要转换的日期字符串
     * @param format
     *            转换的格式，例如：YYYYMMDD
     * @return 转换后的Date类型的日期
     * @throws BusinessException
     *             异常
     */
    public static Date string2Date(String dateString, String format) throws BusinessException
    {
        SimpleDateFormat sd1 = new SimpleDateFormat(format);
        Date date;
        try {
            date = sd1.parse(dateString);
            return date;
        }
        catch (ParseException e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), "日期转换出错"), e);
        }
    }
    
}
/**
 * CHANGE HISTORY
 * M1 2018-08-10 chengls13783@hundsun.com Create
 */
