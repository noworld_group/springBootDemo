package cn.com.bobfintech.commons.entity.user;

import java.util.ArrayList;
import java.util.List;

import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.exception.BusinessException;
import cn.com.bobfintech.commons.utils.StringUtils;

/**
 * @project:用户管理模块
 * @description:角色菜单关系表
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 825338623@qq.com Create 1.0
 * @copyright ©2017-2019
 */
public class SysRoleResourceExample {

    
    /**orderByClause */
    protected String orderByClause;
    
    /**distinct */
    protected boolean distinct;
    
    /**oredCriteria */
    protected List<Criteria> oredCriteria;

    
    /**
     * 构造方法 
     */
    public SysRoleResourceExample() {
        oredCriteria = new ArrayList<Criteria>();
    }
    
    /**
     * @param orderByClause orderByClause
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }
    
    /**
     * @return String
     */
    public String getOrderByClause() {
        return orderByClause;
    }
    
    /**
     * @param distinct distinct
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }
    
    /**
     * @return boolean
     */
    public boolean isDistinct() {
        return distinct;
    }
    
    /**
     * @return List<Criteria>
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }
    
    /**
     * @param criteria criteria
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }
    
    /**
     * @return Criteria
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }
    
    /**
     * @return Criteria
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }
    
    /**
     * @return Criteria
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }
    
    /**
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * @project:用户管理模块
     * @description:角色菜单关系表
     * @version 1.0.0
     * @errorcode
     *            错误码: 错误描述
     * @author
     *         <li>2019-01-23 825338623@qq.com Create 1.0
     * @copyright ©2017-2019 中央结算公司，版权所有。 
     */
    protected abstract static class GeneratedCriteria {

        
        /**criteria */
        protected List<Criterion> criteria;

        
        /**
         * 构造方法 
         */
        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }
        
        /**
         * @return boolean
         */
        public boolean isValid() {
            return criteria.size() > 0;
        }
        
        /**
         * @return List<Criterion>
         */
        public List<Criterion> getAllCriteria() {
            return criteria;
        }
        
        /**
         * @return List<Criterion>
         */
        public List<Criterion> getCriteria() {
            return criteria;
        }
        
        /**
         * @param condition condition
         */
        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                    StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(),
                        "Value for condition cannot be null"));
            }
            criteria.add(new Criterion(condition));
        }
        
        /**
         * @param condition condition
         * @param value value
         * @param property property
         */
        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                    StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(),
                        "Value for condition cannot be null"));
            }
            criteria.add(new Criterion(condition, value));
        }
        
        /**
         * @param condition condition
         * @param value1 value1
         * @param value2 value2
         * @param property property
         */
        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                    StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(),
                        "Value for condition cannot be null"));
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
        /**
         * @return Criteria
         */
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andRoleIdIsNull() {
            addCriterion("role_id is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andRoleIdIsNotNull() {
            addCriterion("role_id is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRoleIdEqualTo(String value) {
            addCriterion("role_id =", value, "roleId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRoleIdNotEqualTo(String value) {
            addCriterion("role_id <>", value, "roleId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRoleIdGreaterThan(String value) {
            addCriterion("role_id >", value, "roleId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRoleIdGreaterThanOrEqualTo(String value) {
            addCriterion("role_id >=", value, "roleId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRoleIdLessThan(String value) {
            addCriterion("role_id <", value, "roleId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRoleIdLessThanOrEqualTo(String value) {
            addCriterion("role_id <=", value, "roleId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRoleIdLike(String value) {
            addCriterion("role_id like", value, "roleId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRoleIdNotLike(String value) {
            addCriterion("role_id not like", value, "roleId");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andRoleIdIn(List<String> values) {
            addCriterion("role_id in", values, "roleId");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andRoleIdNotIn(List<String> values) {
            addCriterion("role_id not in", values, "roleId");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andRoleIdBetween(String value1, String value2) {
            addCriterion("role_id between", value1, value2, "roleId");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andRoleIdNotBetween(String value1, String value2) {
            addCriterion("role_id not between", value1, value2, "roleId");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andResourceIdIsNull() {
            addCriterion("resource_id is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andResourceIdIsNotNull() {
            addCriterion("resource_id is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andResourceIdEqualTo(String value) {
            addCriterion("resource_id =", value, "resourceId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andResourceIdNotEqualTo(String value) {
            addCriterion("resource_id <>", value, "resourceId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andResourceIdGreaterThan(String value) {
            addCriterion("resource_id >", value, "resourceId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andResourceIdGreaterThanOrEqualTo(String value) {
            addCriterion("resource_id >=", value, "resourceId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andResourceIdLessThan(String value) {
            addCriterion("resource_id <", value, "resourceId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andResourceIdLessThanOrEqualTo(String value) {
            addCriterion("resource_id <=", value, "resourceId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andResourceIdLike(String value) {
            addCriterion("resource_id like", value, "resourceId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andResourceIdNotLike(String value) {
            addCriterion("resource_id not like", value, "resourceId");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andResourceIdIn(List<String> values) {
            addCriterion("resource_id in", values, "resourceId");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andResourceIdNotIn(List<String> values) {
            addCriterion("resource_id not in", values, "resourceId");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andResourceIdBetween(String value1, String value2) {
            addCriterion("resource_id between", value1, value2, "resourceId");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andResourceIdNotBetween(String value1, String value2) {
            addCriterion("resource_id not between", value1, value2, "resourceId");
            return (Criteria) this;
        }
    }
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/

    /**
     * @project:用户管理模块
     * @description:角色菜单关系表
     * @version 1.0.0
     * @errorcode
     *            错误码: 错误描述
     * @author
     *         <li>2019-01-23 825338623@qq.com Create 1.0
     * @copyright ©2017-2019 中央结算公司，版权所有。 
     */
    public static class Criteria extends GeneratedCriteria {


        
        /**
         * 构造方法 
         */
        protected Criteria() {
            super();
        }
    }
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/

    /**
     * @project:用户管理模块
     * @description:角色菜单关系表
     * @version 1.0.0
     * @errorcode
     *            错误码: 错误描述
     * @author
     *         <li>2019-01-23 825338623@qq.com Create 1.0
     * @copyright ©2017-2019 中央结算公司，版权所有。 
     */
    public static class Criterion {

        
        /**condition */
        private String condition;
        
        /**value */
        private Object value;
        
        /**secondValue */
        private Object secondValue;
        
        /**noValue */
        private boolean noValue;
        
        /**singleValue */
        private boolean singleValue;
        
        /**betweenValue */
        private boolean betweenValue;
        
        /**listValue */
        private boolean listValue;
        
        /**typeHandler */
        private String typeHandler;

        
        /**
         * @return String
         */
        public String getCondition() {
            return condition;
        }
        
        /**
         * @return Object
         */
        public Object getValue() {
            return value;
        }
        
        /**
         * @return Object
         */
        public Object getSecondValue() {
            return secondValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isNoValue() {
            return noValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isSingleValue() {
            return singleValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isBetweenValue() {
            return betweenValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isListValue() {
            return listValue;
        }
        
        /**
         * @return String
         */
        public String getTypeHandler() {
            return typeHandler;
        }
        
        /**
         * 构造方法 
         * @param condition condition
         */
        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         * @param typeHandler typeHandler
         */
        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         */
        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         * @param secondValue secondValue
         * @param typeHandler typeHandler
         */
        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         * @param secondValue secondValue
         */
        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/
}
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/