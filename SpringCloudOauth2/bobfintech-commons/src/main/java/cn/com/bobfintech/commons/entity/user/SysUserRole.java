package cn.com.bobfintech.commons.entity.user;

/**
 * @project:用户管理模块
 * @description:用户角色关系表
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 825338623@qq.com Create 1.0
 * @copyright ©2017-2019
 */
public class SysUserRole {

    
    /**主键 */
    private String id;
    
    /**用户ID */
    private String userId;
    
    /**角色ID */
    private String roleId;

    
    /**
     * @return 主键
     */
    public String getId() {
        return id;
    }
    
    /**
     * @param id 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }
    
    /**
     * @return 用户ID
     */
    public String getUserId() {
        return userId;
    }
    
    /**
     * @param userId 用户ID
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }
    
    /**
     * @return 角色ID
     */
    public String getRoleId() {
        return roleId;
    }
    
    /**
     * @param roleId 角色ID
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }
}
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/