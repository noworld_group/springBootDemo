package cn.com.bobfintech.commons.pojo;

import java.util.List;

import lombok.Data;

/**
 * @project: 北银金科
 * @description: 多条件查询
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-14 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Data
public class EsMultiQueryInfo
{
    // 索引列表
    private String[] indices;
    
    // 查询条件
    private List<QueryInfo> queryInfos;
    
}

