package cn.com.bobfintech.commons.entity.user;

import java.util.Date;

/**
 * @project:用户管理模块
 * @description:角色表
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 825338623@qq.com Create 1.0
 * @copyright ©2017-2019
 */
public class SysRole {

    
    /**主键 */
    private String id;
    
    /**角色名称 */
    private String roleName;
    
    /**角色描述 */
    private String roleDesc;
    
    /**角色编码， 只能是英文 */
    private String roleCode;
    
    /**停用0启用1 */
    private Byte roleStatus;
    
    /**角色类型：0系统默认，1用户创建 */
    private Byte roleType;
    
    /**所属组织机构代码 */
    private String orgCode;
    
    /**版本号 */
    private Integer version;
    
    /**创建时间 */
    private Date createdDt;
    
    /**创建人UID */
    private String creatorUid;
    
    /**更新时间 */
    private Date updateDt;
    
    /**更新人UID */
    private String updatorUid;
    
    /**是否删除,0:未删除;1:已删除; */
    private Boolean deleted;

    
    /**
     * @return 主键
     */
    public String getId() {
        return id;
    }
    
    /**
     * @param id 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }
    
    /**
     * @return 角色名称
     */
    public String getRoleName() {
        return roleName;
    }
    
    /**
     * @param roleName 角色名称
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }
    
    /**
     * @return 角色描述
     */
    public String getRoleDesc() {
        return roleDesc;
    }
    
    /**
     * @param roleDesc 角色描述
     */
    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc == null ? null : roleDesc.trim();
    }
    
    /**
     * @return 角色编码， 只能是英文
     */
    public String getRoleCode() {
        return roleCode;
    }
    
    /**
     * @param roleCode 角色编码， 只能是英文
     */
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode == null ? null : roleCode.trim();
    }
    
    /**
     * @return 停用0启用1
     */
    public Byte getRoleStatus() {
        return roleStatus;
    }
    
    /**
     * @param roleStatus 停用0启用1
     */
    public void setRoleStatus(Byte roleStatus) {
        this.roleStatus = roleStatus;
    }
    
    /**
     * @return 角色类型：0系统默认，1用户创建
     */
    public Byte getRoleType() {
        return roleType;
    }
    
    /**
     * @param roleType 角色类型：0系统默认，1用户创建
     */
    public void setRoleType(Byte roleType) {
        this.roleType = roleType;
    }
    
    /**
     * @return 所属组织机构代码
     */
    public String getOrgCode() {
        return orgCode;
    }
    
    /**
     * @param orgCode 所属组织机构代码
     */
    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode == null ? null : orgCode.trim();
    }
    
    /**
     * @return 版本号
     */
    public Integer getVersion() {
        return version;
    }
    
    /**
     * @param version 版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }
    
    /**
     * @return 创建时间
     */
    public Date getCreatedDt() {
        return createdDt == null ? null : (Date) createdDt.clone();
    }
    
    /**
     * @param createdDt 创建时间
     */
    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt == null ? null : (Date) createdDt.clone();
    }
    
    /**
     * @return 创建人UID
     */
    public String getCreatorUid() {
        return creatorUid;
    }
    
    /**
     * @param creatorUid 创建人UID
     */
    public void setCreatorUid(String creatorUid) {
        this.creatorUid = creatorUid == null ? null : creatorUid.trim();
    }
    
    /**
     * @return 更新时间
     */
    public Date getUpdateDt() {
        return updateDt == null ? null : (Date) updateDt.clone();
    }
    
    /**
     * @param updateDt 更新时间
     */
    public void setUpdateDt(Date updateDt) {
        this.updateDt = updateDt == null ? null : (Date) updateDt.clone();
    }
    
    /**
     * @return 更新人UID
     */
    public String getUpdatorUid() {
        return updatorUid;
    }
    
    /**
     * @param updatorUid 更新人UID
     */
    public void setUpdatorUid(String updatorUid) {
        this.updatorUid = updatorUid == null ? null : updatorUid.trim();
    }
    
    /**
     * @return 是否删除,0:未删除;1:已删除;
     */
    public Boolean getDeleted() {
        return deleted;
    }
    
    /**
     * @param deleted 是否删除,0:未删除;1:已删除;
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/