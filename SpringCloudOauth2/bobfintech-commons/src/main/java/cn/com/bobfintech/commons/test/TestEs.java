package cn.com.bobfintech.commons.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.lucene.search.TotalHits;
import org.elasticsearch.action.search.MultiSearchRequest;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.util.StringUtils;

import cn.com.bobfintech.commons.config.ElasticsearchConfig;
import cn.com.bobfintech.commons.constant.BobfintechContant;
import cn.com.bobfintech.commons.pojo.EsMultiQueryInfo;
import cn.com.bobfintech.commons.pojo.QueryInfo;


public class TestEs
{
    
    private RestHighLevelClient client;
    public static void main(String[] args)
    {
        ElasticsearchConfig elasticsearchConfig = new ElasticsearchConfig();
        elasticsearchConfig.setEsUris(new String[] { "http://localhost:9200" });
        elasticsearchConfig.setTimeout(1000);
        TestEs testEs = new TestEs();
        try {
            // Map<String, Object> searchOnParamByPage =
            // testEs.searchOnParamByPage(elasticsearchConfig,
            // new String[] { "testuserinfo" }, "createDate",
            // "userName",
            // "李四", 0, 12);
            // System.out.println(searchOnParamByPage);
            
            String[] indexString = new String[] { "testuserinfo" };
            QueryInfo queryInfo = new QueryInfo();
            queryInfo.setParamKey("userName");
            queryInfo.setParamVal("李四");
            EsMultiQueryInfo esMultiQueryInfo = new EsMultiQueryInfo();
            esMultiQueryInfo.setIndices(indexString);
            esMultiQueryInfo.setQueryInfos(Arrays.asList(queryInfo));
            Map<String, Object> searchOnParamByPage = testEs.testMultiSearch(elasticsearchConfig,
                Arrays.asList(esMultiQueryInfo));
            System.out.println(searchOnParamByPage);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 多条件查询ES
     *
     * @param elasticsearchConfig
     *            配置
     * @param paramList
     *            参数列表
     * @throws IOException
     */
    public Map<String, Object> testMultiSearch(ElasticsearchConfig elasticsearchConfig,
                                               List<EsMultiQueryInfo> queryInfos) throws IOException
    {
        MultiSearchRequest request = new MultiSearchRequest();
        if (CollectionUtils.isEmpty(queryInfos)) {
            return null;
        }
        SearchRequest firstSearchRequest = null;
        SearchSourceBuilder searchSourceBuilder = null;
        for (EsMultiQueryInfo esMultiQueryInfo : queryInfos) {
            firstSearchRequest = new SearchRequest();
            searchSourceBuilder = new SearchSourceBuilder();
            firstSearchRequest.indices(esMultiQueryInfo.getIndices());
            List<QueryInfo> queryInfoList = esMultiQueryInfo.getQueryInfos();
            if (CollectionUtils.isNotEmpty(queryInfoList)) {
                for (QueryInfo queryInfo : queryInfoList) {
                    searchSourceBuilder
                        .query(QueryBuilders.matchQuery(queryInfo.getParamKey(), queryInfo.getParamVal()));
                }
            }
            firstSearchRequest.source(searchSourceBuilder);
            request.add(firstSearchRequest);
        }
        
        if (client == null) {
            initClient(elasticsearchConfig);
        }
        MultiSearchResponse response = client.msearch(request, RequestOptions.DEFAULT);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        response.forEach(t -> {
            // Map<String, Object> sourceMap = null;
            SearchResponse resp = t.getResponse();
            Arrays.stream(resp.getHits().getHits()).forEach(searchHit -> {
                Map<String, Object> sourceMap = searchHit.getSourceAsMap();
                resultList.add(sourceMap);
            });
        });
        
        closeClient();
        resultMap.put(BobfintechContant.DATA_LIST, resultList);
        resultMap.put(BobfintechContant.DATA_SIZE, resultList.size());
        return resultMap;
    }
    
    /**
     * Java High Level REST Client 初始化
     */
    private void initClient(ElasticsearchConfig elasticsearchConfig)
    {
        String[] esUris = elasticsearchConfig.getEsUris();
        HttpHost[] httpHosts = new HttpHost[esUris.length];
        // 将地址转换为http主机数组，未配置端口则采用默认9200端口，配置了端口则用配置的端口
        for (int i = 0; i < httpHosts.length; i++) {
            if (!StringUtils.isEmpty(esUris[i])) {
                if (esUris[i].contains(":")) {
                    String[] uris = esUris[i].split(":");
                    String url = null;
                    if (uris[1].contains("/")) {
                        url = uris[1].substring(uris[1].lastIndexOf("/") + 1);
                        System.out.println("剪切后的urils" + url);
                    }
                    else {
                        url = uris[1];
                    }
                    httpHosts[i] = new HttpHost(url, Integer.parseInt(uris[2]), uris[0]);
                }
                else {
                    httpHosts[i] = new HttpHost(esUris[i], 9200, "http");
                }
            }
        }
        // 判断，如果未配置用户名，则进行无用户名密码连接，配置了用户名，则进行用户名密码连接
        if (StringUtils.isEmpty(elasticsearchConfig.getUserName())) {
            client = new RestHighLevelClient(RestClient.builder(httpHosts));
        }
        else {
            final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(elasticsearchConfig.getUserName(), elasticsearchConfig.getPassword()));
            client = new RestHighLevelClient(
                RestClient.builder(httpHosts).setHttpClientConfigCallback((httpClientBuilder) -> {
                    // 这里可以设置一些参数，比如cookie存储、代理等等
                    httpClientBuilder.disableAuthCaching();
                    return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                }).setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback()
                {
                    @Override
                    public RequestConfig.Builder customizeRequestConfig(RequestConfig.Builder requestConfigBuilder)
                    {
                        return requestConfigBuilder.setConnectTimeout(elasticsearchConfig.getTimeout())
                            .setSocketTimeout(elasticsearchConfig.getTimeout());
                    }
                }));
        }
    }
    
    /**
     * 单条件分页查询
     *
     * @param elasticsearchConfig
     *            配置
     * @param indices
     *            索引（非必填）
     * @param sortField
     *            排序字段
     * @param fieldKey
     *            查询条件(KEY)
     * @param fieldVal
     *            查询条件(value)
     * @param from
     *            开始条数
     * @param size
     *            要查询的条数
     * @throws IOException
     */
    public Map<String, Object> searchOnParamByPage(ElasticsearchConfig elasticsearchConfig, String[] indices,
                                                   String sortField, String fieldKey, String fieldVal, int from,
                                                   int size) throws IOException
    {
        SearchRequest searchRequest = new SearchRequest(indices);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.matchPhraseQuery(fieldKey, fieldVal));
        sourceBuilder.from(from);
        sourceBuilder.size(size);
        sourceBuilder.timeout(new TimeValue(1000));
        sourceBuilder.trackTotalHits(true);
        sourceBuilder.sort(new FieldSortBuilder(sortField + ".keyword").order(SortOrder.DESC));
        searchRequest.source(sourceBuilder);
        
        if (client == null) {
            initClient(elasticsearchConfig);
        }
        
        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchResponse.getHits();
        TotalHits totalHits = hits.getTotalHits();
        long totalSize = totalHits.value;
        SearchHit[] searchHits = hits.getHits();
        
        Map<String, Object> resultMap = new HashMap<String, Object>();
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        Map<String, Object> sourceMap = null;
        for (SearchHit searchHit : searchHits) {
            sourceMap = searchHit.getSourceAsMap();
            resultList.add(sourceMap);
        }
        closeClient();
        resultMap.put(BobfintechContant.DATA_LIST, resultList);
        resultMap.put(BobfintechContant.DATA_SIZE, totalSize);
        return resultMap;
    }
    
    /**
     * 关闭ES客户端
     */
    public void closeClient()
    {
        try {
            if (client != null) {
                client.close();
            }
            client = null;
        }
        catch (Exception e2) {
            e2.printStackTrace();
            System.out.println("关闭客户端失败");
        }
    }
}
