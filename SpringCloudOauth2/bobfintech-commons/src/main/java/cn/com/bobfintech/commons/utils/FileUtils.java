package cn.com.bobfintech.commons.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;

/**
 * 功能说明: 文件工具<br>
 * 注意事项: 类似其他可参考<br>
 * org.apache.commons.io.FileUtils<br>
 * org.apache.commons.io.IOUtils<br>
 * org.apache.commons.io.CopyUtils<br>
 * 系统版本: v1.0<br>
 * 开发人员: <br>
 * 开发时间: <br>
 */

/**
 * @project: 北银金科
 * @description: 文件工具。
 *               注意事项: 类似其他可参考<br>
 *               org.apache.commons.io.FileUtils<br>
 *               org.apache.commons.io.IOUtils<br>
 *               org.apache.commons.io.CopyUtils<br>
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-03 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Slf4j
public class FileUtils extends org.apache.commons.io.FileUtils
{
    
    /**
     * 从指定位置读取文件内容,并写到输出流
     * 
     * @param filePath
     *            文件全路径
     * @param output
     *            由调用者负责关闭
     */
    public static void writeToOutput(String filePath, OutputStream output)
    {
        InputStream input = null;
        try {
            input = new BufferedInputStream(new FileInputStream(new File(filePath)));
            IOUtils.copy(input, output);
            output.flush();
        }
        catch (Exception e) {
            String msg = StringUtils.formatStr("操作文件[{0}]失败", filePath);
            throw new BusinessException(
                BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR
                .getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), msg), e);
        }
        finally {
            try {
                if (input != null) {
                    input.close();
                }
            }
            catch (Exception e) {
                LogsUtils.errorGlobal(log, null, "文件流关闭异常", e);
            }
        }
    }
    
    /**
     * 从输入流读取内容，并写入到指定文件
     * 
     * @param input
     *            由调用者负责关闭
     * @param dir
     *            指定文件目录
     * @param fileName
     *            指定文件名(包括后缀名)
     */
    public static File writeToFile(InputStream input, String dir, String fileName)
    {
        OutputStream output = null;
        try {
            File dest = getFile(dir, fileName);
            output = new BufferedOutputStream(new FileOutputStream(dest));
            IOUtils.copy(input, output);
            return dest;
        }
        catch (Exception e) {
            String msg = StringUtils.formatStr("操作文件[{0}/{1}]失败", dir, fileName);
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), msg), e);
        }
        finally {
            try {
                if (output != null) {
                    output.close();
                }
            }
            catch (Exception e) {
                LogsUtils.errorGlobal(log, null, "文件流关闭异常", e);
            }
        }
    }
    
    /**
     * 从输入流读取内容，并写入到指定文件
     * 
     * @param data
     *            数据
     * @param dir
     *            指定文件目录
     * @param fileName
     *            指定文件名(包括后缀名)
     */
    public static File writeToFile(byte[] data, String dir, String fileName)
    {
        OutputStream output = null;
        try {
            File dest = getFile(dir, fileName);
            output = new BufferedOutputStream(new FileOutputStream(dest));
            IOUtils.write(data, output);
            return dest;
        }
        catch (Exception e) {
            String msg = StringUtils.formatStr("操作文件[{0}/{1}]失败", dir, fileName);
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(), msg), e);
        }
        finally {
            try {
                if (output != null) {
                    output.close();
                }
            }
            catch (Exception e) {
                LogsUtils.errorGlobal(log, null, "文件流关闭异常", e);
            }
        }
    }
    
    /**
     * 获取文件对象,并将不存在的一系列父目录创建
     * 
     * @param dir
     *            目录
     * @param fileName
     *            文件名称
     * @return
     */
    public static File getFile(String dir, String fileName)
    {
        File dest = new File(dir + File.separator + fileName);
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        return dest;
    }
    
    /**
     * 从文件全路径中截取文件名(带后缀名)
     * 
     * @param filePath
     *            路径
     * @return
     */
    public static String getFileName(String filePath)
    {
        int slashIndex = filePath.lastIndexOf(File.separator);
        return slashIndex > -1 ? filePath.substring(slashIndex + 1) : filePath;
    }
    
    /**
     * 获取根目录下面的所有文件
     *
     * @param dirFile
     *            根目录
     * @param filter
     *            过滤器
     * @return
     */
    public static List<File> getAllFiles(File dirFile, FileFilter filter)
    {
        List<File> ls = new ArrayList<>();
        if (dirFile == null) {
            return ls;
        }
        
        if (dirFile.isFile()) {
            ls.add(dirFile);
        }
        else {
            File[] listFiles;
            if (filter != null) {
                listFiles = dirFile.listFiles(filter);
            }
            else {
                listFiles = dirFile.listFiles();
            }
            if (listFiles != null) {
                for (File file : listFiles) {
                    ls.addAll(getAllFiles(file, filter));
                }
            }
        }
        return ls;
    }
}
