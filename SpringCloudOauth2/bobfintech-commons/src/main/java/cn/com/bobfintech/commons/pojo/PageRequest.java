package cn.com.bobfintech.commons.pojo;

import lombok.Data;

/**
 * @ClassName: PageRequest
 * @Description: 分页请求  可以用作父类，子类继承后增加自定义属性
 * Author:  Jiaxi
 * Date: 2020/6/30 15:46
 */
@Data
public class PageRequest {
    /**
     * 当前页码
     */
    private int pageNum;
    /**
     * 每页数量
     */
    private int pageSize;


}