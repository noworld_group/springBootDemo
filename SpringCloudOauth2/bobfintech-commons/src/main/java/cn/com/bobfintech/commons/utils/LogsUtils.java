package cn.com.bobfintech.commons.utils;

import org.slf4j.Logger;

import cn.com.bobfintech.commons.exception.BusinessException;

/**
 * @project: 北银金科
 * @description: 日志记录工具类。
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-03 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
public class LogsUtils
{
    /** 日志信息模板 */
    private static final String GLOBAL_MSG_TEMP = "[global_id={}][msg_id={}]"
                                                  + "[trans_id={}][msg_type={}][userMessage={0}]";
    
    /** 失败日志信息模板 */
    private static final String ERR_MSG_TEMP = "错误码：{0}；原生错误信息：{1}；业务提示信息：{2}";
    
    /** 信息占位符 */
    private static final String MSG_REPLACE_CHARS = "{0}";
    
    /**
     * 带全局流水号的info日志打印
     *
     * @param log
     *            日志打印对象
     * @param paramString
     *            打印内容
     * @param paramArrayOfObject
     *            内容参数
     */
    public static void infoGlobal(Logger log, String paramString, Object... paramArrayOfObject)
    {
        log.info(getNewMessage(paramString), paramArrayOfObject);
    }
    
    /**
     * 带全局流水号的debug日志打印
     *
     * @param log
     *            日志打印对象
     * @param paramString
     *            打印内容
     * @param paramArrayOfObject
     *            内容参数
     */
    public static void debugGlobal(Logger log, String paramString, Object... paramArrayOfObject)
    {
        log.debug(getNewMessage(paramString), paramArrayOfObject);
    }
    
    /**
     * 带全局流水号的error日志打印
     *
     * @param log
     *            日志打印对象
     * @param paramString
     *            打印内容
     * @param paramArrayOfObject
     *            内容参数
     */
    public static void errorGlobal(Logger log, String paramString, Object... paramArrayOfObject)
    {
        // 错误信息参数
        Object[] errorParams = new String[] { "", "", paramString };
        try {
            Object[] params = paramArrayOfObject;
            if (params != null && params.length > 0 && params[params.length - 1] instanceof Throwable) {
                Throwable t = (Throwable) params[(params.length - 1)];
                errorParams[1] = t.getMessage();
                if (t instanceof BusinessException) {
                    BusinessException bt = (BusinessException) t;
                    errorParams[0] = bt.getErrorCode();
                }
            }
        }
        catch (Exception e) {
            LogsUtils.errorGlobal(log, null, "日志打印未知异常", e);
        }
        paramString = StringUtils.formatStr(ERR_MSG_TEMP, errorParams);
        log.error(getNewMessage(paramString), paramArrayOfObject);
    }
    
    /**
     * 带全局流水号的warn日志打印
     *
     * @param log
     *            日志打印对象
     * @param paramString
     *            打印内容
     * @param paramArrayOfObject
     *            内容参数
     */
    public static void warnGlobal(Logger log, String paramString, Object... paramArrayOfObject)
    {
        LogsUtils.errorGlobal(log, null, getNewMessage(paramString), paramArrayOfObject);
    }
    
    /**
     * 是否debug级别
     *
     * @param log
     *            日志打印对象
     * @return
     */
    public static boolean isDebugEnabled(Logger log)
    {
        return log.isDebugEnabled();
    }
    
    /**
     * 获取全的日志信息内容
     *
     * @param paramString
     *            日志内容
     * @return
     */
    private static String getNewMessage(String paramString)
    {
        return GLOBAL_MSG_TEMP.replace(MSG_REPLACE_CHARS, paramString);
    }
    
}
