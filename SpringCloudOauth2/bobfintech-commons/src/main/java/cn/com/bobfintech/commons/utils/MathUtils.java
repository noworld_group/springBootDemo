package cn.com.bobfintech.commons.utils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import cn.com.bobfintech.commons.constant.BobfintechContant;

/**
 * @project: 北银金科
 * @description: 数学计算工具类。
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-03 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
public class MathUtils
{
    
    /**
     * 数字减法：src1-src2
     * 
     * @param src1
     *            第一个值
     * @param src2
     *            第二各值
     * @return
     */
    public static BigDecimal dataDecrease(BigDecimal src1, BigDecimal src2)
    {
        if (src1 == null || src2 == null) {
            return null;
        }
        return src1.subtract(src2);
    }
    
    /**
     * 数字加法：src1+src2
     * 
     * @param src1
     *            第一个值
     * @param src2
     *            第二各值
     * @return
     */
    public static BigDecimal dataAdd(BigDecimal src1, BigDecimal src2)
    {
        if (src1 == null || src2 == null) {
            return null;
        }
        return src1.add(src2);
    }
    
    /**
     * 转成以【万元】为单位，且使用ROUND_HALF_UP模式
     * 
     * @param src
     *            参数
     * @return
     */
    public static BigDecimal dataToWanYuan(BigDecimal src)
    {
        if (src == null) {
            return null;
        }
        src = src.divide(new BigDecimal(BobfintechContant.NUMBER_TEN_THOUSAND),
            BobfintechContant.NUMBER_SIX,
            BigDecimal.ROUND_HALF_UP);
        return src;
    }
    
    /**
     * 转成以【亿元】为单位，且使用ROUND_HALF_UP模式
     * 
     * @param src
     *            参数
     * @return
     */
    public static BigDecimal dataToYiYuan(BigDecimal src)
    {
        if (src == null) {
            return null;
        }
        src = src.divide(new BigDecimal(100000000), BobfintechContant.NUMBER_SIX, BigDecimal.ROUND_HALF_UP);
        return src;
    }
    
    /**
     * 乘以【100】并保留四位小数
     * 
     * @param src
     *            参数
     * @return
     */
    public static BigDecimal getBigDecimal(BigDecimal src)
    {
        if (src == null) {
            return null;
        }
        src = src.multiply(new BigDecimal("100")).setScale(4, RoundingMode.HALF_DOWN);
        return src;
    }
    
    /**
     * 获取格式化数值
     *
     * @param str
     *            数值字符串
     * @return
     */
    public static String getFormatString(String str)
    {
        BigDecimal b = new BigDecimal(str);
        DecimalFormat d1 = new DecimalFormat("#,##0.00");
        String dd = d1.format(b);
        return dd;
    }
    
    /**
     * 获取价格数值
     *
     * @param b
     *            数值字符串
     * @return
     */
    public static String getPriceFormat(BigDecimal b)
    {
        DecimalFormat d1 = new DecimalFormat("#,##0.000");
        String dd = d1.format(b);
        return dd;
    }
    
    /**
     * 两者相等(忽略null和0)
     * 
     * @param obj1
     *            对象1
     * @param obj2
     *            对象2
     * @return
     */
    public static boolean isEqualFiltterBlank(Long obj1, Long obj2)
    {
        if ((obj1 == null || obj1.longValue() == 0) && (obj2 == null || obj2.longValue() == 0)) {
            return true;
        }
        if (obj1 != null && obj2 != null && obj1.longValue() == obj2.longValue()) {
            return true;
        }
        return false;
    }
    
    /**
     * 两者要么都为空(忽略null和0)， 要么都不为空
     * 
     * @param obj1
     *            对象1
     * @param obj2
     *            对象2
     * @return
     */
    public static boolean isLikeFiltterBlank(Long obj1, Long obj2)
    {
        if ((obj1 == null || obj1.longValue() == 0) && (obj2 == null || obj2.longValue() == 0)) {
            return true;
        }
        if (obj1 != null && obj2 != null) {
            return true;
        }
        return false;
    }
    
    /**
     * 所有的入参都不小于零（忽略null和0）
     * 
     * @param argms
     *            参数集合
     * @return
     */
    public static boolean isNotLessZeroFiltterBlank(Long... argms)
    {
        for (Long temp : argms) {
            if (temp != null && temp.compareTo(0l) < 0) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * 两者必须其中一个为空或为0，一个不为空或大于0
     * 
     * @param obj1
     *            对象1
     * @param obj2
     *            对象2
     * @return
     */
    public static boolean isNotBlankOfOne(Long obj1, Long obj2)
    {
        if ((obj1 == null || obj1.longValue() == 0) && (obj2 != null && obj2.longValue() > 0)) {
            return true;
        }
        if ((obj2 == null || obj2.longValue() == 0) && (obj1 != null && obj1.longValue() > 0)) {
            return true;
        }
        return false;
    }
    
    /**
     * 数值转换成元
     *
     * @param src
     *            数值入参
     * @return
     */
    public static String dataToYuan(Object src)
    {
        BigDecimal bigd = BigDecimal.ZERO;
        if (src == null) {
            return "0.00";
        }
        if (src instanceof Long) {
            bigd = BigDecimal.valueOf((Long) src);
        }
        else if (src instanceof String) {
            if (StringUtils.isNotBlank((String) src)) {
                bigd = new BigDecimal((String) src);
            }
            else {
                return "0.00";
            }
            
        }
        else if (src instanceof Integer) {
            bigd = new BigDecimal(src + "");
        }
        else if (src instanceof BigDecimal) {
            bigd = (BigDecimal) src;
        }
        src = bigd.divide(new BigDecimal(BobfintechContant.NUMBER_ONE_HUNDRED), 2, BigDecimal.ROUND_HALF_UP);
        return getFormatString(src.toString());
    }
    
    /**
     * 环比增减
     * (begin-end)/end*100 保留两位小数
     * 
     * @param begin
     *            第一个值
     * @param end
     *            第二个值
     * @return
     */
    public static String reckonMom(Long begin, Long end)
    {
        if (begin == null) {
            begin = 0L;
        }
        if (end == null) {
            end = 0L;
        }
        if (begin.intValue() == 0 && end.intValue() == 0) {
            return "0.00";
        }
        if (begin.intValue() != 0 && end.intValue() == 0) {
            return "&infin;";
        }
        double beginD = Double.valueOf(begin);
        double endD = Double.valueOf(end);
        DecimalFormat df = new DecimalFormat("0.00"); // 格式化小数
        return df.format((beginD - endD) / endD * BobfintechContant.NUMBER_ONE_HUNDRED);
    }
    
    /**
     * 字符串转BigDecimal
     *
     * @param obj
     *            数值入参
     * @return
     */
    public BigDecimal strToBigDecimal(String obj)
    {
        if (StringUtils.isBlank(obj)) {
            return BigDecimal.ZERO;
        }
        return new BigDecimal(obj);
    }
    
    /*******************************************
     * BigDecimal类型的乘除运算
     ****************************************/
    /**
     * 乘法运算，返回精确结果
     * 
     * @param dea
     *            第一个数值
     * @param deb
     *            第二个数值
     * @return
     */
    public static BigDecimal multi(BigDecimal dea, BigDecimal deb)
    {
        if (dea == null || deb == null) {
            return null;
        }
        return dea.multiply(deb, MathContext.UNLIMITED);
    }
    
    /**
     * 转换为【百分比】，保留两位小数
     * 
     * @param bd
     *            数值入参
     * @return
     */
    public static BigDecimal percentage(BigDecimal bd)
    {
        if (bd == null) {
            return null;
        }
        return bd.multiply(new BigDecimal("100")).setScale(2, RoundingMode.HALF_DOWN);
    }
    
    /**
     * 除法运算，dividend/divisor，默认保留4位有效数字，四舍五入
     * 
     * @param dividend
     *            被除数
     * @param divisor
     *            除数
     * @return
     */
    public static BigDecimal divideWith(BigDecimal dividend, BigDecimal divisor)
    {
        return divideWith(dividend, divisor, BobfintechContant.NUMBER_FOUR, BigDecimal.ROUND_HALF_UP);
    }
    
    /**
     * 环比增减，返回值为百分比形式，保留两位小数
     * 
     * @param begin
     *            开始值
     * @param end
     *            结束值
     * @return
     */
    public static BigDecimal reckonMom(BigDecimal begin, BigDecimal end)
    {
        if (begin == null | end == null || end.equals(BigDecimal.ZERO)) {
            return null;
        }
        return percentage(divideWith(dataDecrease(begin, end), end));
    }
    
    /**
     * 除法运算，dividend/divisor，
     * 舍入模式：
     * [0]ROUND_UP向远离0方向舍入
     * [1]ROUND_DOWN向0方向舍入
     * [2]ROUND_CEILING向正无穷方向舍入
     * [3]ROUND_FLOOR向负无穷方向舍入
     * [4]ROUND_HALF_UP向距离最近的一边舍入，如果两边距离相等，向上舍入, 1.55保留一位小数结果为1.6
     * [5]ROUND_HALF_DOWN向距离最近的一边舍入，如果两边距离相等，向下舍入, 1.55保留一位小数结果为1.5
     * [6]ROUND_HALF_EVEN向距离最近的一边舍入，如果两边距离相等，如果保留位数是奇数，使用ROUND_HALF_UP ，如果是偶数，使用ROUND_HALF_DOWN
     * [7]ROUND_UNNECESSARY计算结果是精确的，不需要舍入
     * 
     * @param dividend
     *            被除数
     * @param divisor
     *            除数
     * @param scale
     *            保留有效位数
     * @param roundingMode
     *            舍入模式
     * @return
     */
    public static BigDecimal divideWith(BigDecimal dividend, BigDecimal divisor, int scale, int roundingMode)
    {
        if (dividend == null || divisor == null || divisor.equals(BigDecimal.ZERO)) {
            return null;
        }
        return dividend.divide(divisor, scale, roundingMode);
    }
    /*******************************************
     * BigDecimal类型的乘除运算
     ****************************************/
    
}