package cn.com.bobfintech.commons.utils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;

/**
 * @project: 北银金科
 * @description: list集合工具类。
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-03 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Slf4j
public class ListUtils extends org.apache.commons.collections.ListUtils
{
    
    /**
     * list比较，不支持重复对象比较，比较所有要素，不比较顺序
     * 
     * @param <T>
     *            t
     * @param l1
     *            对象型list1
     * @param l2
     *            对象型list2
     * @return
     *         true-完全一致，false-有不一致
     */
    public static <T> boolean compareAll(List<? super T> l1, List<? super T> l2)
    {
        if (l1 == null || l2 == null) {
            return false;
        }
        if (l1.size() != l2.size()) {
            return false;
        }
        if (l1.get(0) instanceof Map) {
            LogsUtils.warnGlobal(log, null, "暂不支持map类型数据比较");
            return false;
        }
        Field[] fields = l1.get(0).getClass().getDeclaredFields();
        String[] cols = new String[fields.length];
        for (int i = 0; i < fields.length; i++) {
            cols[i] = fields[i].getName();
        }
        return compare(l1, l2, cols);
    }
    
    /**
     * list比较，不支持重复对象比较，不比较顺序
     * 
     * @param <T>
     *            t
     * @param l1
     *            对象型list1
     * @param l2
     *            对象型list2
     * @param cols
     *            需要比较的要素，不可为空，驼峰形式
     * @return
     *         true-完全一致，false-有不一致
     */
    public static <T> boolean compare(List<? super T> l1, List<? super T> l2, String... cols)
    {
        if (l1 == null || l2 == null) {
            return false;
        }
        if (l1.size() != l2.size()) {
            return false;
        }
        if (cols == null || cols.length == 0) {
            LogsUtils.warnGlobal(log, null, "需要比较的要素不能为空");
            return false;
        }
        if (l1.get(0) instanceof Map) {
            LogsUtils.warnGlobal(log, null, "暂不支持map类型数据比较");
            return false;
        }
        try {
            return l1.stream().allMatch(t1 -> l2.stream().anyMatch(t2 -> compare(t1, t2, cols)))
                   && l2.stream().allMatch(t2 -> l1.stream().anyMatch(t1 -> compare(t2, t1, cols)));
        }
        catch (Exception e) {
            LogsUtils.warnGlobal(log, null, "只支持实体类类型的list比较");
            throw e;
        }
    }
    
    /**
     * 方法说明：2个列表对象进行匹配，如果条目数不一致，返回all，否则返回不相同字段名称
     *
     * @param org1
     *            第一个list
     * @param org2
     *            第二个list
     * @param noNeedFieldList
     *            无需匹配的属性名称
     * @return
     * @throws BusinessException
     *             异常
     */
    public static String checkListCommonNoNeed(List<?> org1, List<?> org2,
                                               List<String> noNeedFieldList) throws BusinessException
    {
        if ((CollectionUtils.isEmpty(org1) && CollectionUtils.isNotEmpty(org2))
            || (CollectionUtils.isNotEmpty(org1) && CollectionUtils.isEmpty(org2))
            || (CollectionUtils.isNotEmpty(org1) && CollectionUtils.isNotEmpty(org2) && org1.size() != org2.size())) {
            return "all";
        }
        if (CollectionUtils.isNotEmpty(org1)) {
            for (int i = 0; i < org1.size(); i++) {
                Object tmp1 = org1.get(i);
                Object tmp2 = org2.get(i);
                String msg = CommonUtils.checkCommon(tmp1, tmp2, noNeedFieldList);
                if (StringUtils.isNotBlank(msg)) {
                    return msg;
                }
            }
        }
        
        return null;
    }
    
    
    /**
     * 对象比较
     * 
     * @param <T>
     *            t
     * @param t1
     *            对象1
     * @param t2
     *            对象2
     * @param cols
     *            比较属性集合
     */
    private static <T> boolean compare(T t1, T t2, String[] cols)
    {
        try {
            boolean b = true;
            for (String col : cols) {
                Object field1 = FieldUtils.readDeclaredField(t1, col, true);
                Object field2 = FieldUtils.readDeclaredField(t2, col, true);
                if (field1 == null && field2 == null) {
                    continue;
                }
                else if (field1 != null && field2 != null) {
                    if (field1 instanceof BigDecimal) {
                        BigDecimal d1 = (BigDecimal) field1;
                        BigDecimal d2 = (BigDecimal) field2;
                        b = b && (d1.compareTo(d2) == 0);
                    }
                    else if (field1 instanceof Long) {
                        Long l1 = (Long) field1;
                        Long l2 = (Long) field2;
                        b = b && (l1.compareTo(l2) == 0);
                    }
                    else {
                        b = b && field1.toString().equals(field2.toString());
                    }
                    if (!b) {
                        break;
                    }
                }
                else {
                    b = false;
                    break;
                }
            }
            return b;
        }
        catch (Exception e) {
            LogsUtils.errorGlobal(log, null, "对象比较异常", e);
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo());
        }
    }
    
    /**
     * list比较，只比较size大小是否一样
     * 
     * @param <T>
     *            t
     * @param l1
     *            对象型list1
     * @param l2
     *            对象型list2
     * @return
     *         true-size一样，false-size不一致
     */
    public static <T> boolean compareListSize(List<? super T> l1, List<? super T> l2)
    {
        return CollectionUtils.size(l1) == CollectionUtils.size(l2);
    }
}
