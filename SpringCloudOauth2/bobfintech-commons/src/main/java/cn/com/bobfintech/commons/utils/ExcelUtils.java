package cn.com.bobfintech.commons.utils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;

import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;

/**
 * @project: 北银金科
 * @description: excel 工具类
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-03 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Slf4j
public class ExcelUtils
{
    /**
     * 根据Cell类型设置数据
     * 
     * @param cell
     *            cell单位
     * @return
     */
    @SuppressWarnings("deprecation")
    public static Object getCellFormatValue(Cell cell)
    {
        Object cellvalue = "";
        if (cell != null) {
            // 判断当前Cell的Type
            if (cell.getCellType() == CellType.NUMERIC || cell.getCellType() == CellType.FORMULA) {
                // 判断当前的cell是否为Date
                if (DateUtil.isCellDateFormatted(cell)) {
                    // 判断当前的cell是否为Date
                    // 如果是Date类型则，转化为Data格式
                    Date date = cell.getDateCellValue();
                    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                    cellvalue = format.format(date);
                }
                else {
                    cell.setCellType(CellType.STRING);
                    cellvalue = cell.getStringCellValue();
                }
            }
            else if (cell.getCellType() == CellType.STRING) {
                // 如果当前Cell的Type为STRING
                // 取得当前的Cell字符串
                cellvalue = cell.getRichStringCellValue().getString();
            }
            else {
                // 默认的Cell值
                cellvalue = "";
            }
        }
        else {
            cellvalue = "";
        }
        return cellvalue;
    }
    
    /**
     * 设置pojo属性值
     * 
     * @param pojo
     *            对象属性
     * @param headMap
     *            headMap
     * @param valueMap
     *            valueMap
     * @return
     */
    public static Object setFields(Object pojo, Map<Integer, String> headMap, Map<Integer, Object> valueMap)
    {
        Field[] fields = ReflectUtils.getFieldArray(pojo.getClass());
        try {
            for (Field field : fields) {
                String fieldName = field.getName();
                for (int i = 0; i < headMap.size(); i++) {
                    if (headMap.get(i) != null) {
                        String head = headMap.get(i);
                        if (StringUtils.equals(head, fieldName)) {
                            if (StringUtils.isBlank(String.valueOf(valueMap.get(i)))) {
                                break;
                            }
                            if (field.getType() == String.class) {
                                ReflectUtils.setValueByFieldName(pojo, head, valueMap.get(i));
                                break;
                            }
                            else if (field.getType() == Integer.class) {
                                ReflectUtils.setValueByFieldName(pojo, head,
                                    Integer.valueOf(valueMap.get(i).toString()));
                                break;
                            }
                            else if (field.getType() == BigDecimal.class) {
                                ReflectUtils.setValueByFieldName(pojo, head,
                                    new BigDecimal(valueMap.get(i).toString()));
                                break;
                            }
                            else {
                                // 其他类型待添加
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            LogsUtils.errorGlobal(log, null, "填充pojo属性出错", e);
            throw new BusinessException(BobfintechErrorNoEnum.COM_UNKNOWN_ERROR.getErrorNo(),
                BobfintechErrorNoEnum.COM_UNKNOWN_ERROR.getErrorConsonleInfo());
        }
        return pojo;
    }
    
}