package cn.com.bobfintech.commons.entity.user;

import java.util.Date;

/**
 * @project:用户管理模块
 * @description:新表菜单
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 825338623@qq.com Create 1.0
 * @copyright ©2017-2019
 */
public class SysResource {

    
    /**主键 */
    private String id;
    
    /**资源名称 */
    private String resourceName;
    
    /**资源图标 */
    private String resourceIcon;
    
    /**1 菜单， 2 按钮 */
    private Byte resourceType;
    
    /**排序字段 */
    private Byte orderNum;
    
    /**权限编码 */
    private String permCode;
    
    /**父节点ID */
    private String parentId;
    
    /**菜单级别 */
    private Byte resourceLevel;
    
    /**资源路径 */
    private String resourceUrl;
    
    /**是否停用1是0否 */
    private Byte resourceStatus;
    
    /**创建时间 */
    private Date createdDt;
    
    /**更新时间 */
    private Date updateDt;

    
    /**
     * @return 主键
     */
    public String getId() {
        return id;
    }
    
    /**
     * @param id 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }
    
    /**
     * @return 资源名称
     */
    public String getResourceName() {
        return resourceName;
    }
    
    /**
     * @param resourceName 资源名称
     */
    public void setResourceName(String resourceName) {
        this.resourceName = resourceName == null ? null : resourceName.trim();
    }
    
    /**
     * @return 资源图标
     */
    public String getResourceIcon() {
        return resourceIcon;
    }
    
    /**
     * @param resourceIcon 资源图标
     */
    public void setResourceIcon(String resourceIcon) {
        this.resourceIcon = resourceIcon == null ? null : resourceIcon.trim();
    }
    
    /**
     * @return 1 菜单， 2 按钮
     */
    public Byte getResourceType() {
        return resourceType;
    }
    
    /**
     * @param resourceType 1 菜单， 2 按钮
     */
    public void setResourceType(Byte resourceType) {
        this.resourceType = resourceType;
    }
    
    /**
     * @return 排序字段
     */
    public Byte getOrderNum() {
        return orderNum;
    }
    
    /**
     * @param orderNum 排序字段
     */
    public void setOrderNum(Byte orderNum) {
        this.orderNum = orderNum;
    }
    
    /**
     * @return 权限编码
     */
    public String getPermCode() {
        return permCode;
    }
    
    /**
     * @param permCode 权限编码
     */
    public void setPermCode(String permCode) {
        this.permCode = permCode == null ? null : permCode.trim();
    }
    
    /**
     * @return 父节点ID
     */
    public String getParentId() {
        return parentId;
    }
    
    /**
     * @param parentId 父节点ID
     */
    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }
    
    /**
     * @return 菜单级别
     */
    public Byte getResourceLevel() {
        return resourceLevel;
    }
    
    /**
     * @param resourceLevel 菜单级别
     */
    public void setResourceLevel(Byte resourceLevel) {
        this.resourceLevel = resourceLevel;
    }
    
    /**
     * @return 资源路径
     */
    public String getResourceUrl() {
        return resourceUrl;
    }
    
    /**
     * @param resourceUrl 资源路径
     */
    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl == null ? null : resourceUrl.trim();
    }
    
    /**
     * @return 是否停用1是0否
     */
    public Byte getResourceStatus() {
        return resourceStatus;
    }
    
    /**
     * @param resourceStatus 是否停用1是0否
     */
    public void setResourceStatus(Byte resourceStatus) {
        this.resourceStatus = resourceStatus;
    }
    
    /**
     * @return 创建时间
     */
    public Date getCreatedDt() {
        return createdDt == null ? null : (Date) createdDt.clone();
    }
    
    /**
     * @param createdDt 创建时间
     */
    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt == null ? null : (Date) createdDt.clone();
    }
    
    /**
     * @return 更新时间
     */
    public Date getUpdateDt() {
        return updateDt == null ? null : (Date) updateDt.clone();
    }
    
    /**
     * @param updateDt 更新时间
     */
    public void setUpdateDt(Date updateDt) {
        this.updateDt = updateDt == null ? null : (Date) updateDt.clone();
    }
}
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/