package cn.com.bobfintech.commons.pojo;

import java.io.Serializable;

import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;

/**
 * 响应数据基类
 * 
 * @author 郭鹏飞
 */
public class BaseResponse implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -5544502424932175589L;
    
    /**
     * 状态码
     */
	private String code;

	/**
	 * 响应消息
	 */
	private String msg;

	protected BaseResponse() {
	}

    protected BaseResponse(BobfintechErrorNoEnum code)
    {
        this.code = code.getErrorNo();
        this.msg = code.getErrorConsonleInfo();
	}
    
    protected BaseResponse(String code, String msg)
    {
        this.code = code;
        this.msg = msg;
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}


}
