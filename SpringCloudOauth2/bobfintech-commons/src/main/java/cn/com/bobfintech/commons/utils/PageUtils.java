package cn.com.bobfintech.commons.utils;

import com.github.pagehelper.PageInfo;

import cn.com.bobfintech.commons.pojo.PageResponse;

/**
 * @project: 北银金科
 * @description: 中文类名。
 *               类功能简介。
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-02 Jiaxi Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
public class PageUtils {
    /**
     * 将分页信息封装到统一的接口
     * @param pageInfo
     * @return
     */
    public static PageResponse getPageResponse(PageInfo<?> pageInfo) {
        PageResponse PageResponse = new PageResponse();
        PageResponse.setPageNum(pageInfo.getPageNum());
        PageResponse.setPageSize(pageInfo.getPageSize());
        PageResponse.setTotalSize(pageInfo.getTotal());
        PageResponse.setTotalPages(pageInfo.getPages());
        PageResponse.setContent(pageInfo.getList());
        return PageResponse;
    }
    
    public static String getVersion()
    {
        return "测试";
    }
}