package cn.com.bobfintech.commons.exception;

/**
 * @Project: 便民(BeBianMin-BaJson)系统
 * @Description: 自定义异常
 * @Version 1.0.0
 * @Author
 *         <li>2020-04-25 guopengfeiheze@blog-china.cn Create 1.0
 * @Copyright ©2017-2020 BeBianMin（www.oipinche.com），版权所有。
 */
public class BaseException extends RuntimeException
{
    
    /**
     * 
     */
    private static final long serialVersionUID = 8200800604332755686L;
    
    public BaseException()
    {
        
    }
    
    public BaseException(String msg)
    {
        super(msg);
    }
    
    public BaseException(String msg, Throwable t)
    {
        super(msg, t);
    }

    
}
