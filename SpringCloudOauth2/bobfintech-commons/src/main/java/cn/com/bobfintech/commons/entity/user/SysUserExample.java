package cn.com.bobfintech.commons.entity.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.exception.BusinessException;
import cn.com.bobfintech.commons.utils.StringUtils;

/**
 * @project:用户管理模块
 * @description:用户表
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 825338623@qq.com Create 1.0
 * @copyright ©2017-2019
 */
public class SysUserExample {

    
    /**orderByClause */
    protected String orderByClause;
    
    /**distinct */
    protected boolean distinct;
    
    /**oredCriteria */
    protected List<Criteria> oredCriteria;

    
    /**
     * 构造方法 
     */
    public SysUserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }
    
    /**
     * @param orderByClause orderByClause
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }
    
    /**
     * @return String
     */
    public String getOrderByClause() {
        return orderByClause;
    }
    
    /**
     * @param distinct distinct
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }
    
    /**
     * @return boolean
     */
    public boolean isDistinct() {
        return distinct;
    }
    
    /**
     * @return List<Criteria>
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }
    
    /**
     * @param criteria criteria
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }
    
    /**
     * @return Criteria
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }
    
    /**
     * @return Criteria
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }
    
    /**
     * @return Criteria
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }
    
    /**
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * @project:用户管理模块
     * @description:用户表
     * @version 1.0.0
     * @errorcode
     *            错误码: 错误描述
     * @author
     *         <li>2019-01-23 825338623@qq.com Create 1.0
     * @copyright ©2017-2019 中央结算公司，版权所有。 
     */
    protected abstract static class GeneratedCriteria {

        
        /**criteria */
        protected List<Criterion> criteria;

        
        /**
         * 构造方法 
         */
        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }
        
        /**
         * @return boolean
         */
        public boolean isValid() {
            return criteria.size() > 0;
        }
        
        /**
         * @return List<Criterion>
         */
        public List<Criterion> getAllCriteria() {
            return criteria;
        }
        
        /**
         * @return List<Criterion>
         */
        public List<Criterion> getCriteria() {
            return criteria;
        }
        
        /**
         * @param condition condition
         */
        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                    StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(),
                        "Value for condition cannot be null"));
            }
            criteria.add(new Criterion(condition));
        }
        
        /**
         * @param condition condition
         * @param value value
         * @param property property
         */
        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                    StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(),
                        "Value for condition cannot be null"));
            }
            criteria.add(new Criterion(condition, value));
        }
        
        /**
         * @param condition condition
         * @param value1 value1
         * @param value2 value2
         * @param property property
         */
        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorNo(),
                    StringUtils.formatStr(BobfintechErrorNoEnum.COM_BOBFINTECH_ERROR.getErrorConsonleInfo(),
                        "Value for condition cannot be null"));
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
        /**
         * @return Criteria
         */
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andOrgCodeIsNull() {
            addCriterion("org_code is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andOrgCodeIsNotNull() {
            addCriterion("org_code is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andOrgCodeEqualTo(String value) {
            addCriterion("org_code =", value, "orgCode");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andOrgCodeNotEqualTo(String value) {
            addCriterion("org_code <>", value, "orgCode");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andOrgCodeGreaterThan(String value) {
            addCriterion("org_code >", value, "orgCode");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andOrgCodeGreaterThanOrEqualTo(String value) {
            addCriterion("org_code >=", value, "orgCode");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andOrgCodeLessThan(String value) {
            addCriterion("org_code <", value, "orgCode");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andOrgCodeLessThanOrEqualTo(String value) {
            addCriterion("org_code <=", value, "orgCode");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andOrgCodeLike(String value) {
            addCriterion("org_code like", value, "orgCode");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andOrgCodeNotLike(String value) {
            addCriterion("org_code not like", value, "orgCode");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andOrgCodeIn(List<String> values) {
            addCriterion("org_code in", values, "orgCode");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andOrgCodeNotIn(List<String> values) {
            addCriterion("org_code not in", values, "orgCode");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andOrgCodeBetween(String value1, String value2) {
            addCriterion("org_code between", value1, value2, "orgCode");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andOrgCodeNotBetween(String value1, String value2) {
            addCriterion("org_code not between", value1, value2, "orgCode");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserNameIsNull() {
            addCriterion("user_name is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserNameIsNotNull() {
            addCriterion("user_name is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameEqualTo(String value) {
            addCriterion("user_name =", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("user_name <>", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("user_name >", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("user_name >=", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameLessThan(String value) {
            addCriterion("user_name <", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("user_name <=", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameLike(String value) {
            addCriterion("user_name like", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameNotLike(String value) {
            addCriterion("user_name not like", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserNameIn(List<String> values) {
            addCriterion("user_name in", values, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("user_name not in", values, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("user_name between", value1, value2, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("user_name not between", value1, value2, "userName");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andRealNameIsNull() {
            addCriterion("real_name is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andRealNameIsNotNull() {
            addCriterion("real_name is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameEqualTo(String value) {
            addCriterion("real_name =", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameNotEqualTo(String value) {
            addCriterion("real_name <>", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameGreaterThan(String value) {
            addCriterion("real_name >", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameGreaterThanOrEqualTo(String value) {
            addCriterion("real_name >=", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameLessThan(String value) {
            addCriterion("real_name <", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameLessThanOrEqualTo(String value) {
            addCriterion("real_name <=", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameLike(String value) {
            addCriterion("real_name like", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameNotLike(String value) {
            addCriterion("real_name not like", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andRealNameIn(List<String> values) {
            addCriterion("real_name in", values, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andRealNameNotIn(List<String> values) {
            addCriterion("real_name not in", values, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andRealNameBetween(String value1, String value2) {
            addCriterion("real_name between", value1, value2, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andRealNameNotBetween(String value1, String value2) {
            addCriterion("real_name not between", value1, value2, "realName");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andPasswordIsNull() {
            addCriterion("password is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andPasswordIsNotNull() {
            addCriterion("password is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andPasswordEqualTo(String value) {
            addCriterion("password =", value, "password");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("password <>", value, "password");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("password >", value, "password");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("password >=", value, "password");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andPasswordLessThan(String value) {
            addCriterion("password <", value, "password");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("password <=", value, "password");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andPasswordLike(String value) {
            addCriterion("password like", value, "password");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andPasswordNotLike(String value) {
            addCriterion("password not like", value, "password");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andPasswordIn(List<String> values) {
            addCriterion("password in", values, "password");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("password not in", values, "password");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("password between", value1, value2, "password");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("password not between", value1, value2, "password");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andDeptIdIsNull() {
            addCriterion("dept_id is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andDeptIdIsNotNull() {
            addCriterion("dept_id is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andDeptIdEqualTo(String value) {
            addCriterion("dept_id =", value, "deptId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andDeptIdNotEqualTo(String value) {
            addCriterion("dept_id <>", value, "deptId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andDeptIdGreaterThan(String value) {
            addCriterion("dept_id >", value, "deptId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andDeptIdGreaterThanOrEqualTo(String value) {
            addCriterion("dept_id >=", value, "deptId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andDeptIdLessThan(String value) {
            addCriterion("dept_id <", value, "deptId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andDeptIdLessThanOrEqualTo(String value) {
            addCriterion("dept_id <=", value, "deptId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andDeptIdLike(String value) {
            addCriterion("dept_id like", value, "deptId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andDeptIdNotLike(String value) {
            addCriterion("dept_id not like", value, "deptId");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andDeptIdIn(List<String> values) {
            addCriterion("dept_id in", values, "deptId");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andDeptIdNotIn(List<String> values) {
            addCriterion("dept_id not in", values, "deptId");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andDeptIdBetween(String value1, String value2) {
            addCriterion("dept_id between", value1, value2, "deptId");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andDeptIdNotBetween(String value1, String value2) {
            addCriterion("dept_id not between", value1, value2, "deptId");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserStatusIsNull() {
            addCriterion("user_status is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserStatusIsNotNull() {
            addCriterion("user_status is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserStatusEqualTo(Byte value) {
            addCriterion("user_status =", value, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserStatusNotEqualTo(Byte value) {
            addCriterion("user_status <>", value, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserStatusGreaterThan(Byte value) {
            addCriterion("user_status >", value, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("user_status >=", value, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserStatusLessThan(Byte value) {
            addCriterion("user_status <", value, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserStatusLessThanOrEqualTo(Byte value) {
            addCriterion("user_status <=", value, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserStatusIn(List<Byte> values) {
            addCriterion("user_status in", values, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserStatusNotIn(List<Byte> values) {
            addCriterion("user_status not in", values, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserStatusBetween(Byte value1, Byte value2) {
            addCriterion("user_status between", value1, value2, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("user_status not between", value1, value2, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andIsAdminIsNull() {
            addCriterion("is_admin is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andIsAdminIsNotNull() {
            addCriterion("is_admin is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIsAdminEqualTo(Byte value) {
            addCriterion("is_admin =", value, "isAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIsAdminNotEqualTo(Byte value) {
            addCriterion("is_admin <>", value, "isAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIsAdminGreaterThan(Byte value) {
            addCriterion("is_admin >", value, "isAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIsAdminGreaterThanOrEqualTo(Byte value) {
            addCriterion("is_admin >=", value, "isAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIsAdminLessThan(Byte value) {
            addCriterion("is_admin <", value, "isAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIsAdminLessThanOrEqualTo(Byte value) {
            addCriterion("is_admin <=", value, "isAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andIsAdminIn(List<Byte> values) {
            addCriterion("is_admin in", values, "isAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andIsAdminNotIn(List<Byte> values) {
            addCriterion("is_admin not in", values, "isAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andIsAdminBetween(Byte value1, Byte value2) {
            addCriterion("is_admin between", value1, value2, "isAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andIsAdminNotBetween(Byte value1, Byte value2) {
            addCriterion("is_admin not between", value1, value2, "isAdmin");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andIsOrgAdminIsNull() {
            addCriterion("is_org_admin is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andIsOrgAdminIsNotNull() {
            addCriterion("is_org_admin is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIsOrgAdminEqualTo(Byte value) {
            addCriterion("is_org_admin =", value, "isOrgAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIsOrgAdminNotEqualTo(Byte value) {
            addCriterion("is_org_admin <>", value, "isOrgAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIsOrgAdminGreaterThan(Byte value) {
            addCriterion("is_org_admin >", value, "isOrgAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIsOrgAdminGreaterThanOrEqualTo(Byte value) {
            addCriterion("is_org_admin >=", value, "isOrgAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIsOrgAdminLessThan(Byte value) {
            addCriterion("is_org_admin <", value, "isOrgAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIsOrgAdminLessThanOrEqualTo(Byte value) {
            addCriterion("is_org_admin <=", value, "isOrgAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andIsOrgAdminIn(List<Byte> values) {
            addCriterion("is_org_admin in", values, "isOrgAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andIsOrgAdminNotIn(List<Byte> values) {
            addCriterion("is_org_admin not in", values, "isOrgAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andIsOrgAdminBetween(Byte value1, Byte value2) {
            addCriterion("is_org_admin between", value1, value2, "isOrgAdmin");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andIsOrgAdminNotBetween(Byte value1, Byte value2) {
            addCriterion("is_org_admin not between", value1, value2, "isOrgAdmin");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserJobNumberIsNull() {
            addCriterion("user_job_number is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserJobNumberIsNotNull() {
            addCriterion("user_job_number is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserJobNumberEqualTo(String value) {
            addCriterion("user_job_number =", value, "userJobNumber");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserJobNumberNotEqualTo(String value) {
            addCriterion("user_job_number <>", value, "userJobNumber");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserJobNumberGreaterThan(String value) {
            addCriterion("user_job_number >", value, "userJobNumber");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserJobNumberGreaterThanOrEqualTo(String value) {
            addCriterion("user_job_number >=", value, "userJobNumber");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserJobNumberLessThan(String value) {
            addCriterion("user_job_number <", value, "userJobNumber");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserJobNumberLessThanOrEqualTo(String value) {
            addCriterion("user_job_number <=", value, "userJobNumber");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserJobNumberLike(String value) {
            addCriterion("user_job_number like", value, "userJobNumber");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserJobNumberNotLike(String value) {
            addCriterion("user_job_number not like", value, "userJobNumber");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserJobNumberIn(List<String> values) {
            addCriterion("user_job_number in", values, "userJobNumber");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserJobNumberNotIn(List<String> values) {
            addCriterion("user_job_number not in", values, "userJobNumber");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserJobNumberBetween(String value1, String value2) {
            addCriterion("user_job_number between", value1, value2, "userJobNumber");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserJobNumberNotBetween(String value1, String value2) {
            addCriterion("user_job_number not between", value1, value2, "userJobNumber");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andAvatarUrlIsNull() {
            addCriterion("avatar_url is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andAvatarUrlIsNotNull() {
            addCriterion("avatar_url is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andAvatarUrlEqualTo(String value) {
            addCriterion("avatar_url =", value, "avatarUrl");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andAvatarUrlNotEqualTo(String value) {
            addCriterion("avatar_url <>", value, "avatarUrl");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andAvatarUrlGreaterThan(String value) {
            addCriterion("avatar_url >", value, "avatarUrl");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andAvatarUrlGreaterThanOrEqualTo(String value) {
            addCriterion("avatar_url >=", value, "avatarUrl");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andAvatarUrlLessThan(String value) {
            addCriterion("avatar_url <", value, "avatarUrl");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andAvatarUrlLessThanOrEqualTo(String value) {
            addCriterion("avatar_url <=", value, "avatarUrl");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andAvatarUrlLike(String value) {
            addCriterion("avatar_url like", value, "avatarUrl");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andAvatarUrlNotLike(String value) {
            addCriterion("avatar_url not like", value, "avatarUrl");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andAvatarUrlIn(List<String> values) {
            addCriterion("avatar_url in", values, "avatarUrl");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andAvatarUrlNotIn(List<String> values) {
            addCriterion("avatar_url not in", values, "avatarUrl");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andAvatarUrlBetween(String value1, String value2) {
            addCriterion("avatar_url between", value1, value2, "avatarUrl");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andAvatarUrlNotBetween(String value1, String value2) {
            addCriterion("avatar_url not between", value1, value2, "avatarUrl");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andVersionIsNull() {
            addCriterion("version is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andVersionIsNotNull() {
            addCriterion("version is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andVersionEqualTo(Integer value) {
            addCriterion("version =", value, "version");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andVersionNotEqualTo(Integer value) {
            addCriterion("version <>", value, "version");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andVersionGreaterThan(Integer value) {
            addCriterion("version >", value, "version");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andVersionGreaterThanOrEqualTo(Integer value) {
            addCriterion("version >=", value, "version");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andVersionLessThan(Integer value) {
            addCriterion("version <", value, "version");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andVersionLessThanOrEqualTo(Integer value) {
            addCriterion("version <=", value, "version");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andVersionIn(List<Integer> values) {
            addCriterion("version in", values, "version");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andVersionNotIn(List<Integer> values) {
            addCriterion("version not in", values, "version");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andVersionBetween(Integer value1, Integer value2) {
            addCriterion("version between", value1, value2, "version");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andVersionNotBetween(Integer value1, Integer value2) {
            addCriterion("version not between", value1, value2, "version");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andCreatorUidIsNull() {
            addCriterion("creator_uid is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andCreatorUidIsNotNull() {
            addCriterion("creator_uid is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreatorUidEqualTo(String value) {
            addCriterion("creator_uid =", value, "creatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreatorUidNotEqualTo(String value) {
            addCriterion("creator_uid <>", value, "creatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreatorUidGreaterThan(String value) {
            addCriterion("creator_uid >", value, "creatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreatorUidGreaterThanOrEqualTo(String value) {
            addCriterion("creator_uid >=", value, "creatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreatorUidLessThan(String value) {
            addCriterion("creator_uid <", value, "creatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreatorUidLessThanOrEqualTo(String value) {
            addCriterion("creator_uid <=", value, "creatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreatorUidLike(String value) {
            addCriterion("creator_uid like", value, "creatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreatorUidNotLike(String value) {
            addCriterion("creator_uid not like", value, "creatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andCreatorUidIn(List<String> values) {
            addCriterion("creator_uid in", values, "creatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andCreatorUidNotIn(List<String> values) {
            addCriterion("creator_uid not in", values, "creatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andCreatorUidBetween(String value1, String value2) {
            addCriterion("creator_uid between", value1, value2, "creatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andCreatorUidNotBetween(String value1, String value2) {
            addCriterion("creator_uid not between", value1, value2, "creatorUid");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andCreatedDtIsNull() {
            addCriterion("created_dt is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andCreatedDtIsNotNull() {
            addCriterion("created_dt is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreatedDtEqualTo(Date value) {
            addCriterion("created_dt =", value, "createdDt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreatedDtNotEqualTo(Date value) {
            addCriterion("created_dt <>", value, "createdDt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreatedDtGreaterThan(Date value) {
            addCriterion("created_dt >", value, "createdDt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreatedDtGreaterThanOrEqualTo(Date value) {
            addCriterion("created_dt >=", value, "createdDt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreatedDtLessThan(Date value) {
            addCriterion("created_dt <", value, "createdDt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreatedDtLessThanOrEqualTo(Date value) {
            addCriterion("created_dt <=", value, "createdDt");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andCreatedDtIn(List<Date> values) {
            addCriterion("created_dt in", values, "createdDt");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andCreatedDtNotIn(List<Date> values) {
            addCriterion("created_dt not in", values, "createdDt");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andCreatedDtBetween(Date value1, Date value2) {
            addCriterion("created_dt between", value1, value2, "createdDt");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andCreatedDtNotBetween(Date value1, Date value2) {
            addCriterion("created_dt not between", value1, value2, "createdDt");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUpdatorUidIsNull() {
            addCriterion("updator_uid is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUpdatorUidIsNotNull() {
            addCriterion("updator_uid is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdatorUidEqualTo(String value) {
            addCriterion("updator_uid =", value, "updatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdatorUidNotEqualTo(String value) {
            addCriterion("updator_uid <>", value, "updatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdatorUidGreaterThan(String value) {
            addCriterion("updator_uid >", value, "updatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdatorUidGreaterThanOrEqualTo(String value) {
            addCriterion("updator_uid >=", value, "updatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdatorUidLessThan(String value) {
            addCriterion("updator_uid <", value, "updatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdatorUidLessThanOrEqualTo(String value) {
            addCriterion("updator_uid <=", value, "updatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdatorUidLike(String value) {
            addCriterion("updator_uid like", value, "updatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdatorUidNotLike(String value) {
            addCriterion("updator_uid not like", value, "updatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUpdatorUidIn(List<String> values) {
            addCriterion("updator_uid in", values, "updatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUpdatorUidNotIn(List<String> values) {
            addCriterion("updator_uid not in", values, "updatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUpdatorUidBetween(String value1, String value2) {
            addCriterion("updator_uid between", value1, value2, "updatorUid");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUpdatorUidNotBetween(String value1, String value2) {
            addCriterion("updator_uid not between", value1, value2, "updatorUid");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUpdateDtIsNull() {
            addCriterion("update_dt is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUpdateDtIsNotNull() {
            addCriterion("update_dt is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDtEqualTo(Date value) {
            addCriterion("update_dt =", value, "updateDt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDtNotEqualTo(Date value) {
            addCriterion("update_dt <>", value, "updateDt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDtGreaterThan(Date value) {
            addCriterion("update_dt >", value, "updateDt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDtGreaterThanOrEqualTo(Date value) {
            addCriterion("update_dt >=", value, "updateDt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDtLessThan(Date value) {
            addCriterion("update_dt <", value, "updateDt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDtLessThanOrEqualTo(Date value) {
            addCriterion("update_dt <=", value, "updateDt");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUpdateDtIn(List<Date> values) {
            addCriterion("update_dt in", values, "updateDt");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUpdateDtNotIn(List<Date> values) {
            addCriterion("update_dt not in", values, "updateDt");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUpdateDtBetween(Date value1, Date value2) {
            addCriterion("update_dt between", value1, value2, "updateDt");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUpdateDtNotBetween(Date value1, Date value2) {
            addCriterion("update_dt not between", value1, value2, "updateDt");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andDeletedIsNull() {
            addCriterion("deleted is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andDeletedIsNotNull() {
            addCriterion("deleted is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andDeletedEqualTo(Boolean value) {
            addCriterion("deleted =", value, "deleted");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andDeletedNotEqualTo(Boolean value) {
            addCriterion("deleted <>", value, "deleted");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andDeletedGreaterThan(Boolean value) {
            addCriterion("deleted >", value, "deleted");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andDeletedGreaterThanOrEqualTo(Boolean value) {
            addCriterion("deleted >=", value, "deleted");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andDeletedLessThan(Boolean value) {
            addCriterion("deleted <", value, "deleted");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andDeletedLessThanOrEqualTo(Boolean value) {
            addCriterion("deleted <=", value, "deleted");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andDeletedIn(List<Boolean> values) {
            addCriterion("deleted in", values, "deleted");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andDeletedNotIn(List<Boolean> values) {
            addCriterion("deleted not in", values, "deleted");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andDeletedBetween(Boolean value1, Boolean value2) {
            addCriterion("deleted between", value1, value2, "deleted");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andDeletedNotBetween(Boolean value1, Boolean value2) {
            addCriterion("deleted not between", value1, value2, "deleted");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andSaltIsNull() {
            addCriterion("salt is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andSaltIsNotNull() {
            addCriterion("salt is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andSaltEqualTo(String value) {
            addCriterion("salt =", value, "salt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andSaltNotEqualTo(String value) {
            addCriterion("salt <>", value, "salt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andSaltGreaterThan(String value) {
            addCriterion("salt >", value, "salt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andSaltGreaterThanOrEqualTo(String value) {
            addCriterion("salt >=", value, "salt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andSaltLessThan(String value) {
            addCriterion("salt <", value, "salt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andSaltLessThanOrEqualTo(String value) {
            addCriterion("salt <=", value, "salt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andSaltLike(String value) {
            addCriterion("salt like", value, "salt");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andSaltNotLike(String value) {
            addCriterion("salt not like", value, "salt");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andSaltIn(List<String> values) {
            addCriterion("salt in", values, "salt");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andSaltNotIn(List<String> values) {
            addCriterion("salt not in", values, "salt");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andSaltBetween(String value1, String value2) {
            addCriterion("salt between", value1, value2, "salt");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andSaltNotBetween(String value1, String value2) {
            addCriterion("salt not between", value1, value2, "salt");
            return (Criteria) this;
        }
    }
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/

    /**
     * @project:用户管理模块
     * @description:用户表
     * @version 1.0.0
     * @errorcode
     *            错误码: 错误描述
     * @author
     *         <li>2019-01-23 825338623@qq.com Create 1.0
     * @copyright ©2017-2019 中央结算公司，版权所有。 
     */
    public static class Criteria extends GeneratedCriteria {


        
        /**
         * 构造方法 
         */
        protected Criteria() {
            super();
        }
    }
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/

    /**
     * @project:用户管理模块
     * @description:用户表
     * @version 1.0.0
     * @errorcode
     *            错误码: 错误描述
     * @author
     *         <li>2019-01-23 825338623@qq.com Create 1.0
     * @copyright ©2017-2019 中央结算公司，版权所有。 
     */
    public static class Criterion {

        
        /**condition */
        private String condition;
        
        /**value */
        private Object value;
        
        /**secondValue */
        private Object secondValue;
        
        /**noValue */
        private boolean noValue;
        
        /**singleValue */
        private boolean singleValue;
        
        /**betweenValue */
        private boolean betweenValue;
        
        /**listValue */
        private boolean listValue;
        
        /**typeHandler */
        private String typeHandler;

        
        /**
         * @return String
         */
        public String getCondition() {
            return condition;
        }
        
        /**
         * @return Object
         */
        public Object getValue() {
            return value;
        }
        
        /**
         * @return Object
         */
        public Object getSecondValue() {
            return secondValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isNoValue() {
            return noValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isSingleValue() {
            return singleValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isBetweenValue() {
            return betweenValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isListValue() {
            return listValue;
        }
        
        /**
         * @return String
         */
        public String getTypeHandler() {
            return typeHandler;
        }
        
        /**
         * 构造方法 
         * @param condition condition
         */
        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         * @param typeHandler typeHandler
         */
        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         */
        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         * @param secondValue secondValue
         * @param typeHandler typeHandler
         */
        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         * @param secondValue secondValue
         */
        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/
}
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/