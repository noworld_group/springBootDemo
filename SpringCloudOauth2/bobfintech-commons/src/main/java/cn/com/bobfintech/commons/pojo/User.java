package cn.com.bobfintech.commons.pojo;

import lombok.Data;

/**
 * @ClassName: User
 * @Description: User
 * Author:  Jiaxi
 * Date: 2020/6/28 10:18
 */
@Data
public class User {
    private String id;
    private String name;
    private String age;

}