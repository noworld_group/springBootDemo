package cn.com.bobfintech.commons.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.util.LinkedCaseInsensitiveMap;

/**
 * @project: 北银金科
 * @description: map工具类。
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-03 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
public class MapUtils extends org.apache.commons.collections.MapUtils
{
    /**
     * 将map中的key全部转换为小写
     *
     * @param map
     *            map对象
     */
    public static void key2Lower(Map<String, Object> map)
    {
        if (map == null) {
            return;
        }
        Map<String, Object> tempMap = new HashMap<>();
        tempMap.putAll(map);
        map.clear();
        Set<Entry<String, Object>> entrySet = tempMap.entrySet();
        for (Map.Entry<String, Object> entry : entrySet) {
            map.put(StringUtils.lowerCase(entry.getKey()), entry.getValue());
        }
    }
    
    /**
     * 将map中的key全部由下划线转换为驼峰
     * 注意：现将key全部转换为小写
     *
     * @param map
     *            map对象
     */
    public static void keyUnderline2Camel(Map<String, Object> map)
    {
        if (map == null) {
            return;
        }
        Map<String, Object> tempMap = new HashMap<>();
        tempMap.putAll(map);
        map.clear();
        Set<Entry<String, Object>> entrySet = tempMap.entrySet();
        for (Map.Entry<String, Object> entry : entrySet) {
            map.put(StringUtils.lowerUnderline2Camel(entry.getKey()), entry.getValue());
        }
        
    }
    
    /**
     * 将Map设置为key对大小写不敏感
     * 
     * @param map
     *            map对象
     * @return
     */
    public static Map<String, Object> getCaseInsensitiveMap(Map<String, Object> map)
    {
        Map<String, Object> tempMap = new LinkedCaseInsensitiveMap<Object>();
        tempMap.putAll(map);
        return tempMap;
    }
    
    /**
     * 判断map中是否全部包含keys中的值
     *
     * @param map
     *            map对象
     * @param keys
     *            key集合
     * @return map为null或者keys为空 则返回false
     */
    public static boolean containsAllKey(@SuppressWarnings("rawtypes") Map map, Object... keys)
    {
        if (isEmpty(map)) {
            return false;
        }
        
        if (keys == null) {
            return true;
        }
        
        for (Object key : keys) {
            if (key == null) {
                return false;
            }
            if (key instanceof Object[]) {
                if (!containsAllKey(map, (Object[]) key)) {
                    return false;
                }
            }
            else {
                if (!map.containsKey(key)) {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * 从Map中获取指定类型的值
     *
     * @param map
     *            map对象
     * @param key
     *            key
     * @param clazz
     *            返回对象类型
     * @param <T>
     *            泛型对象
     * @return
     */
    public static <T> T getObject(@SuppressWarnings("rawtypes") Map map, Object key, Class<T> clazz)
    {
        return getObject(map, key, clazz, null);
    }
    
    /**
     * 从Map中获取指定类型的值
     *
     * @param map
     *            map对象
     * @param key
     *            key
     * @param clazz
     *            返回对象类型
     * @param <T>
     *            泛型对象
     * @param defaultObj
     *            默认值
     * @return 从Map中获取指定类型的值
     */
    public static <T> T getObject(@SuppressWarnings("rawtypes") Map map, Object key, Class<T> clazz, T defaultObj)
    {
        if (map != null && map.containsKey(key)) {
            Object object = map.get(key);
            if (object != null && clazz.isInstance(object)) {
                return clazz.cast(map.get(key));
            }
        }
        return defaultObj;
    }
    
    /**
     * 检查Map中是否存在Key 并且key存在值
     * 功能描述
     *
     * @param map
     *            map对象
     * @param key
     *            key
     * @return
     */
    public static boolean isExistKeyAndValue(Map<String, Object> map, String key)
    {
        boolean flag = false;
        if (null == map) {
            return flag;
        }
        if (map.containsKey(key) && null != map.get(key)) {
            flag = true;
        }
        return flag;
    }
    
    /**
     * 从map中获取字符串，如果为null，默认为""
     *
     * @param map
     *            map对象
     * @param key
     *            key
     * @return
     */
    public static String getStringDefaultEmpty(@SuppressWarnings("rawtypes") Map map, String key)
    {
        return getString(map, key, "");
    }
    
    /**
     * 从map中获取字符串，如果为null，默认为" "
     *
     * @param map
     *            map对象
     * @param key
     *            key
     * @return
     */
    public static String getStringDefaultBlank(@SuppressWarnings("rawtypes") Map map, String key)
    {
        return getString(map, key, " ");
    }
    
    /**
     * 将mapUpdate的键值对添加到mapBase中，若有重复则覆盖
     * 
     * @param mapBase
     *            原Map
     * @param mapUpdate
     *            要增加的Map
     * @param <K,V>
     *            泛型参数
     */
    public static <K, V> void merge(Map<K, V> mapBase, Map<K, V> mapUpdate)
    {
        mapUpdate.forEach((x, y) -> {
            mapBase.put(x, y);
        });
    }
}
