package cn.com.bobfintech.commons.pojo;
import lombok.Data;

import java.util.List;

/**
 * @ClassName: PageRequest
 * @Description: 分页返回结果
 * Author:  Jiaxi
 * Date: 2020/6/30 15:46
 */
@Data
public class PageResponse {
    /**
     * 当前页码
     */
    private int pageNum;
    /**
     * 每页数量
     */
    private int pageSize;
    /**
     * 记录总数
     */
    private long totalSize;
    /**
     * 页码总数
     */
    private int totalPages;
    /**
     * 数据模型
     */
    private List<?> content;

}