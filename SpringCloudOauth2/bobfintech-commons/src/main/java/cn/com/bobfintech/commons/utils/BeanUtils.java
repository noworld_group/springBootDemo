package cn.com.bobfintech.commons.utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;

/**
 * @project: 北银金科
 * @description: bean 工具类
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-03 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Slf4j
public class BeanUtils
{
    
    /**
     * 从对象orig复制属性到对象dest
     * 
     * @param dest
     *            目标对象, 不能为null.
     * @param orig
     *            源对象, 不能为null.
     */
    public static void copyProperties(Object dest, Object orig)
    {
        try {
            // 注意,参数第一个是源对象,第2个是目标对象.
            org.springframework.beans.BeanUtils.copyProperties(orig, dest);
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_BOBFINTECH_BEAN_COPY_ERROR.getErrorNo(),
                BobfintechErrorNoEnum.COM_BOBFINTECH_BEAN_COPY_ERROR.getErrorConsonleInfo(), e);
            
        }
    }
    
    /**
     * 对象属性复制
     * 
     * @param dest
     *            目标对象, 不能为null.
     * @param orig
     *            源对象, 不能为null.
     * @param excludeNull
     *            true-剔除源对象中的null值,false-全部覆盖
     */
    public static void copyProperties(Object dest, Object orig, boolean excludeNull)
    {
        if (excludeNull) {
            copyPropertiesExcludeNull(dest, orig);
        }
        else {
            copyProperties(dest, orig);
        }
    }
    
    /**
     * 对象属性复制
     *
     * @param dest
     *            目标对象, 不能为null
     * @param orig
     *            源对象, 不能为null
     */
    private static void copyPropertiesExcludeNull(Object dest, Object orig)
    {
        try {
            Field[] fields = ReflectUtils.getFieldArray(orig.getClass());
            for (Field field : fields) {
                if ("serialVersionUID".equals(field.getName())) {
                    continue;
                }
                Object value = FieldUtils.readField(field, orig, true);
                if (value != null) {
                    FieldUtils.writeField(dest, field.getName(), value, true);
                }
            }
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), "工具类执行异常"), e);
        }
    }
    
    /**
     * 对象属性复制
     * 
     * @param dest
     *            目标对象, 不能为null.
     * @param orig
     *            源对象, 不能为null.
     * @param flag
     *            true-剔除源对象中的null值,false-全部覆盖
     */
    public static void copyPropertiesFromProtectedObject(Object dest, Object orig, boolean flag)
    {
        try {
            Field[] fields = ReflectUtils.getFieldArray(orig.getClass());
            for (Field field : fields) {
                if ("serialVersionUID".equals(field.getName())) {
                    continue;
                }
                PropertyDescriptor origPd = new PropertyDescriptor(field.getName(), orig.getClass());
                Method getMethod = origPd.getReadMethod(); // 获得get方法
                Object value = null;
                if (getMethod != null) {
                    value = getMethod.invoke(orig); // 执行get方法返回一个Object
                }
                if (value == null && flag) {
                    continue;
                }
                PropertyDescriptor destPd = new PropertyDescriptor(field.getName(), dest.getClass());
                Method setMethod = destPd.getWriteMethod(); // 获得set方法
                if (setMethod != null) {
                    setMethod.invoke(dest, value);
                }
            }
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), "工具类执行异常"), e);
        }
    }
    
    /**
     * Map转JavaBean.
     * 
     * @param <T>
     *            t
     * @param map
     *            map
     * @param cls
     *            必须有默认构造函数
     * @return
     */
    public static <T> T map2Bean(Map<String, ?> map, Class<T> cls)
    {
        T t = null;
        try {
            t = cls.newInstance();
            map2Bean(map, t);
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), "工具类执行异常"), e);
        }
        return t;
    }
    
    /**
     * Map转JavaBean
     * 
     * @param map
     *            map
     * @param bean
     *            实体类
     * @return
     */
    public static void map2Bean(Map<String, ?> map, Object bean)
    {
        try {
            org.apache.commons.beanutils.BeanUtils.populate(bean, map);
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), "工具类执行异常"), e);
        }
    }
    
    /**
     * JavaBean转 Map
     * 
     * @param bean
     *            实体
     * @return
     */
    public static Map<String, Object> bean2Map(Object bean)
    {
        try {
            Map<String, Object> beanMap = new HashMap<>();
            if (bean == null) {
                return beanMap;
            }
            
            Field[] fields = FieldUtils.getAllFields(bean.getClass());
            if (fields != null) {
                for (Field field : fields) {
                    field.setAccessible(true);
                    beanMap.put(field.getName(), field.get(bean));
                }
            }
            return beanMap;
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), "工具类执行异常"), e);
        }
    }
    
    /**
     * JavaBean转 Map，只把包含busiField注解的转换
     * 
     * @param bean
     *            实体
     * @return
     */
    public static Map<String, Object> bean2MapIncludeBusiField(Object bean)
    {
        try {
            Map<String, Object> beanMap = new HashMap<>();
            if (bean == null) {
                return beanMap;
            }
            
            Field[] fields = FieldUtils.getAllFields(bean.getClass());
            if (fields != null) {
                for (Field field : fields) {
                    field.setAccessible(true);
                    beanMap.put(field.getName(), field.get(bean));
                }
            }
            return beanMap;
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), "工具类执行异常"), e);
        }
    }
    
    /**
     * 拷贝列表
     * 
     * @param targetList
     *            目标列表
     * @param srcList
     *            源列表
     * @param clazz
     *            目标列表中存储的类对象
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void copyList(List targetList, List srcList, Class clazz)
    {
        if (CollectionUtils.isEmpty(srcList)) {
            return;
        }
        try {
            Iterator it = srcList.iterator();
            while (it.hasNext()) {
                Object target = clazz.newInstance();
                // BeanUtils.copyProperties(target, it.next()); // //
                // 如果it.next()中的Integer类型的数据是null的，那么通过这个方法复制给target后，这个null的Integer数据会变成0。
                PropertyUtils.copyProperties(target, it.next());
                targetList.add(target);
            }
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), "工具类执行异常"), e);
        }
    }
    
    /**
     * 将源对象中的值覆盖到目标对象中，仅覆盖源对象中不为NULL值且目标对象中为NULL值的属性
     * 
     * @param dest
     *            目标对象，标准的JavaBean
     * @param orig
     *            源对象，可为Map、标准的JavaBean
     */
    @SuppressWarnings("rawtypes")
    public static void copyPropertiesExcludeDestValueAndOrigNull(Object dest, Object orig)
    {
        try {
            if (orig instanceof Map) {
                Iterator names = ((Map) orig).keySet().iterator();
                while (names.hasNext()) {
                    String name = (String) names.next();
                    if (PropertyUtils.isWriteable(dest, name) && PropertyUtils.getSimpleProperty(dest, name) == null) {
                        Object value = ((Map) orig).get(name);
                        if (value != null) {
                            PropertyUtils.setSimpleProperty(dest, name, value);
                        }
                    }
                }
            }
            else {
                Field[] fields = FieldUtils.getAllFields(orig.getClass());
                for (int i = 0; i < fields.length; i++) {
                    String name = fields[i].getName();
                    if (PropertyUtils.isReadable(orig, name) && PropertyUtils.isWriteable(dest, name)
                        && PropertyUtils.getSimpleProperty(dest, name) == null) {
                        Object value = PropertyUtils.getSimpleProperty(orig, name);
                        if (value != null) {
                            PropertyUtils.setSimpleProperty(dest, name, value);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), "工具类执行异常"), e);
        }
    }
    
    /**
     * 将源对象中的值覆盖到目标对象中，仅覆盖源对象中不为NULL值的属性
     * 
     * @param dest
     *            目标对象，标准的JavaBean
     * @param orig
     *            源对象，可为Map、标准的JavaBean
     */
    @SuppressWarnings("rawtypes")
    public static void applyIf(Object dest, Object orig)
    {
        try {
            if (orig instanceof Map) {
                Iterator names = ((Map) orig).keySet().iterator();
                while (names.hasNext()) {
                    String name = (String) names.next();
                    if (PropertyUtils.isWriteable(dest, name)) {
                        Object value = ((Map) orig).get(name);
                        if (value != null) {
                            PropertyUtils.setSimpleProperty(dest, name, value);
                        }
                    }
                }
            }
            else {
                Field[] fields = FieldUtils.getAllFields(orig.getClass());
                for (int i = 0; i < fields.length; i++) {
                    String name = fields[i].getName();
                    if (PropertyUtils.isReadable(orig, name) && PropertyUtils.isWriteable(dest, name)) {
                        Object value = PropertyUtils.getSimpleProperty(orig, name);
                        if (value != null) {
                            PropertyUtils.setSimpleProperty(dest, name, value);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), "工具类执行异常"), e);
        }
    }
    
    /**
     * 将源对象中的值覆盖到目标对象中，仅覆盖源对象中不为NULL值的属性
     * 其中如果源对象中有Long类型的日期属性与目标对象中有String类型的日期属性相对于，则修改拷贝，如果源数据为null，则拷贝默认日期值
     * 
     * @param dest
     *            目标对象，标准的JavaBean
     * @param orig
     *            源对象，标准的JavaBean
     */
    public static void copyRequestWithDate(Object dest, Object orig)
    {
        try {
            Object destCol = dest.getClass().newInstance();
            java.lang.reflect.Field[] fields = orig.getClass().getDeclaredFields();
            for (java.lang.reflect.Field field : fields) {
                String name = field.getName();
                Type t1 = field.getGenericType();
                Type t2 = getPropertiesByType(destCol, name);
                if (name != null && "class java.lang.Long".equals(t1.toString()) // 源类型是Long
                    && "class java.lang.String".equals(t2 == null ? null : t2.toString())) {// 目标类型是String
                    if (PropertyUtils.isReadable(orig, name) && PropertyUtils.isWriteable(destCol, name)) {
                        Object value = PropertyUtils.getSimpleProperty(orig, name);
                        if (value != null) {
                            PropertyUtils.setSimpleProperty(destCol, name, String.valueOf(value));
                        }
                        PropertyUtils.setSimpleProperty(orig, name, null);
                    }
                }
                
            }
            copyProperties(dest, orig);
            applyIf(dest, destCol);
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), "工具类执行异常"), e);
        }
    }
    
    /**
     * 功能描述
     *
     * @param obj
     *            实体
     * @param pName
     *            属性名称
     * @return
     */
    public static Type getPropertiesByType(Object obj, String pName)
    {
        if (pName == null) {
            return null;
        }
        java.lang.reflect.Field[] fields = obj.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            String name = fields[i].getName();
            if (pName.equals(name) && PropertyUtils.isReadable(obj, name)) {
                return fields[i].getGenericType();
            }
        }
        return null;
    }
    
    /**
     * 将源对象中的值覆盖到目标对象中，仅覆盖源对象中不为NULL值的属性
     * 其中如果源对象中有Long类型的日期属性与目标对象中有String类型的日期属性相对于，则修改拷贝
     * 
     * @param dest
     *            目标对象，标准的JavaBean
     * @param orig
     *            源对象，标准的JavaBean
     */
    public static void copyResponseWithDate(Object dest, Object orig)
    {
        try {
            Object destCol = dest.getClass().newInstance();
            java.lang.reflect.Field[] fields = orig.getClass().getDeclaredFields();
            for (java.lang.reflect.Field field : fields) {
                String name = field.getName();
                Type t1 = field.getGenericType();
                Type t2 = getPropertiesByType(destCol, name);
                if (name != null && "class java.lang.String".equals(t1.toString()) // 源类型是String
                    && "class java.lang.Long".equals(t2 == null ? null : t2.toString())) {// 目标类型是Long
                    if (PropertyUtils.isReadable(orig, name) && PropertyUtils.isWriteable(destCol, name)) {
                        Object value = PropertyUtils.getSimpleProperty(orig, name);
                        if (value != null) {
                            PropertyUtils.setSimpleProperty(destCol, name, Long.parseLong(String.valueOf(value)));
                        }
                        PropertyUtils.setSimpleProperty(orig, name, null);
                    }
                }
                
            }
            copyProperties(dest, orig);
            applyIf(dest, destCol);
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), "工具类执行异常"), e);
        }
    }
    
    /**
     * 清楚空格和空字符串
     *
     * @param obj
     *            实体对象
     */
    public static void setBeanBlankToNull(Object obj)
    {
        try {
            java.lang.reflect.Field[] fields = obj.getClass().getDeclaredFields();
            for (java.lang.reflect.Field field : fields) {
                String name = field.getName();
                if (PropertyUtils.isReadable(obj, name)) {
                    Object value = PropertyUtils.getSimpleProperty(obj, name);
                    if (value != null && "".equals((String.valueOf(value)).trim())) {
                        PropertyUtils.setSimpleProperty(obj, name, null);
                    }
                }
            }
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), "工具类执行异常"), e);
        }
    }
    
    /**
     * 直接读取对象属性值, 忽略private/protected修饰符, 不经过getter函数.
     * 
     * @param object
     *            目标对象
     * @param fieldName
     *            属性名称
     * @return
     * @throws BusinessException
     *             异常
     */
    public static Object getFieldValue(final Object object, final String fieldName) throws BusinessException
    {
        Field field;
        try {
            field = getDeclaredField(object.getClass(), fieldName);
        }
        catch (Exception e1) {
            String msg = StringUtils.formatStr("Could not find field [{0}] on target [{1}]", fieldName, object);
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), msg), e1);
        }
        
        makeAccessible(field);
        Object result = null;
        try {
            result = field.get(object);
        }
        catch (IllegalAccessException e) {
            LogsUtils.warnGlobal(log, null, "获取属性值失败", e);
        }
        return result;
    }
    
    /**
     * 通过反射方法给属性赋值
     *
     * @param obj
     *            实体对象
     * @param fieldName
     *            属性名称
     * @param fieldObj
     *            属性对象
     * @throws Exception
     *             异常
     */
    public static void setFieldValue(Object obj, String fieldName, Object fieldObj) throws Exception
    {
        try {// 子类属性赋值
            Field field = obj.getClass().getDeclaredField(fieldName);
            if (null != field) {
                field.setAccessible(true);
                field.set(obj, fieldObj);
            }
        }
        catch (NoSuchFieldException e) {// 父类属性赋值
            Field field = obj.getClass().getSuperclass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(obj, fieldObj);
        }
    }
    
    /**
     * 循环向上转型,获取对象的DeclaredField.
     * 
     * @param clazz
     *            目标类
     * @param propertyName
     *            属性名称
     * @return
     * @throws BusinessException
     *             异常
     */
    public static Field getDeclaredField(Class<?> clazz, String propertyName) throws BusinessException
    {
        for (Class<?> superClass = clazz; superClass != Object.class; superClass = superClass.getSuperclass()) {
            try {
                return superClass.getDeclaredField(propertyName);
            }
            catch (NoSuchFieldException e) {
                // Field不在当前类定义,继续向上转型
                LogsUtils.warnGlobal(log, null, "获取对象属性异常", e);
            }
        }
        String msg = StringUtils.formatStr("No such field: {0}'.'{1}", clazz.getName(), propertyName);
        throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
            StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), msg));
    }
    
    /**
     * 强行设置Field可访问.
     * 
     * @param field
     *            目标字段
     */
    private static void makeAccessible(final Field field)
    {
        if (!Modifier.isPublic(field.getModifiers()) || !Modifier.isPublic(field.getDeclaringClass().getModifiers())) {
            field.setAccessible(true);
        }
    }
    
    /**
     * 获取两个对象不同值，仅比较源对象和目标对象都存在的字段
     * 
     * @param dest
     *            比较目标对象，标准的JavaBean
     * @param orig
     *            比较源对象，可为Map、标准的JavaBean
     * @return 不同的字段Map
     * @throws BusinessException
     *             业务异常
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static Map<String, Object> getDifferent(Object dest, Object orig) throws BusinessException
    {
        try {
            Map diff = new HashMap();
            if (orig instanceof Map) {
                Iterator names = ((Map) orig).keySet().iterator();
                while (names.hasNext()) {
                    String name = (String) names.next();
                    if (PropertyUtils.isReadable(dest, name)) {
                        Object origVal = ((Map) orig).get(name);
                        Object destVal = PropertyUtils.getSimpleProperty(dest, name);
                        if (origVal != null && !origVal.equals(destVal)) {
                            diff.put(name, destVal);
                        }
                    }
                }
            }
            else /* if (orig is a standard JavaBean) */ {
                java.lang.reflect.Field[] fields = orig.getClass().getDeclaredFields();
                for (int i = 0; i < fields.length; i++) {
                    String name = fields[i].getName();
                    if (PropertyUtils.isReadable(orig, name) && PropertyUtils.isReadable(dest, name)) {
                        Object origVal = PropertyUtils.getSimpleProperty(orig, name);
                        Object destVal = PropertyUtils.getSimpleProperty(dest, name);
                        if (origVal != null && !origVal.equals(destVal)) {
                            diff.put(name, destVal);
                        }
                    }
                }
            }
            return diff;
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), "对象比较失败"), e);
        }
    }
    
    /**
     * list转为map
     *
     * @param pojos
     *            入参
     * @return
     */
    public static List<Map<String, Object>> listObject2Map(List<?> pojos)
    {
        if (CollectionUtils.isEmpty(pojos)) {
            return null;
        }
        
        List<Map<String, Object>> lMap = new ArrayList<Map<String, Object>>();
        Map<String, Object> map;
        try {
            for (Object pojo : pojos) {
                Class<?> thisClass = pojo.getClass();
                map = new HashMap<String, Object>();
                lMap.add(map);
                while (thisClass != null) {
                    Field[] fields = thisClass.getDeclaredFields();
                    for (Field field : fields) {
                        String name = field.getName();
                        if (name == null) {
                            continue;
                        }
                        Type type = field.getType();
                        Object fieldVal = getFieldValue(pojo, name);
                        if (Long.class.equals(type) || Integer.class.equals(type) || BigDecimal.class.equals(type)
                            || Double.class.equals(type)) {
                            map.put(name, String.valueOf(fieldVal == null ? "" : fieldVal));
                        }
                        else {
                            map.put(name, fieldVal);
                        }
                    }
                    thisClass = thisClass.getSuperclass();
                }
                
            }
        }
        catch (Exception e) {
            throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                StringUtils.formatStr(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo(), "工具类执行异常"), e);
        }
        return lMap;
    }
    
}
