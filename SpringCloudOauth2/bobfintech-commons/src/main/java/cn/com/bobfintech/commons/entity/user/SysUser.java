package cn.com.bobfintech.commons.entity.user;

import java.util.Date;

/**
 * @project:用户管理模块
 * @description:用户表
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 825338623@qq.com Create 1.0
 * @copyright ©2017-2019
 */
public class SysUser {

    
    /**主键 */
    private String id;
    
    /**所属组织机构代码 */
    private String orgCode;
    
    /**用户名 */
    private String userName;
    
    /**真实姓名 */
    private String realName;
    
    /**密码 */
    private String password;
    
    /**所属组织ID */
    private String deptId;
    
    /**启用1停用0 */
    private Byte userStatus;
    
    /**1 是， 0 否 */
    private Byte isAdmin;
    
    /**是否机构管理员1是，0否 */
    private Byte isOrgAdmin;
    
    /**用户工号 */
    private String userJobNumber;
    
    /**用户头像地址 */
    private String avatarUrl;
    
    /**版本号 */
    private Integer version;
    
    /**创建人UID */
    private String creatorUid;
    
    /**创建时间 */
    private Date createdDt;
    
    /**更新人UID */
    private String updatorUid;
    
    /**更新时间 */
    private Date updateDt;
    
    /**是否删除,0:未删除;1:已删除; */
    private Boolean deleted;
    
    /**盐值8位长度随机数 */
    private String salt;

    
    /**
     * @return 主键
     */
    public String getId() {
        return id;
    }
    
    /**
     * @param id 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }
    
    /**
     * @return 所属组织机构代码
     */
    public String getOrgCode() {
        return orgCode;
    }
    
    /**
     * @param orgCode 所属组织机构代码
     */
    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode == null ? null : orgCode.trim();
    }
    
    /**
     * @return 用户名
     */
    public String getUserName() {
        return userName;
    }
    
    /**
     * @param userName 用户名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }
    
    /**
     * @return 真实姓名
     */
    public String getRealName() {
        return realName;
    }
    
    /**
     * @param realName 真实姓名
     */
    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }
    
    /**
     * @return 密码
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }
    
    /**
     * @return 所属组织ID
     */
    public String getDeptId() {
        return deptId;
    }
    
    /**
     * @param deptId 所属组织ID
     */
    public void setDeptId(String deptId) {
        this.deptId = deptId == null ? null : deptId.trim();
    }
    
    /**
     * @return 启用1停用0
     */
    public Byte getUserStatus() {
        return userStatus;
    }
    
    /**
     * @param userStatus 启用1停用0
     */
    public void setUserStatus(Byte userStatus) {
        this.userStatus = userStatus;
    }
    
    /**
     * @return 1 是， 0 否
     */
    public Byte getIsAdmin() {
        return isAdmin;
    }
    
    /**
     * @param isAdmin 1 是， 0 否
     */
    public void setIsAdmin(Byte isAdmin) {
        this.isAdmin = isAdmin;
    }
    
    /**
     * @return 是否机构管理员1是，0否
     */
    public Byte getIsOrgAdmin() {
        return isOrgAdmin;
    }
    
    /**
     * @param isOrgAdmin 是否机构管理员1是，0否
     */
    public void setIsOrgAdmin(Byte isOrgAdmin) {
        this.isOrgAdmin = isOrgAdmin;
    }
    
    /**
     * @return 用户工号
     */
    public String getUserJobNumber() {
        return userJobNumber;
    }
    
    /**
     * @param userJobNumber 用户工号
     */
    public void setUserJobNumber(String userJobNumber) {
        this.userJobNumber = userJobNumber == null ? null : userJobNumber.trim();
    }
    
    /**
     * @return 用户头像地址
     */
    public String getAvatarUrl() {
        return avatarUrl;
    }
    
    /**
     * @param avatarUrl 用户头像地址
     */
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl == null ? null : avatarUrl.trim();
    }
    
    /**
     * @return 版本号
     */
    public Integer getVersion() {
        return version;
    }
    
    /**
     * @param version 版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }
    
    /**
     * @return 创建人UID
     */
    public String getCreatorUid() {
        return creatorUid;
    }
    
    /**
     * @param creatorUid 创建人UID
     */
    public void setCreatorUid(String creatorUid) {
        this.creatorUid = creatorUid == null ? null : creatorUid.trim();
    }
    
    /**
     * @return 创建时间
     */
    public Date getCreatedDt() {
        return createdDt == null ? null : (Date) createdDt.clone();
    }
    
    /**
     * @param createdDt 创建时间
     */
    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt == null ? null : (Date) createdDt.clone();
    }
    
    /**
     * @return 更新人UID
     */
    public String getUpdatorUid() {
        return updatorUid;
    }
    
    /**
     * @param updatorUid 更新人UID
     */
    public void setUpdatorUid(String updatorUid) {
        this.updatorUid = updatorUid == null ? null : updatorUid.trim();
    }
    
    /**
     * @return 更新时间
     */
    public Date getUpdateDt() {
        return updateDt == null ? null : (Date) updateDt.clone();
    }
    
    /**
     * @param updateDt 更新时间
     */
    public void setUpdateDt(Date updateDt) {
        this.updateDt = updateDt == null ? null : (Date) updateDt.clone();
    }
    
    /**
     * @return 是否删除,0:未删除;1:已删除;
     */
    public Boolean getDeleted() {
        return deleted;
    }
    
    /**
     * @param deleted 是否删除,0:未删除;1:已删除;
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    
    /**
     * @return 盐值8位长度随机数
     */
    public String getSalt() {
        return salt;
    }
    
    /**
     * @param salt 盐值8位长度随机数
     */
    public void setSalt(String salt) {
        this.salt = salt == null ? null : salt.trim();
    }
}
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/