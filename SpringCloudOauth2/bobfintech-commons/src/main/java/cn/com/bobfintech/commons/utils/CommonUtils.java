package cn.com.bobfintech.commons.utils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @project: 北银金科
 * @description: 公共工具类。
 * @version 1.0.0
 * @errorcode
 * 		  错误码: 错误描述
 *
 * @author
 *        <li>2020-07-03 guopengfei@bobfintech.com.cn Create 1.0
 * 
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Slf4j
public class CommonUtils
{
    /**
     * 获取机器mac地址
     *
     * @return
     */
    public static String getMACAddress()
    {
        String macStr = "";
        try {
            String osName = System.getProperty("os.name").toLowerCase();
            if (osName.equals("windows 7")) {
                return getWindows7OrMACAddress();
            }
            else if (osName.equals("windows 10")) {
                return getWindows7OrMACAddress();
            }
            else if (osName.startsWith("windows")) {
                return getWindowsMACAddress();
            }
            else {
                return getLinuxMACAddress();
            }
        }
        catch (Exception e) {
            LogsUtils.warnGlobal(log, null, "获取机器mac地址报错>>>>>>", e);
            macStr = "未知";
        }
        
        return macStr;
    }
    
    /**
     * 获取windows7或以上版本机器mac地址
     *
     * @return
     */
    public static String getWindows7OrMACAddress()
    {
        String macStr = "";
        try {
            InetAddress ia = InetAddress.getLocalHost();
            
            byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < mac.length; i++) {
                if (i != 0) {
                    sb.append("-");
                }
                String s = Integer.toHexString(mac[i] & 0xFF);
                sb.append(s.length() == 1 ? 0 + s : s);
            }
            macStr = sb.toString().toUpperCase().replaceAll("-", "");
        }
        catch (Exception e) {
            LogsUtils.warnGlobal(log, null, "获取机器mac地址报错>>>>>>", e);
            macStr = "未知";
        }
        
        return macStr;
    }
    
    /**
     * 获取LINUX网卡的MAC地址
     *
     * @return mac地址
     */
    public static String getLinuxMACAddress()
    {
        String[] cmd = { "ifconfig" };
        String mac = null;
        BufferedReader bufferedReader = null;
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(cmd);
            process.waitFor();
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream(), "UTF-8"));
            StringBuffer sb = new StringBuffer();
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            String str1 = sb.toString();
            String str2 = str1.split("ether")[1].trim();
            mac = str2.split("txqueuelen")[0].trim();
        }
        catch (Exception e) {
            LogsUtils.warnGlobal(log, null, "unix报错>>>>>>>>>>>>>>>>>>>", e);
            mac = "未知";
        }
        finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                }
                catch (IOException e) {
                    LogsUtils.warnGlobal(log, null, "关闭bufferReader报错>>>>>>>>>>>>>>>>>>>", e);
                }
            }
            bufferedReader = null;
            process = null;
        }
        return mac;
    }
    
    /**
     * 获取windows网卡的mac地址
     *
     * @return mac地址
     */
    public static String getWindowsMACAddress()
    {
        String mac = null;
        BufferedReader bufferedReader = null;
        Process process = null;
        try {
            process = Runtime.getRuntime().exec("ipconfig /all");
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream(), "UTF-8"));
            String line = null;
            int index = -1;
            while ((line = bufferedReader.readLine()) != null) {
                index = line.toLowerCase().indexOf("physical address");
                if (index >= 0) {
                    index = line.indexOf(":");
                    if (index >= 0) {
                        mac = line.substring(index + 1).trim();
                    }
                    break;
                }
            }
        }
        catch (Exception e) {
            mac = "未知";
        }
        finally {
            if (null != bufferedReader) {
                try {
                    bufferedReader.close();
                }
                catch (IOException e) {
                    LogsUtils.warnGlobal(log, null, "关闭bufferReader报错>>>>>>>>>>>>>>>>>>>", e);
                }
            }
            bufferedReader = null;
            process = null;
        }
        
        return mac;
    }
    
    
    /**
     * 通过反射方法给属性赋值
     *
     * @param obj
     *            实体对象
     * @param fieldName
     *            属性名称
     * @param fieldObj
     *            属性值
     * @throws Exception
     *             异常
     */
    public static void setSuperReflect(Object obj, String fieldName, Object fieldObj) throws Exception
    {
        Field field = obj.getClass().getSuperclass().getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(obj, fieldObj);
    }
    
    /**
     * 通过反射方法给属性赋值
     *
     * @param obj
     *            实体对象
     * @param fieldName
     *            属性名称
     * @param fieldObj
     *            属性值
     * @throws BusinessException
     *             异常
     */
    public static void setReflect(Object obj, String fieldName, Object fieldObj) throws BusinessException
    {
        Field field = null;
        try {
            field = obj.getClass().getDeclaredField(fieldName);
            if (null != field) {
                field.setAccessible(true);
                field.set(obj, fieldObj);
            }
        }
        catch (NoSuchFieldException ex) {
            LogsUtils.errorGlobal(log, null, "赋值异常：", ex);
            // 如果是属性不存在，则不赋值跳过
        }
        catch (Exception e) {
            LogsUtils.errorGlobal(log, null, "值转换异常", e);
            throw new BusinessException(
                BobfintechErrorNoEnum.COM_UNKNOWN_ERROR
                    .getErrorNo(),
                BobfintechErrorNoEnum.COM_UNKNOWN_ERROR.getErrorConsonleInfo(), e);
        }
        
    }

    
    /**
     * 方法说明：两个对象相同属性值进行比较
     * 
     * @param check
     *            源对象
     * @param tmp
     *            比较目标对象
     * @return 如果相同属性值比较全部相同返回空,否则返回不相同字段名称
     * @throws BusinessException
     *             异常
     */
    public static String checkCommon(Object check, Object tmp)
    {
        return checkCommon(check, tmp, null);
    }
    
    /**
     * 判断对象是否为空或空值、空格
     * 
     * @param value
     *            实体对象
     * @return
     */
    private static boolean isNull(Object value)
    {
        boolean result = false;
        if (null == value) {
            return true;
        }
        if (value instanceof String) {
            result = StringUtils.isBlank((String) value);
        }
        return result;
    }
    
    /**
     * 判断两个对象是否相等
     *
     * @param value1
     *            第一个对象
     * @param value2
     *            第二个对象
     * @param dataType
     *            对象类型
     * @return
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static boolean comparValue(Object value1, Object value2, Class<?> dataType)
    {
        
        if (isNull(value1) && isNull(value2)) {
            return true;
        }
        if (isNull(value1) || isNull(value2)) {
            return false;
        }
        if (BigDecimal.class.equals(dataType)) {
            BigDecimal bdVal1 = new BigDecimal(value1.toString());
            BigDecimal bdVal2 = new BigDecimal(value2.toString());
            return (0 == bdVal1.compareTo(bdVal2));
        }
        else if (Long.class.equals(dataType)) {
            Long bdVal1 = Long.valueOf(value1.toString());
            Long bdVal2 = Long.valueOf(value2.toString());
            return (0 == bdVal1.compareTo(bdVal2));
        }
        else {
            if (value1 instanceof Comparable) {
                return (0 == ((Comparable) value1).compareTo(value2));
            }
            else {
                return value1.equals(value2);
            }
        }
    }

    
    /**
     * 方法说明：两个对象相同属性值进行比较，可以自定义不需要匹配的属性
     *
     * @param check
     *            源对象
     * @param tmp
     *            比较对象
     * @param notNeedFieldList
     *            不需要匹配的属性列表
     * @return
     * @throws BusinessException
     *             异常
     */
    public static String checkCommon(Object check, Object tmp, List<String> notNeedFieldList) throws BusinessException
    {
        String msg = null;
        // 不需要匹配的属性列表
        List<String> list = new ArrayList<String>();
        
        if (CollectionUtils.isNotEmpty(notNeedFieldList)) {
            list.addAll(notNeedFieldList);
        }
        try {
            Field tmpFields[] = tmp.getClass().getDeclaredFields();
            Map<String, Object> checkMap = convert2MapReal(check);
            for (int j = 0; j < tmpFields.length; j++) {
                boolean flag = true;
                Field tmpField = tmpFields[j];
                tmpField.setAccessible(true);
                Object tmpValue = tmpField.get(tmp);
                String tmpFieldName = tmpField.getName();
                LogsUtils.debugGlobal(log, null, "方法[checkCommon]--->属性名:{}--属性类型：{}", tmpFieldName,
                    tmpField.getType());
                if (list.contains(tmpFieldName)) {
                    LogsUtils.debugGlobal(log, null, "不需比对的属性名：{}", tmpFieldName);
                    continue;
                }
                if (checkMap.containsKey(tmpFieldName)) {
                    Object checkValue = checkMap.get(tmpFieldName);
                    LogsUtils.debugGlobal(log, null, "方法[checkCommon]--->属性名:{}--属性值check:{}--属性类型check：{}",
                        tmpFieldName, checkValue, tmpField.getType());
                    LogsUtils.debugGlobal(log, null, "方法[checkCommon]--->属性名:{}--属性值tmp:{}--属性类型tmp：{}", tmpFieldName,
                        tmpValue, tmpField.getType());
                    
                    flag = comparValue(checkValue, tmpValue, tmpField.getType());
                    LogsUtils.debugGlobal(log, null, "方法[checkCommon]--->属性名:{}--属性值tmp:{}--比较结果：{}", tmpFieldName,
                        tmpValue, flag);
                }
                else {
                    LogsUtils.debugGlobal(log, null, "checkMapKey不包含{}", tmpFieldName);
                }
                if (!flag) {
                    if (StringUtils.isEmpty(msg)) {
                        msg = tmpFieldName;
                    }
                    else {
                        msg = msg + "," + tmpFieldName;
                    }
                }
            }
        }
        catch (Exception e) {
            throw new BusinessException(
                BobfintechErrorNoEnum.COM_UNKNOWN_ERROR
                .getErrorNo(),
                BobfintechErrorNoEnum.COM_UNKNOWN_ERROR.getErrorConsonleInfo(), e);
        }
        return msg;
    }
    
    /**
     * 方法说明：两个对象相同属性值进行比较，可以自定义需要匹配的属性
     *
     * @param check
     *            源对象
     * @param tmp
     *            比较对象
     * @param needFieldList
     *            需要匹配的属性列表
     * @return
     * @throws BusinessException
     *             异常
     */
    public static String checkObjCommon(Object check, Object tmp, List<String> needFieldList) throws BusinessException
    {
        StringBuilder msg = new StringBuilder();
        try {
            if (null == check && null == tmp) {
                return "";
            }
            if ((null == check && null != tmp) || (null != check && null == tmp)) {
                msg.append("比较对象，有空对象，不能比较");
                return msg.toString();
            }
            Field tmpFields[] = tmp.getClass().getDeclaredFields();
            Map<String, Object> checkMap = convert2MapReal(check);
            for (int j = 0; j < tmpFields.length; j++) {
                boolean flag = true;
                Field tmpField = tmpFields[j];
                tmpField.setAccessible(true);
                Object tmpValue = tmpField.get(tmp);
                String tmpFieldName = tmpField.getName();
                LogsUtils.debugGlobal(log, null, "方法[checkCommon]--->属性名:{}--属性类型：{}", tmpFieldName,
                    tmpField.getType());
                if (!needFieldList.contains(tmpFieldName)) {
                    LogsUtils.debugGlobal(log, null, "不需比对的属性名：{}", tmpFieldName);
                    continue;
                }
                Object checkValue = null;
                if (checkMap.containsKey(tmpFieldName)) {
                    checkValue = checkMap.get(tmpFieldName);
                    LogsUtils.debugGlobal(log, null,
                        "方法[checkCommon]--->属性名:{}--属性类型check：{}--属性值check:{}--比对的属性值tmp:{}--比对的属性类型tmp：{}",
                        tmpFieldName, tmpField.getType(), checkValue, tmpValue, tmpField.getType());
                    flag = comparValue(checkValue, tmpValue, tmpField.getType());
                    LogsUtils.infoGlobal(log, null, "方法[checkCommon]--->属性名:{}--比对的属性值tmp:{}--比较结果：{}", tmpFieldName,
                        tmpValue, flag);
                }
                else {
                    LogsUtils.debugGlobal(log, null, "checkMapKey不包含{}", tmpFieldName);
                }
                if (!flag) {
                    LogsUtils.errorGlobal(log, null, "比对要素不一致[属性名:{},原值:{},比对值{}", tmpFieldName, checkValue, tmpValue);
                    if (StringUtils.isBlank(msg.toString())) {
                        msg.append(tmpFieldName);
                    }
                    else {
                        msg.append("," + tmpFieldName);
                    }
                }
            }
        }
        catch (Exception e) {
            LogsUtils.errorGlobal(log, null, "比对数据异常", e);
            throw new BusinessException(
                BobfintechErrorNoEnum.COM_UNKNOWN_ERROR
                .getErrorNo(),
                BobfintechErrorNoEnum.COM_UNKNOWN_ERROR.getErrorConsonleInfo(), e);
        }
        return msg.toString();
    }
    
    /**
     * 将一个 JavaBean 对象转化为一个 Map
     * 
     * @param bean
     *            要转化的JavaBean 对象
     * @return 转化出来的 Map 对象
     * @throws BusinessException
     *             异常
     */
    @SuppressWarnings("rawtypes")
    public static Map<String, Object> convert2MapReal(Object bean) throws BusinessException
    {
        Class type = bean.getClass();
        Map<String, Object> returnMap = new HashMap<String, Object>();
        BeanInfo beanInfo = null;
        try {
            beanInfo = Introspector.getBeanInfo(type);
        }
        catch (IntrospectionException e) {
            LogsUtils.errorGlobal(log, null, "拿到Bean的描述失败", e);
            throw new BusinessException(
                BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo());
        }
        
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        for (int i = 0; i < propertyDescriptors.length; i++) {
            PropertyDescriptor descriptor = propertyDescriptors[i];
            String propertyName = descriptor.getName();
            if (!propertyName.equals("class")) {
                Method readMethod = descriptor.getReadMethod();
                Object result = null;
                try {
                    result = readMethod.invoke(bean, new Object[0]);
                }
                catch (Exception e) {
                    LogsUtils.errorGlobal(log, null, "读取对象的属性{}失败", descriptor.getName(), e);
                    throw new BusinessException(BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorNo(),
                        BobfintechErrorNoEnum.COM_SYS_ERROR.getErrorConsonleInfo());
                }
                LogsUtils.debugGlobal(log, null, "方法：convert2MapReal--->属性名：{}属性值：{}", propertyName, result);
                returnMap.put(propertyName, result);
                if (result != null) {
                    if (result instanceof String) {
                        if (StringUtils.isBlank((String) result)) {
                            returnMap.put(propertyName, null);
                        }
                    }
                }
            }
        }
        return returnMap;
    }
    
    /**
     * 判断某个对象中是否存在为空的属性值
     *
     * @param object
     *            对象
     * @param param
     *            属性列表
     * @return 如有值为空的列，返回值为空属性列表
     */
    public static String isBlankAny4Bean(Object object, List<String> param)
    {
        if (CollectionUtils.isEmpty(param)) {
            return null;
        }
        List<String> needFieldList = new ArrayList<String>();
        param.stream().forEach(p -> {
            needFieldList.add(JsonUtils.underlineTohump(p));
        });
        StringBuilder isBlankField = new StringBuilder();
        Field tmpFields[] = object.getClass().getDeclaredFields();
        Map<String, Object> checkMap = CommonUtils.convert2MapReal(object);
        for (int j = 0; j < tmpFields.length; j++) {
            Field tmpField = tmpFields[j];
            String tmpFieldName = tmpField.getName();
            if (needFieldList.contains(tmpFieldName)) {
                Object checkValue = checkMap.get(tmpFieldName);
                String fieldType = tmpField.getType().toString();
                if (fieldType.indexOf("String") != -1) {
                    if (checkValue == null || StringUtils.isBlank(checkValue.toString())) {
                        isBlankField.append(tmpFieldName).append(",");
                    }
                }
                else if (fieldType.indexOf("Integer") != -1) {
                    if (checkValue == null) {
                        isBlankField.append(tmpFieldName).append(",");
                    }
                }
                else if (fieldType.indexOf("Date") != -1) {
                    Date date = (Date) checkValue;
                    if (date == null) {
                        isBlankField.append(tmpFieldName).append(",");
                    }
                }
                
            }
        }
        
        if (isBlankField.length() != 0) {
            isBlankField.setLength(isBlankField.length() - 1);
        }
        return isBlankField.toString();
    }
}
