package cn.com.bobfintech.commons.utils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

import lombok.extern.slf4j.Slf4j;

/**
 * @project: 北银金科
 * @description: json工具类。
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-03 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Slf4j
public class JsonUtils
{
    
    /**
     * Java对象转Json字符串
     *
     * @param object
     *            Java对象，可以是对象，数组，List,Map等
     * @return json 字符串
     */
    public static String toJson(Object object)
    {
        String jsonString = "";
        try {
            jsonString = JSONObject.toJSONString(object, SerializerFeature.WriteMapNullValue);
        }
        catch (Exception e) {
            LogsUtils.warnGlobal(log, null, "json error", e);
        }
        return jsonString;
    }
    
    /**
     * json字符串反序列化成指定类型的对象
     *
     * @param <T>
     *            t
     * @param jsonString
     *            json字符串
     * @param clazz
     *            反序列化类型
     * @return
     */
    public static <T> T json2Object(String jsonString, Class<T> clazz)
    {
        
        if (jsonString == null || "".equals(jsonString)) {
            return null;
        }
        else {
            try {
                return JSONObject.parseObject(jsonString, clazz);
            }
            catch (Exception e) {
                LogsUtils.warnGlobal(log, null, "json error", e);
            }
        }
        return null;
        
    }
    
    /**
     * json字符串反序列化成指定泛型的List
     * 
     * @param t
     *            t
     * @param jsonString
     *            json字符串
     * @param clazz
     *            反序列化类型
     * @return
     */
    public static <T> List<T> json2List(String jsonString, Class<T> clazz)
    {
        if (jsonString == null || "".equals(jsonString)) {
            return null;
        }
        else {
            try {
                return JSONObject.parseArray(jsonString, clazz);
            }
            catch (Exception e) {
                LogsUtils.warnGlobal(log, null, "json error", e);
            }
        }
        return null;
        
    }
    
    /**
     * 下划线命名转驼峰命名
     * 
     * @param param
     *            下划线字符串命名
     * @return
     */
    public static String underlineTohump(String param)
    {
        StringBuilder result = new StringBuilder();
        String a[] = param.split("_");
        for (String s : a) {
            if (result.length() == 0) {
                result.append(s.toLowerCase());
            }
            else {
                result.append(s.substring(0, 1).toUpperCase());
                if (s.length() > 1) {
                    result.append(s.substring(1).toLowerCase());
                }
                
            }
        }
        return result.toString();
    }
    
    /**
     * 驼峰命名转下划线命名
     * 
     * @param param
     *            下划线字符串命名
     * @return
     */
    public static String humpToUnderline(String param)
    {
        Pattern pattern = Pattern.compile("[A-Z]");
        Matcher matcher = pattern.matcher(param);
        StringBuffer sb = new StringBuffer(param);
        if (matcher.find()) {
            sb = new StringBuffer();
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
            matcher.appendTail(sb);
        }
        else {
            return sb.toString();
        }
        return humpToUnderline(sb.toString());
    }
    
}
