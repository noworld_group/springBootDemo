package cn.com.bobfintech.commons.exception;

/**
 * @project: 北银金科
 * @description: 公共异常类
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-03 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
public class BusinessException extends BaseException
{
    /**
     * 
     */
    private static final long serialVersionUID = -4792137987566934582L;
    private String errorCode;
    
    public BusinessException(String msg)
    {
        super(msg);
    }
    
    public BusinessException(String errorCode, String msg, Throwable t)
    {
        super(msg, t);
        this.errorCode = errorCode;
    }
    
    public BusinessException(String errorCode, String msg)
    {
        super(msg);
        this.errorCode = errorCode;
    }
    
    public String getErrorCode()
    {
        return errorCode;
    }
    
    public void setErrorCode(String errorCode)
    {
        this.errorCode = errorCode;
    }
}

