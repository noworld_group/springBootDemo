package cn.com.bobfintech.commons.enums;

/**
 * 
 * @author 郭鹏飞
 * 
 *         返回码枚举值类
 * 
 */
public enum ResponseCodeEnum {

	SUCCESS("0", "成功"), FAIL("1", "失败");

	private final String code;

	private final String msg;

	ResponseCodeEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	public static ResponseCodeEnum get(String code) {
		for (ResponseCodeEnum responseCodeEnum : ResponseCodeEnum.values()) {
			if (responseCodeEnum.getCode().equals(code)) {
				return responseCodeEnum;
			}
		}
		return null;
	}

}
