package cn.com.bobfintech.commons.constant;

/**
 * @project: 北银金科
 * @description: elasticsearch 常量类
 *               类功能简介。
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-02 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
public class ElasticsearchContant
{
    /**
     * ES数据类型
     */
    // String
    public static final String DATA_TYPE_STRING = "01";
    // Map
    public static final String DATA_TYPE_MAP = "02";
    // 自定义bean
    public static final String DATA_TYPE_BEAN = "03";
    // jsonstr
    public static final String DATA_TYPE_JSON = "04";
    
    /**
     * 索引类型
     */
    // 添加
    public static final String ELASTICSEARCH_DATA_TYPE_CREATE = "create";
    // 添加/编辑
    public static final String ELASTICSEARCH_DATA_TYPE_INDEX = "index";
    
}
