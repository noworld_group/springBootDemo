package cn.com.bobfintech.commons;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author 郭鹏飞
 * 
 * 将通用的配置，代码从各个业务模块中抽取出来，在 common 里实现一份。
 * 将响应的格式统一起来。
 * 将异常等信息，日志等格式也统一起来，在 common 里实现。
 * 
 */
@SpringBootApplication
public class BobfintechCommonsApplication
{

	//参见文章https://www.jianshu.com/p/bd2b6549f5cc
    public static void main(String[] args) {
        SpringApplication.run(BobfintechCommonsApplication.class, args);
    }

}
