package cn.com.bobfintech.commons.enums;

/**
 * @project: 北银金科
 * @description: 返回码枚举值类。
 *               错误编码格式：码段一-码段二-码段三-码段四-码段五-码段六，ex:ERR-E-X-NC-BOBFINTECH-0001
 *               1.码段一：错误码前缀（固定值ERR）
 *               2.码段二：错误级别 M-信息、W-警告、E-错误、F-严重错误
 *               3.码段三：长度为1位，为错误的一级分类，用于错误的定向（确定错误处理大方向），
 *               指明当前错误应该由技术人员、业务人员(用户),还是第三方支持人员来检查或处理，相应的分类说明如下：
 *               技术类错误：指错误在于技术方面因素，或者问题需要由技术人员来检查或解决，一般倾向于硬件或者是系统软件的原因。
 *               对于此类错误，码段三应填“X”(eg.硬件错误、文件读写、数据库、网络通讯等)。
 *               业务类错误：指错误在于业务方面因素，或者问题需要由业务人员或用户来检查或处理，一般倾向于人为原因，亦可称应用类错误。
 *               对于此类错误，码段三应填“Y”(eg.密码错误、限额、权限、业务逻辑、风险控制等)。
 *               第三方错误：指错误在于第三方系统（外来/外购产品或中间件，或者国债以外的系统），一般可向第三方支持人员提请排查和解决。
 *               对于此类错误，码段三应填“Z”。（二级分类为ZZ）(eg.第三方产品技术错误、第三方服务错误等)
 *               4.码段四：长度为2位，为错误的二级分类，用于错误的定性（确定错误性质或严重程度）。
 *               按性质分类，将业务类错误分成8种；将技术类错误分成9种，将第三方类错误分成XX种，如下表：
 *               业 务 类 错 误: Y 校验错误 CK（Check）
 *               Y 交易错误 DA（Deal）
 *               Y 指令错误 IS（Instruction）
 *               Y 费用错误 FE（Fee）
 *               Y 利率错误 RT（Rate）
 *               Y 流程错误 FO（Flow）
 *               Y 客户错误 CT（Customer）
 *               Y 账务错误 AC（Account）
 *               ……
 *               技术类错误: X 不确定服务结果 NA（N/A）
 *               X 框架错误 FW（FrameWork）
 *               X 数据内容相关 DC（DataContext）
 *               X 读写相关 IO（I／O）
 *               X 数据库相关 DB（Database）
 *               X 网络通讯 NC（Network Communication）
 *               X 组件模块调用 CM（Components or Modules）
 *               X 重复调用 DO（Duplicate Operation）
 *               X 纯技术性错误 AT（Absolute Technique）
 *               ……
 *               第三方错误 Z 负载均衡 LB（LoadBalance）例如：F5
 *               Z 缓存 CH（Cache）例如：Redis
 *               Z 中间件 MW(Middleware)例如：WAS、MQ
 *               Z 安全设备 SE(Scurity Equipment)例如：加密机
 *               Z 调度相关 SD（Schedule）例如：Control M，Zookeepr
 *               Z 数据处理相关 DM（Data Processing），例如：informatica
 *               Z 报表相关 RP（Report）例如：congos,鼎红
 *               Z 联合运营单位相关 UC（Union Coperation）例如：大额、外汇交易中心、中金所、上交所
 *               ……
 *               5.码段五：长度为5位，用于对错误源进行分类和识别，内容填写出错系统的简称：
 *               TEST- 智能客服系统
 *               6.码段六：错误明细编码，长度为4位，用于对错误的细分，具体编码由各应用(组件)自行定义
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-03 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
public enum BobfintechErrorNoEnum
{
    
    COM_BOBFINTECH_SUCCESS("00000", "操作成功！"),

    /************************************通用类错误************************************/
    COM_DATABASE_ERROR("ERR-E-X-DB-BOB-0001", "数据库访问异常"), //
    COM_SYS_ERROR("ERR-E-Y-FO-BOB-0002", "系统异常"), //
    COM_UNKNOWN_ERROR("ERR-E-Y-NA-BOB-0003", "未知异常"), //
    COM_UNKNOWN_GET_VAL_ERROR("ERR-E-Y-FO-BOB-0004", "取值异常"), //
    COM_TARGETSERVICE_FAILURE("ERR-E-Y-NV-BOB-0005", "服务[服务编号={0}]调用失败，调用次数[{1}]"), //
    COM_TARGETSERVICE_UNKOWNEXE("ERR-E-Y-NV-BOB-0006", "服务[{0}]调用失败，未知异常 "), //
    COM_BOBFINTECH_BEAN_COPY_ERROR("ERR-E-Y-NV-BOB-0008","系统处理异常"),
    COM_BOBFINTECH_SERVICE_CAN_NOT_READ("ERR-E-Y-NV-BOB-0009", "服务暂时不可用"),
    COM_BOBFINTECH_SERVICE_AUTHORIZE_ERROR("ERR-E-Y-NV-BOB-0010", "服务授权异常"),
    COM_BOBFINTECH_SIGN_IN_ERROR("ERR-E-Y-NV-BOB-0011", "登录校验失败"),
    COM_BOBFINTECH_OAU_FAIL("ERR-E-Y-NV-BOB-0012", "鉴权失败，请重新登录"),
    COM_BOBFINTECH_CAN_NOT_VISIT_URI("ERR-E-Y-NV-BOB-0013", "无此项访问权限！"),
    COM_BOBFINTECH_ES_FAIL("ERR-E-Y-NV-BOB-0014", "ES访问异常！"),
    COM_BOBFINTECH_ERROR("ERR-E-Y-NV-BOB-0015", "{0}"),//
    /************************************通用类错误************************************/
    
    /************************************用户管理模块************************************/
    COM_BOBFINTECH_SIGN_IN_USER_NAME_CAN_NOT_BE_NULL("ERR-E-Y-NV-BOB-0501", "用户名不能为空"),
    COM_BOBFINTECH_SIGN_IN_USER_NOT_EXIST(
                    "ERR-E-Y-NV-BOB-0502", "用户不存在！"
    ), COM_BOBFINTECH_SIGN_IN_USER_NAME_OR_PWD_NOT_RIGHT("ERR-E-Y-NV-BOB-0502", "用户名或者密码不对");
    /************************************用户管理模块************************************/
    
    /** 错误编码 */
    private String errorNo;
    /** 错误信息 */
    private String errorConsonleInfo;
    
    /**
     * 构造方法。
     *
     * @param errorNo
     *            错误编码
     * @param errorConsonleInfo
     *            错误信息
     */
    private BobfintechErrorNoEnum(String errorNo, String errorConsonleInfo)
    {
        this.errorNo = errorNo;
        this.errorConsonleInfo = errorConsonleInfo;
    }
    
    /**
     * @return 错误编码
     */
    public String getErrorNo()
    {
        return errorNo;
    }
    
    /**
     * @return 错误信息
     */
    public String getErrorConsonleInfo()
    {
        return errorConsonleInfo;
    }
    
    public static BobfintechErrorNoEnum get(String errorNo)
    {
        for (BobfintechErrorNoEnum responseCodeEnum : BobfintechErrorNoEnum.values()) {
            if (responseCodeEnum.getErrorNo().equals(errorNo)) {
                return responseCodeEnum;
            }
        }
        return null;
    }
    
}
