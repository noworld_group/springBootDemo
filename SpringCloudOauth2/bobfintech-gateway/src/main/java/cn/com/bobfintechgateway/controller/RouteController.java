package cn.com.bobfintechgateway.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.csp.sentinel.annotation.SentinelResource;

import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.pojo.BaseResponse;
import cn.com.bobfintech.commons.pojo.ResponseData;
import cn.com.bobfintechgateway.pojo.GatewayRouteInfo;
import cn.com.bobfintechgateway.route.GatewayServiceHandler;

/***
 * @project: 北银金科
 * @description: 路由controller
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-15 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@RestController
@RequestMapping("/route")
public class RouteController
{
    
    @Autowired
    private GatewayServiceHandler gatewayServiceHandler;
    
    /**
     * 刷新路由配置
     *
     * @param gwdefinition
     * @return
     */
    @SentinelResource("testSentinel")
    @GetMapping("/refresh")
    public BaseResponse refresh() throws Exception
    {
        this.gatewayServiceHandler.loadRouteConfig();
        return ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_SUCCESS);
    }
    
    @SuppressWarnings("rawtypes")
    @GetMapping("/routes")
    public ResponseData routes() throws Exception
    {
        List<GatewayRouteInfo> gatewayRouteInfos = gatewayServiceHandler.queryAllRoutes();
        
        return ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_SUCCESS, gatewayRouteInfos);
    }
    
}
