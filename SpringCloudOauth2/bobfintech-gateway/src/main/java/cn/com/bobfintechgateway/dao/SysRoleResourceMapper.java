package cn.com.bobfintechgateway.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.bobfintech.commons.entity.user.SysRoleResource;
import cn.com.bobfintech.commons.entity.user.SysRoleResourceExample;

/**
 * @project:用户管理模块
 * @description:角色菜单关系表
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 825338623@qq.com Create 1.0
 * @copyright ©2017-2019
 */
@Mapper
public interface SysRoleResourceMapper {
    
    /**
     * @param example example
     * @return int
     */
    int countByExample(SysRoleResourceExample example);

    
    /**
     * @param example example
     * @return int
     */
    int deleteByExample(SysRoleResourceExample example);

    
    /**
     * @param id id
     * @return int
     */
    int deleteByPrimaryKey(String id);

    
    /**
     * @param record record
     * @return int
     */
    int insert(SysRoleResource record);

    
    /**
     * @param record record
     * @return int
     */
    int insertSelective(SysRoleResource record);

    
    /**
     * @param example example
     * @return List<SysRoleResource>
     */
    List<SysRoleResource> selectByExample(SysRoleResourceExample example);

    
    /**
     * @param id id
     * @return SysRoleResource
     */
    SysRoleResource selectByPrimaryKey(String id);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExampleSelective(@Param("record") SysRoleResource record, @Param("example") SysRoleResourceExample example);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExample(@Param("record") SysRoleResource record, @Param("example") SysRoleResourceExample example);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKeySelective(SysRoleResource record);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKey(SysRoleResource record);
}
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/