package cn.com.bobfintechgateway.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.bobfintech.commons.entity.user.SysUserRole;
import cn.com.bobfintech.commons.entity.user.SysUserRoleExample;

/**
 * @project:用户管理模块
 * @description:用户角色关系表
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 825338623@qq.com Create 1.0
 * @copyright ©2017-2019
 */
@Mapper
public interface SysUserRoleMapper {
    
    /**
     * @param example example
     * @return int
     */
    int countByExample(SysUserRoleExample example);

    
    /**
     * @param example example
     * @return int
     */
    int deleteByExample(SysUserRoleExample example);

    
    /**
     * @param id id
     * @return int
     */
    int deleteByPrimaryKey(String id);

    
    /**
     * @param record record
     * @return int
     */
    int insert(SysUserRole record);

    
    /**
     * @param record record
     * @return int
     */
    int insertSelective(SysUserRole record);

    
    /**
     * @param example example
     * @return List<SysUserRole>
     */
    List<SysUserRole> selectByExample(SysUserRoleExample example);

    
    /**
     * @param id id
     * @return SysUserRole
     */
    SysUserRole selectByPrimaryKey(String id);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExampleSelective(@Param("record") SysUserRole record, @Param("example") SysUserRoleExample example);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExample(@Param("record") SysUserRole record, @Param("example") SysUserRoleExample example);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKeySelective(SysUserRole record);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKey(SysUserRole record);
}
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/