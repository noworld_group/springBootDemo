package cn.com.bobfintechgateway.config.security;

import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.server.authentication.HttpBasicServerAuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.server.ServerWebExchange;

import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.pojo.BaseResponse;
import cn.com.bobfintech.commons.pojo.ResponseData;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * @project: 北银金科
 * @description: 认证成功后访问新的接口需在请求头中添加基于httpbasic的认证鉴权信息，服务端收到请求后通过识别为httpbasic的鉴权信息，通过ServerHttpBasicAuthenticationConverter提取用户名和密码后进行鉴权，鉴权通过放行请求。
 *               此处自定义鉴权失败时的处理逻辑CustomHttpBasicServerAuthenticationEntryPoint，只需继承默认的httpbasic鉴权失败处理器HttpBasicServerAuthenticationEntryPoint并覆盖其commence方法即可
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-06 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Component
@Slf4j
public class CustomHttpBasicServerAuthenticationEntryPoint extends HttpBasicServerAuthenticationEntryPoint
/* implements ServerAuthenticationEntryPoint */ {
    
    private static final String WWW_AUTHENTICATE        = "WWW-Authenticate";
    private static final String DEFAULT_REALM           = "Realm";
    private static String       WWW_AUTHENTICATE_FORMAT = "Basic realm=\"%s\"";
    private String              headerValue             = createHeaderValue("Realm");
    
    public CustomHttpBasicServerAuthenticationEntryPoint()
    {
    }
    
    @Override
    public void setRealm(String realm)
    {
        this.headerValue = createHeaderValue(realm);
    }
    
    private static String createHeaderValue(String realm)
    {
        Assert.notNull(realm, "realm cannot be null");
        return String.format(WWW_AUTHENTICATE_FORMAT, new Object[] { realm });
    }
    
    public Mono<Void> commence(ServerWebExchange exchange, AuthenticationException e)
    {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        response.getHeaders().add("Content-Type", "application/json; charset=UTF-8");
        response.getHeaders().set(HttpHeaders.AUTHORIZATION, this.headerValue);
        log.info("统一处理信息为:[{}]", e.getMessage());
        
        BaseResponse responseData = ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_OAU_FAIL);
        byte[] dataBytes = responseData.toString().getBytes();
        DataBuffer bodyDataBuffer = response.bufferFactory().wrap(dataBytes);
        return response.writeWith(Mono.just(bodyDataBuffer));
    }
}
