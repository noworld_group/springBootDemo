package cn.com.bobfintechgateway.sentinel;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.web.reactive.result.view.ViewResolver;

import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayFlowRule;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayRuleManager;
import com.alibaba.csp.sentinel.adapter.gateway.sc.SentinelGatewayFilter;
import com.alibaba.csp.sentinel.adapter.gateway.sc.exception.SentinelGatewayBlockExceptionHandler;
import com.alibaba.fastjson.JSONObject;

import cn.com.bobfintech.commons.constant.BobfintechContant;
import cn.com.bobfintech.commons.utils.StringUtils;
import cn.com.bobfintechgateway.component.MyRedisComponent;
import cn.com.bobfintechgateway.pojo.FlowRuleEntity;
import lombok.extern.slf4j.Slf4j;

/**
 * @project: 北银金科
 * @description: 客户端初始化配置信息
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-20 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Slf4j
@Configuration
public class GatewayConfiguration {

    private final List<ViewResolver> viewResolvers;
    private final ServerCodecConfigurer serverCodecConfigurer;
    
    @Autowired
    private MyRedisComponent myRedisComponent;

    public GatewayConfiguration(ObjectProvider<List<ViewResolver>> viewResolversProvider,
                                ServerCodecConfigurer serverCodecConfigurer) {
        this.viewResolvers = viewResolversProvider.getIfAvailable(Collections::emptyList);
        this.serverCodecConfigurer = serverCodecConfigurer;
    }

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public SentinelGatewayBlockExceptionHandler sentinelGatewayBlockExceptionHandler() {
        // Register the block exception handler for Spring Cloud Gateway.
        return new SentinelGatewayBlockExceptionHandler(viewResolvers, serverCodecConfigurer);
    }

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public GlobalFilter sentinelGatewayFilter() {
        return new SentinelGatewayFilter();
    }
    
    @PostConstruct
    public void doInit()
    {
        log.info("从redis读取限流规则 begin >>>>>>>>>>>>>>>>");
        // 第一次启动应用时，加载redis中的限流规则
        String value = (String) myRedisComponent
            .get(BobfintechContant.SENTINEL_REDIS_RULE_FLOW_KEY);
        if (StringUtils.isNotBlank(value)) {
            log.info("初始化应用时从redis读取到的限流规则为:【{}】", value);
            List<FlowRuleEntity> ruleLists = JSONObject.parseArray(value, FlowRuleEntity.class);
            if (CollectionUtils.isNotEmpty(ruleLists)) {
                Set<GatewayFlowRule> rules = new HashSet<>();
                for (FlowRuleEntity flowRuleEntity : ruleLists) {
                    rules.add(new GatewayFlowRule(flowRuleEntity
                        .getRefResource()).setCount(flowRuleEntity
                            .getCount())
                            .setIntervalSec(
                                1));
                }
                GatewayRuleManager.loadRules(rules);
            }
        }
        log.info("从redis读取限流规则 end >>>>>>>>>>>>>>>>");
    }
}