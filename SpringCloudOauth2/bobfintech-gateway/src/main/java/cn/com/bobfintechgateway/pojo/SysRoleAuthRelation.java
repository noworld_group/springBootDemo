package cn.com.bobfintechgateway.pojo;

import java.util.Date;

import lombok.Data;

/**
 * @project: 北银金科
 * @description:角色权限中间表
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-07 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Data
public class SysRoleAuthRelation {
    private String id;
    private String roleId;
    private String authId;
    private Date createTime;
    private Date updateTime;

}