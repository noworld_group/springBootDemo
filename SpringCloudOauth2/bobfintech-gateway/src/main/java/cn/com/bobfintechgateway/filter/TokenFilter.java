package cn.com.bobfintechgateway.filter;
 
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import cn.com.bobfintech.commons.constant.BobfintechContant;
import cn.com.bobfintech.commons.entity.user.SysResource;
import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.pojo.ResponseData;
import cn.com.bobfintech.commons.utils.BeanUtils;
import cn.com.bobfintech.commons.utils.StringUtils;
import cn.com.bobfintechgateway.component.MyRedisComponent;
import cn.com.bobfintechgateway.pojo.UserInfo;
import cn.com.bobfintechgateway.service.SecurityUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
 
/**
 * @project: 北银金科
 * @description: Token处理器
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-07 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Component
@Slf4j
public class TokenFilter implements WebFilter {

    @Autowired
    private MyRedisComponent redisUtil;
    
    @Autowired
    private SecurityUserDetailsService securityUserDetailsService;
    
    @Value("${server.permission}")
    private boolean permission;
    
    /**
     * 根据客户端传递的token，更新redis用户信息
     * 
     * @param exchange
     *            the current server exchange
     * @param chain
     *            provides a way to delegate to the next filter
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain)
    {
        log.info("第一个filter !");
        log.info("begin check  token for user requestd >>>>>>>>>>>>>>>>>>>>>>>>");
        ServerHttpRequest request = exchange.getRequest();
        String token = request.getHeaders().getFirst(BobfintechContant.CLIENT_TOKEN_KEY);
        String requestUrl = request.getURI().getPath();
        if (StringUtils.isNotBlank(token)) {
            UserInfo userInfo = (UserInfo) redisUtil.get(BobfintechContant.TOKEN_REDIS_KEY + token);
            if (userInfo != null) {
                log.info("查询到的用户信息,token：[{}],用户名:[{}],用户密码:[{}]", BobfintechContant.TOKEN_REDIS_KEY + token,
                    userInfo.getUsername(), userInfo.getPassword());
                long expireTime = userInfo.getExpireTime();
                long currentTime = System.currentTimeMillis();
                if (expireTime - currentTime <= BobfintechContant.MINUTES_5) {
                    UserDetails userDetails = securityUserDetailsService.queryUserByUserName(userInfo.getUsername());
                    BeanUtils.copyProperties(userInfo, userDetails);
                    userInfo.setToken(token);
                    userInfo.setLoginTime(System.currentTimeMillis());
                    userInfo.setExpireTime(userInfo.getLoginTime() + BobfintechContant.USER_TOKEN_TIME * 1000);
                    // 更新缓存的用户信息
                    redisUtil.set(BobfintechContant.TOKEN_REDIS_KEY + token, userInfo,
                        BobfintechContant.USER_TOKEN_TIME);
                }
                // 权限验证
                if (permission) {
                    log.info("需要做权限校验！");
                    List<SysResource> sysResources = userInfo.getWebAuthes();
                    if (CollectionUtils.isEmpty(sysResources)) {
                        // 该用户没有任何权限，直接返回
                        ServerHttpResponse response = exchange.getResponse();
                        byte[] bits = ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_CAN_NOT_VISIT_URI)
                            .toString().getBytes();
                        DataBuffer buffer = response.bufferFactory().wrap(bits);
                        return response.writeWith(Mono.just(buffer));
                        // return Mono.error(new BadCredentialsException(
                        // ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_CAN_NOT_VISIT_URI).toString()));
                    }
                    else {
                        boolean isPass = false;
                        for (SysResource sysResource : sysResources) {
                            if (StringUtils.equals(sysResource.getResourceUrl(), requestUrl)) {
                                isPass = true;
                                break;
                            }
                        }
                        if (!isPass) {
                            // 该用户没有任何权限，直接返回
                            ServerHttpResponse response = exchange.getResponse();
                            byte[] bits = ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_CAN_NOT_VISIT_URI)
                                .toString().getBytes();
                            DataBuffer buffer = response.bufferFactory().wrap(bits);
                            return response.writeWith(Mono.just(buffer));
                        }
                        else {
                            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                                userInfo.getUsername(), null, userInfo.getAuthorities());
                            SecurityContextHolder.getContext().setAuthentication(authentication);
                        }
                    }
                }
                
            }
            // ServerHttpRequest authErrorReq = request.mutate().path("/auth/error").build();
            // // erverWebExchange.mutate类似，构建一个新的ServerWebExchange
            // ServerWebExchange authErrorExchange =
            // exchange.mutate().request(authErrorReq).build();
            // return chain.filter(authErrorExchange);
            //
            // exchange.getResponse().writeWith(Flux.just(exchange.getResponse().bufferFactory()
            // .wrap(JsonUtil.toJson(new BaseErrorVO().fail(e.getMessageCode(),
            // e.getSystemMessage())).getBytes())));
            
        }
        return chain.filter(exchange);
    }
 
}