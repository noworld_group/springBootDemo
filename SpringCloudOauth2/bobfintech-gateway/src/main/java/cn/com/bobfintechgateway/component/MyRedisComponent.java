package cn.com.bobfintechgateway.component;

import cn.com.bobfintech.commons.utils.RedisUtil;
import org.springframework.stereotype.Component;

/**
 * @ClassName: MyRedisComponent
 * @Description: 继承RedisUtil(不加包扫描，使用继承引入bean)
 * Author:  Jiaxi
 * Date: 2020/6/29 18:30
 */
@Component
public class MyRedisComponent extends RedisUtil{

}

