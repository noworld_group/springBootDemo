package cn.com.bobfintechgateway.pojo;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cn.com.bobfintech.commons.entity.user.SysResource;
import cn.com.bobfintech.commons.entity.user.SysRole;
import lombok.Data;

/**
 * @project: 北银金科
 * @description: 用户信息Security
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-07 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Data
public class UserInfo implements UserDetails
{
    
    private static final long         serialVersionUID = -5536977220434963292L;
    private String                    id;
    private String                    username;
    private List<SysRole>     roleInfos;
    @JsonIgnore
    private String                    password;
    @JsonIgnore
    private String                    token;
    /** 登陆时间戳（毫秒） */
    private Long                      loginTime;
    /** 过期时间戳 */
    private Long                      expireTime;
    private List<SysResource> webAuthes;
    
    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        if (CollectionUtils.isEmpty(webAuthes)) {
            return null;
        }
        return webAuthes
            .parallelStream()
            .map(p -> new SimpleGrantedAuthority(p.getId())).collect(Collectors.toSet());
    }
    
    // 账户是否未过期
    @JsonIgnore
    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }
    
    // 账户是否未锁定
    @JsonIgnore
    @Override
    public boolean isAccountNonLocked()
    {
        return true;
    }
    
    // 密码是否未过期
    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }
    
    // 账户是否激活
    @JsonIgnore
    @Override
    public boolean isEnabled()
    {
        return true;
    }
    
}
