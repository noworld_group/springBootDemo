package cn.com.bobfintechgateway.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.bobfintech.commons.entity.user.SysUser;
import cn.com.bobfintech.commons.entity.user.SysUserExample;

/**
 * @project:用户管理模块
 * @description:用户表
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 825338623@qq.com Create 1.0
 * @copyright ©2017-2019
 */
@Mapper
public interface SysUserMapper {
    
    /**
     * @param example example
     * @return int
     */
    int countByExample(SysUserExample example);

    
    /**
     * @param example example
     * @return int
     */
    int deleteByExample(SysUserExample example);

    
    /**
     * @param id id
     * @return int
     */
    int deleteByPrimaryKey(String id);

    
    /**
     * @param record record
     * @return int
     */
    int insert(SysUser record);

    
    /**
     * @param record record
     * @return int
     */
    int insertSelective(SysUser record);

    
    /**
     * @param example example
     * @return List<SysUser>
     */
    List<SysUser> selectByExample(SysUserExample example);

    
    /**
     * @param id id
     * @return SysUser
     */
    SysUser selectByPrimaryKey(String id);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExampleSelective(@Param("record") SysUser record, @Param("example") SysUserExample example);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExample(@Param("record") SysUser record, @Param("example") SysUserExample example);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKeySelective(SysUser record);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKey(SysUser record);
}
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/