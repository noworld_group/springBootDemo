package cn.com.bobfintechgateway.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.bobfintech.commons.entity.user.SysResource;
import cn.com.bobfintech.commons.entity.user.SysResourceExample;

/**
 * @project:用户管理模块
 * @description:新表菜单
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 825338623@qq.com Create 1.0
 * @copyright ©2017-2019
 */
@Mapper
public interface SysResourceMapper {
    
    /**
     * @param example example
     * @return int
     */
    int countByExample(SysResourceExample example);

    
    /**
     * @param example example
     * @return int
     */
    int deleteByExample(SysResourceExample example);

    
    /**
     * @param id id
     * @return int
     */
    int deleteByPrimaryKey(String id);

    
    /**
     * @param record record
     * @return int
     */
    int insert(SysResource record);

    
    /**
     * @param record record
     * @return int
     */
    int insertSelective(SysResource record);

    
    /**
     * @param example example
     * @return List<SysResource>
     */
    List<SysResource> selectByExample(SysResourceExample example);

    
    /**
     * @param id id
     * @return SysResource
     */
    SysResource selectByPrimaryKey(String id);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExampleSelective(@Param("record") SysResource record, @Param("example") SysResourceExample example);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExample(@Param("record") SysResource record, @Param("example") SysResourceExample example);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKeySelective(SysResource record);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKey(SysResource record);
}
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/