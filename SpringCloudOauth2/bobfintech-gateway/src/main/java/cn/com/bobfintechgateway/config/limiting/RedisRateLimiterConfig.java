package cn.com.bobfintechgateway.config.limiting;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

/**
 * @project: 北银金科
 * @description: 限流器-ip限流
 *               类功能简介。
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-01 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Configuration
public class RedisRateLimiterConfig
{
    /**
     * 针对ip限流
     * 
     * @return
     */
    @Bean
    public KeyResolver ipKeyResolver()
    {
        return new KeyResolver()
        {
            /**
             * X-Forwarded-For 是一个扩展头。HTTP/1.1（RFC 2616）协议并没有对它的定义，它最开始是由 Squid 这个缓存代理软件引入，用来表示 HTTP
             * 请求端真实 IP，现在已经成为事实上的标准，被各大 HTTP 代理、负载均衡等转发服务广泛使用，并被写入 RFC 7239（Forwarded HTTP
             * Extension）标准之中
             * 链接：https://www.jianshu.com/p/15f3498a7fad
             * 
             * @see org.springframework.cloud.gateway.filter.ratelimit.KeyResolver#resolve(org.springframework.web.server.ServerWebExchange)
             */
            @Override
            public Mono<String> resolve(ServerWebExchange exchange)
            {
                // 获得用户iP
                // String ip = exchange.getRequest().getRemoteAddress().getHostString();
                String ip = exchange.getRequest().getHeaders().getFirst("X-Forwarded-For");
                System.out.println("测试进入方法>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                return Mono.just(ip);
            }
        };
    }
    
    /**
     * 按照Path限流
     *
     * @return key
     */
    // @Bean
    // public KeyResolver pathKeyResolver()
    // {
    // return exchange -> Mono.just(exchange.getRequest().getPath().toString());
    // }
    
    /**
     * 针对用户限流
     *
     * @return
     */
    // @Bean
    // public KeyResolver userKeyResolver()
    // {
    // return exchange -> Mono.just(exchange.getRequest().getQueryParams().getFirst("user"));
    // }
    
}
