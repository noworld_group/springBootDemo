package cn.com.bobfintechgateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.com.bobfintech.commons.constant.BobfintechContant;
import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.pojo.BaseResponse;
import cn.com.bobfintech.commons.pojo.ResponseData;
import cn.com.bobfintech.commons.utils.StringUtils;
import cn.com.bobfintechgateway.component.MyRedisComponent;
import cn.com.bobfintechgateway.pojo.UserInfo;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class GateWayContoller {

    @Autowired
    private MyRedisComponent redisUtil;
    
    /**
     * 用户信息查询
     *
     * @param token
     *            token
     * @return
     */
    @GetMapping("/auth/info")
    @ResponseBody
    public BaseResponse userInfo(String token) {
        log.info("开始查询redis中缓存的用户！");
        if (StringUtils.isNotBlank(token)) {
            UserInfo userInfo = (UserInfo) redisUtil.get(BobfintechContant.TOKEN_REDIS_KEY + token);
            if (userInfo == null) {
                return ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_OAU_FAIL);
            }
            else {
                return ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_SUCCESS, userInfo);
            }
        }
        else {
            return ResponseData.out(BobfintechErrorNoEnum.COM_UNKNOWN_GET_VAL_ERROR);
        }
        
    }
}
