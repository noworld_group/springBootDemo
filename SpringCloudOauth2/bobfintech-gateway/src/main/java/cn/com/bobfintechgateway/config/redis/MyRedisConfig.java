package cn.com.bobfintechgateway.config.redis;

import cn.com.bobfintech.commons.config.RedisConfig;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: RedisConfiguration
 * @Description: redis序列化配置，(继承redisconfig，不加包扫描的情况引入bean)
 * Author:  Jiaxi
 * Date: 2020/6/29 18:30
 */
@Configuration
public class MyRedisConfig extends RedisConfig {

}
