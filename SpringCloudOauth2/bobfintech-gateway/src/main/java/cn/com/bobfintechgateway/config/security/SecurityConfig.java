package cn.com.bobfintechgateway.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.context.NoOpServerSecurityContextRepository;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;

import cn.com.bobfintechgateway.filter.TokenFilter;
import lombok.extern.slf4j.Slf4j;

/**
 * @project: 北银金科
 * @description: security主配置类。
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-06 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@EnableWebFluxSecurity
@Slf4j
public class SecurityConfig
{
    /**
     * 登录校验通过的处理逻辑
     */
    @Autowired
    private AuthenticationSuccessHandler                  authenticationSuccessHandler;
    /**
     * 登录校验失败的处理逻辑
     */
    @Autowired
    private AuthenticationFaillHandler                    authenticationFaillHandler;
    /**
     * 鉴权失败的处理逻辑
     */
    @Autowired
    private CustomHttpBasicServerAuthenticationEntryPoint customHttpBasicServerAuthenticationEntryPoint;
    /**
     * token 处理filter
     */
    @Autowired
    private TokenFilter                                   tokenFilter;
    

    // security的鉴权排除列表
    // private static final String[] excludedAuthPages = { "/api/socket/**" };
    private static final String[] excludedAuthPages = { "/auth/login", "/auth/logout", "/route/**" };
    /**
     * 将登陆后的用户及权限信息存入session中
     * 
     * @return
     */
    @Bean
    ServerSecurityContextRepository serverSecurityContextRepository()
    {
        return NoOpServerSecurityContextRepository.getInstance();
    }
    
    @Bean
    SecurityWebFilterChain webFluxSecurityFilterChain(ServerHttpSecurity http) throws Exception
    {
        http.authorizeExchange().pathMatchers(excludedAuthPages).permitAll() // 无需进行权限过滤的请求路径
            .pathMatchers(HttpMethod.OPTIONS).permitAll() // option 请求默认放行
            .anyExchange().authenticated().and().httpBasic().and().formLogin().loginPage(
                "/auth/login")
            .authenticationSuccessHandler(authenticationSuccessHandler) // 认证成功
            .authenticationFailureHandler(authenticationFaillHandler) // 登陆验证失败
            .and().exceptionHandling().authenticationEntryPoint(customHttpBasicServerAuthenticationEntryPoint) // 基于http的接口请求鉴权失败
            .and()
            .csrf()
            .disable()// 必须支持跨域
            .logout().logoutUrl("/auth/logout");
        // .logoutSuccessHandler(logoutSuccessHandlerWebFlux);//成功登出时调用的自定义处理类
        http.addFilterBefore(tokenFilter, SecurityWebFiltersOrder.FIRST);
        return http.build();
    }
    
    /**
     * 如果使用本方法实现，需要在下面的方法体中全量缓存用户信息到session中，
     * 而如果使用SecurityUserDetailsService，则可以自定义从数据库中读取用户信息，而不是从 缓存中
     * 
     * @return
     */
    // @Bean
    // public MapReactiveUserDetailsService userDetailsService()
    // {
    // User.UserBuilder userBuilder = User.withDefaultPasswordEncoder();
    // UserDetails rob = userBuilder.username("rob").password("rob").roles("USER").build();
    // UserDetails admin = userBuilder.username("admin").password("admin").roles("USER",
    // "ADMIN").build();
    // return new MapReactiveUserDetailsService(rob, admin);
    // }
    
    /**
     * 密码加密工具
     * 
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder();
    }
    

}
