package cn.com.bobfintechgateway.config.security;

import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.server.WebFilterExchange;
import org.springframework.security.web.server.authentication.ServerAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import com.fasterxml.jackson.databind.ObjectMapper;

import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.pojo.ResponseData;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * @project: 北银金科
 * @description: 登陆失败后执行的处理器,返回客户端错误信息
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-07 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Component
@Slf4j
public class AuthenticationFaillHandler implements ServerAuthenticationFailureHandler
{
    
    public Mono<Void> onAuthenticationFailure(WebFilterExchange webFilterExchange, AuthenticationException exception)
    {
        ServerWebExchange exchange = webFilterExchange.getExchange();
        ServerHttpResponse response = exchange.getResponse();
        // 设置headers
        HttpHeaders httpHeaders = response.getHeaders();
        httpHeaders.add("Content-Type", "application/json; charset=UTF-8");
        httpHeaders.add("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
        // 设置body
        byte[] dataBytes = {};
        try {
            ObjectMapper mapper = new ObjectMapper();
            String errorMsg = exception.getMessage().toString();
            if (errorMsg.contains("Invalid Credentials")) {
                ResponseData<String> resultMap = new ResponseData<String>(
                    BobfintechErrorNoEnum.COM_BOBFINTECH_SIGN_IN_USER_NAME_OR_PWD_NOT_RIGHT);
                dataBytes = mapper.writeValueAsBytes(resultMap);
            }
            else {
                dataBytes = mapper.writeValueAsBytes(errorMsg);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        DataBuffer bodyDataBuffer = response.bufferFactory().wrap(dataBytes);
        return response.writeWith(Mono.just(bodyDataBuffer));
    }
    
}
