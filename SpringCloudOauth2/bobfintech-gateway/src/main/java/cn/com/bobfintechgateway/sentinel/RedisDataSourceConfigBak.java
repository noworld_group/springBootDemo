// package cn.com.bobfintechgateway.sentinel;
//
// import java.util.List;
//
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.boot.ApplicationArguments;
// import org.springframework.boot.ApplicationRunner;
// import org.springframework.stereotype.Component;
//
// import com.alibaba.csp.sentinel.datasource.Converter;
// import com.alibaba.csp.sentinel.datasource.ReadableDataSource;
// import com.alibaba.csp.sentinel.datasource.redis.RedisDataSource;
// import com.alibaba.csp.sentinel.datasource.redis.config.RedisConnectionConfig;
// import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
// import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
// import com.alibaba.fastjson.JSON;
// import com.alibaba.fastjson.TypeReference;
//
/// ***
// * @project: 北银金科
// * @description: 客户端在启动时初始化Redis中规则
// * @version 1.0.0
// * @errorcode
// * 错误码: 错误描述
// * @author
// * <li>2020-07-17 guopengfei@bobfintech.com.cn Create 1.0
// * @copyright ©2019-2020 北银金科，版权所有。
// */
// @Component
// public class RedisDataSourceConfigBak implements ApplicationRunner
// {
// private static final Logger log = LoggerFactory.getLogger(RedisDataSourceConfigBak.class);
//
// @Value("${spring.redis.host}")
// public String redisHost;
//
// @Value("${spring.redis.port}")
// public int redisPort;
//
// @Value("${spring.redis.password}")
// public String redisPass;
//
// @Value("${spring.redis.database}")
// public Integer database;
//
// // 限流规则key前缀
// public final String RULE_FLOW = "sentinel_rule_flow_";
// public final String RULE_FLOW_CHANNEL = "sentinel_rule_flow_channel";
//
// // 降级规则key前缀
// // public final String RULE_DEGRADE = "sentinel_rule_degrade_";
// // public final String RULE_DEGRADE_CHANNEL = "sentinel_rule_degrade_channel";
//
// // 系统规则key前缀
// // public final String RULE_SYSTEM = "sentinel_rule_system_";
// // public final String RULE_SYSTEM_CHANNEL = "sentinel_rule_system_channel";
//
// /**
// * ApplicationRunner
// * 该接口的方法会在服务启动之后被立即执行
// * 主要用来做一些初始化的工作
// * 但是该方法的运行是在SpringApplication.run(…​) 执行完毕之前执行
// */
// @Override
// public void run(ApplicationArguments args)
// {
// log.info("执行sentinel规则初始化 start >>>>>>>>>>>>>");
// RedisConnectionConfig config =
// RedisConnectionConfig.builder().withHost(redisHost).withPort(redisPort)
// .withPassword(redisPass).withDatabase(database).build();
// Converter<String, List<FlowRule>> parser = source -> JSON.parseObject(source,
// new TypeReference<List<FlowRule>>()
// {});
//
// ReadableDataSource<String, List<FlowRule>> redisDataSource = new RedisDataSource<>(config,
// RULE_FLOW,
// RULE_FLOW_CHANNEL, parser);
// FlowRuleManager.register2Property(redisDataSource.getProperty());
// log.info("执行sentinel规则初始化 end >>>>>>>>>>>>>");
//
// // Converter<String, List<DegradeRule>> parserDegrade = source -> JSON.parseObject(source,
// // new TypeReference<List<DegradeRule>>() {
// // });
// // ReadableDataSource<String, List<DegradeRule>> redisDataSourceDegrade = new
// // RedisDataSource<>(config, RULE_DEGRADE + SentinelConfig.getAppName(),
// // RULE_DEGRADE_CHANNEL, parserDegrade);
// // DegradeRuleManager.register2Property(redisDataSourceDegrade.getProperty());
//
// // Converter<String, List<SystemRule>> parserSystem = source -> JSON.parseObject(source, new
// // TypeReference<List<SystemRule>>() {
// // });
// // ReadableDataSource<String, List<SystemRule>> redisDataSourceSystem = new
// // RedisDataSource<>(config, RULE_SYSTEM + SentinelConfig.getAppName(), RULE_SYSTEM_CHANNEL,
// // parserSystem);
// // SystemRuleManager.register2Property(redisDataSourceSystem.getProperty());
// log.info(">>>>>>>>>执行sentinel规则初始化 end。。。");
// }
//
// }
