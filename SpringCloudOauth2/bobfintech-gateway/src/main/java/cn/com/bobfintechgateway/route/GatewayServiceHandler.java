package cn.com.bobfintechgateway.route;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cloud.gateway.event.RefreshRoutesEvent;
import org.springframework.cloud.gateway.filter.FilterDefinition;
import org.springframework.cloud.gateway.handler.predicate.PredicateDefinition;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.alibaba.fastjson.JSON;

import cn.com.bobfintechgateway.dao.GatewayRouteInfoMapper;
import cn.com.bobfintechgateway.pojo.GatewayRouteInfo;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/****
 * @project: 北银金科
 * @description: 项目初始化加载数据库的路由配置到redis
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-14 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Slf4j
@Service
public class GatewayServiceHandler implements ApplicationEventPublisherAware, CommandLineRunner
{
    @Autowired
    private RedisRouteDefinitionRepository routeDefinitionWriter;
    private ApplicationEventPublisher      publisher;
    
    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher)
    {
        this.publisher = applicationEventPublisher;
    }
    
    @Autowired
    private RedisTemplate redisTemplate;
    
    // 自己的获取数据dao
    @Autowired
    private GatewayRouteInfoMapper gatewayRouteInfoMapper;
    
    @Override
    public void run(String... args)
    {
        this.loadRouteConfig();
    }
    
    @SuppressWarnings("unchecked")
    public String loadRouteConfig()
    {
        log.info("====开始加载=====网关配置信息=========");
        // 删除redis里面的路由配置信息
        redisTemplate.delete(RedisRouteDefinitionRepository.GATEWAY_ROUTES);
        
        // 从数据库拿到基本路由配置
        List<GatewayRouteInfo> gatewayRouteList = gatewayRouteInfoMapper.queryAllRoutes();
        gatewayRouteList.forEach(gatewayRoute -> {
            RouteDefinition definition = handleData(gatewayRoute);
            routeDefinitionWriter.save(Mono.just(definition)).subscribe();
        });
        
        this.publisher.publishEvent(new RefreshRoutesEvent(this));
        
        log.info("=======网关配置信息===加载完成======");
        return "success";
    }
    
    /**
     * 查询所有已经加载的路由
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<GatewayRouteInfo> queryAllRoutes()
    {
        List<GatewayRouteInfo> gatewayRouteInfos = new ArrayList<GatewayRouteInfo>();
        redisTemplate.opsForHash().values(RedisRouteDefinitionRepository.GATEWAY_ROUTES).stream()
            .forEach(routeDefinition -> {
                RouteDefinition definition = JSON.parseObject(routeDefinition.toString(), RouteDefinition.class);
                gatewayRouteInfos.add(convert2GatewayRouteInfo(definition));
            });
        return gatewayRouteInfos;
    }
    
    /**
     * 将redis中路由信息转换为返回给前端的路由信息
     *
     * @param routeDefinition
     *            redis中的路由
     * @return
     */
    private GatewayRouteInfo convert2GatewayRouteInfo(Object obj)
    {
        RouteDefinition routeDefinition = (RouteDefinition) obj;
        GatewayRouteInfo gatewayRouteInfo = new GatewayRouteInfo();
        gatewayRouteInfo.setUri(routeDefinition.getUri().toString());
        gatewayRouteInfo.setServiceId(routeDefinition.getId());
        List<PredicateDefinition> predicates = routeDefinition.getPredicates();
        // 只有一个
        if (CollectionUtils.isNotEmpty(predicates)) {
            String predicatesString = predicates.get(0).getArgs().get("pattern");
            gatewayRouteInfo.setPredicates(predicatesString);
        }
        List<FilterDefinition> filters = routeDefinition.getFilters();
        if (CollectionUtils.isNotEmpty(filters)) {
            String filterString = filters.get(0).getArgs().get("_genkey_0");
            gatewayRouteInfo.setFilters(filterString);
        }
        gatewayRouteInfo.setOrder(String.valueOf(routeDefinition.getOrder()));;
        return gatewayRouteInfo;
    }
    
    public void saveRoute(GatewayRouteInfo gatewayRouteInfo)
    {
        RouteDefinition definition = handleData(gatewayRouteInfo);
        routeDefinitionWriter.save(Mono.just(definition)).subscribe();
        this.publisher.publishEvent(new RefreshRoutesEvent(this));
    }
    
    public void update(GatewayRouteInfo gatewayRouteInfo)
    {
        RouteDefinition definition = handleData(gatewayRouteInfo);
        try {
            this.routeDefinitionWriter.delete(Mono.just(definition.getId()));
            routeDefinitionWriter.save(Mono.just(definition)).subscribe();
            this.publisher.publishEvent(new RefreshRoutesEvent(this));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void deleteRoute(String routeId)
    {
        routeDefinitionWriter.delete(Mono.just(routeId)).subscribe();
        this.publisher.publishEvent(new RefreshRoutesEvent(this));
    }
    
    /**
     * 路由数据转换公共方法
     *
     * @param gatewayRoute
     * @return
     */
    private RouteDefinition handleData(GatewayRouteInfo gatewayRouteInfo)
    {
        RouteDefinition definition = new RouteDefinition();
        Map<String, String> predicateParams = new HashMap<>(8);
        PredicateDefinition predicate = new PredicateDefinition();
        FilterDefinition filterDefinition = new FilterDefinition();
        Map<String, String> filterParams = new HashMap<>(8);
        
        URI uri = null;
        if (gatewayRouteInfo.getUri().startsWith("http")) {
            // http地址
            uri = UriComponentsBuilder.fromHttpUrl(gatewayRouteInfo.getUri()).build().toUri();
        }
        else {
            // 注册中心
            uri = UriComponentsBuilder.fromUriString("lb://" + gatewayRouteInfo.getUri()).build().toUri();
        }
        
        definition.setId(gatewayRouteInfo.getServiceId());
        // 名称是固定的，spring gateway会根据名称找对应的PredicateFactory
        predicate.setName("Path");
        predicateParams.put("pattern", gatewayRouteInfo.getPredicates());
        predicate.setArgs(predicateParams);
        
        // 名称是固定的, 路径去前缀
        filterDefinition.setName("StripPrefix");
        filterParams.put("_genkey_0", gatewayRouteInfo.getFilters().toString());
        filterDefinition.setArgs(filterParams);
        
        definition.setPredicates(Arrays.asList(predicate));
        definition.setFilters(Arrays.asList(filterDefinition));
        definition.setUri(uri);
        definition.setOrder(Integer.parseInt(gatewayRouteInfo.getOrder()));
        
        return definition;
    }
}
