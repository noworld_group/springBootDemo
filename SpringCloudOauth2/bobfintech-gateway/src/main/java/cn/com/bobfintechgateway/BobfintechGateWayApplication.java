package cn.com.bobfintechgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @project: 北银金科
 * @description: 网关启动类。
 * ////////////////////////////////////////////////////////////////////
 * //                          _ooOoo_
 * //                         o8888888o
 * //                         88" . "88
 * //                         (| ^_^ |)
 * //                         O\  =  /O
 * //                      ____/`---'\____
 * //                    .'  \\|     |//  `.
 * //                   /  \\|||  :  |||//  \
 * //                  /  _||||| -:- |||||-  \
 * //                  |   | \\\  -  /// |   |
 * //                  | \_|  ''\---/''  |   |
 * //                  \  .-\__  `-`  ___/-. /
 * //                ___`. .'  /--.--\  `. . ___
 * //              ."" '<  `.___\_<|>_/___.'  >'"".
 * //            | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 * //            \  \ `-.   \_ __\ /__ _/   .-` /  /
 * //      ========`-.____`-.___\_____/___.-`____.-'========
 * //                           `=---='
 * //      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * //         佛祖保佑       永无BUG     永不修改
 * ////////////////////////////////////////////////////////////////////
 *
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-15 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients // 这是新增的注解 表示开启feign
// @ComponentScan(basePackages = { "cn.com.bobfintech" })
public class BobfintechGateWayApplication {

    public static void main(String[] args) {
        SpringApplication.run(BobfintechGateWayApplication.class, args);
    }
    
    // @Bean
    // public RouteLocator customRouteLocator(RouteLocatorBuilder builder)
    // {
    // return builder.routes().route("path_route", r -> r.path("/get").uri(
    // "http://httpbin.org"))
    // .route("host_route", r -> r.host("*.myhost.org").uri("http://httpbin.org"))
    // .route("rewrite_route",
    // r -> r.host("*.rewrite.org").filters(f -> f.rewritePath("/foo/(?<segment>.*)",
    // "/${segment}"))
    // .uri(
    // "http://httpbin.org"))// 将在网关路由请求路径之前重写该请求路径
    // .route("hystrix_route",
    // r -> r.host("*.hystrix.org").filters(f -> f.hystrix(c -> c.setName("slowcmd")))
    // .uri("http://httpbin.org"))
    // .route("hystrix_fallback_route",
    // r -> r.host("*.hystrixfallback.org")
    // .filters(f -> f.hystrix(c ->
    // c.setName("slowcmd").setFallbackUri("forward:/hystrixfallback")))
    // .uri("http://httpbin.org"))
    // .route("limit_route",
    // r -> r.host("*.limited.org").and().path("/anything/**")
    // .filters(f -> f.requestRateLimiter(c -> c.setRateLimiter(redisRateLimiter())))
    // .uri("http://httpbin.org"))
    // .build();
    // }

}
