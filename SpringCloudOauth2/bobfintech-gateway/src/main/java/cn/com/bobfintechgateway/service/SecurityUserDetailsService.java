package cn.com.bobfintechgateway.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;

import cn.com.bobfintech.commons.entity.user.SysResource;
import cn.com.bobfintech.commons.entity.user.SysResourceExample;
import cn.com.bobfintech.commons.entity.user.SysRole;
import cn.com.bobfintech.commons.entity.user.SysRoleExample;
import cn.com.bobfintech.commons.entity.user.SysRoleResource;
import cn.com.bobfintech.commons.entity.user.SysRoleResourceExample;
import cn.com.bobfintech.commons.entity.user.SysUser;
import cn.com.bobfintech.commons.entity.user.SysUserExample;
import cn.com.bobfintech.commons.entity.user.SysUserRole;
import cn.com.bobfintech.commons.entity.user.SysUserRoleExample;
import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.pojo.BaseResponse;
import cn.com.bobfintech.commons.pojo.ResponseData;
import cn.com.bobfintech.commons.utils.StringUtils;
import cn.com.bobfintechgateway.dao.SysResourceMapper;
import cn.com.bobfintechgateway.dao.SysRoleMapper;
import cn.com.bobfintechgateway.dao.SysRoleResourceMapper;
import cn.com.bobfintechgateway.dao.SysUserMapper;
import cn.com.bobfintechgateway.dao.SysUserRoleMapper;
import cn.com.bobfintechgateway.pojo.UserInfo;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * @project: 北银金科
 * @description: 定义用户查找逻辑
 *               security 的认证和授权都离不开系统中的用户，实际用户都来自db，本例中采用的是系统配置的默认用户。
 *               UserDetailsRepositoryReactiveAuthenticationManager作为security的核心认证管理器，并调用userDetailsService去查找用户，本集成环境中自定义用户查找逻辑需实现ReactiveUserDetailsService接口并覆盖findByUsername（通过用户名查找用户）方法，核心代码如下
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-06 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Component
@Slf4j
public class SecurityUserDetailsService implements ReactiveUserDetailsService
{
    @Autowired
    private SysUserMapper sysUserMapper;
    
    @Autowired
    private SysRoleMapper sysRoleMapper;
    
    @Autowired
    private SysRoleResourceMapper sysRoleResourceMapper;
    
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    
    @Autowired
    private SysResourceMapper sysResourceMapper;
    
    public Mono<UserDetails> findByUsername(String username)
    {
        log.info("begin  query user data >>>>>>>>>>>>>>>>");
        if (StringUtils.isBlank(username)) {
            BaseResponse baseResponse = ResponseData
                .out(BobfintechErrorNoEnum.COM_BOBFINTECH_SIGN_IN_USER_NAME_CAN_NOT_BE_NULL);
            String json = JSON.toJSONString(baseResponse);
            return Mono.error(new UsernameNotFoundException(json.toString()));
        }
        UserDetails userDetail = queryUserByUserName(username);
        if (userDetail == null) {
            BaseResponse baseResponse = ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_SIGN_IN_USER_NOT_EXIST);
            String json = JSON.toJSONString(baseResponse);
            return Mono.error(new UsernameNotFoundException(json.toString()));
        }
        return Mono.just(userDetail);
    }
    
    /**
     * 根据用户名查询用户的详细信息
     *
     * @param username
     *            用户名
     * @return
     */
    public UserDetails queryUserByUserName(String username) {
        SysUserExample sysUserExample = new SysUserExample();
        sysUserExample.createCriteria().andUserNameEqualTo(username);
        
        List<SysUser> sysUsers = sysUserMapper.selectByExample(sysUserExample);
        if (CollectionUtils.isEmpty(sysUsers)) {
            return null;
        }
        SysUser sysUser = sysUsers.get(0);
        log.info("查询的用户名：【{}】，用户密码：【{}】", sysUser.getUserName(), sysUser.getPassword());
        
        // UserDetails user = User.withUsername(username).password(MD5Encoder.encode(userInfo
        // .getUserPassword(),
        // username))
        // .authorities(AuthorityUtils.commaSeparatedStringToAuthorityList("admin")).build();
        // return Mono.just(user);
        UserInfo userDetail = new UserInfo();
        userDetail.setUsername(sysUser.getUserName());
        userDetail.setPassword(sysUser.getPassword());
        
        SysUserRoleExample sysUserRoleExample = new SysUserRoleExample();
        sysUserRoleExample.createCriteria().andUserIdEqualTo(sysUser.getId());
        List<SysUserRole> sysUserRoles = sysUserRoleMapper.selectByExample(sysUserRoleExample);
        if(CollectionUtils.isEmpty(sysUserRoles)) {
            //TODO,角色为空如何处理
            log.error("用户:{}的角色为空，如何处理？",sysUser.getId());
        }
        List<String> roleIds = sysUserRoles.stream().map(SysUserRole::getRoleId).collect(Collectors.toList());
        
        SysRoleExample sysRoleExample = new SysRoleExample();
        sysRoleExample.createCriteria().andIdIn(roleIds);
        List<SysRole> sysRoles = sysRoleMapper.selectByExample(sysRoleExample);
        userDetail.setRoleInfos(sysRoles);
        
        SysRoleResourceExample sysRoleResourceExample = new SysRoleResourceExample();
        sysRoleResourceExample.createCriteria().andRoleIdIn(roleIds);
        sysRoleResourceExample.setDistinct(true);
        List<SysRoleResource> sysRoleResources = sysRoleResourceMapper.selectByExample(sysRoleResourceExample);
        if (CollectionUtils.isEmpty(sysRoleResources)) {
            log.info("用户:{}的资源权限为空", sysUser.getId());
            // TODO
            userDetail.setWebAuthes(null);
        }
        List<String> resourceIds = sysRoleResources.stream()
            .map(SysRoleResource::getResourceId)
            .collect(Collectors.toList());
        SysResourceExample sysResourceExample = new SysResourceExample();
        sysResourceExample.createCriteria().andIdIn(resourceIds);
        sysResourceExample.setDistinct(true);
        List<SysResource> sysResources = sysResourceMapper.selectByExample(sysResourceExample);
        userDetail.setWebAuthes(sysResources);
        return userDetail;
        
        // SysUserInfo userInfo = userInfoDao.findByUsername(username);
        // if (userInfo == null) {
        // return null;
        // }
        
        // return User.withUsername(username).password(
        // userInfo.getPassword())
        // .authorities(AuthorityUtils.commaSeparatedStringToAuthorityList("admin")).build();
    }
    
}
