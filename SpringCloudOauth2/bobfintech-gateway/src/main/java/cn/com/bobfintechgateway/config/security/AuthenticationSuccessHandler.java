package cn.com.bobfintechgateway.config.security;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.WebFilterExchange;
import org.springframework.security.web.server.authentication.WebFilterChainServerAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import com.fasterxml.jackson.databind.ObjectMapper;

import cn.com.bobfintech.commons.constant.BobfintechContant;
import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.pojo.BaseResponse;
import cn.com.bobfintech.commons.pojo.ResponseData;
import cn.com.bobfintechgateway.component.MyRedisComponent;
import cn.com.bobfintechgateway.pojo.AuthUserDetails;
import cn.com.bobfintechgateway.pojo.Token;
import cn.com.bobfintechgateway.pojo.UserInfo;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * @project: 北银金科
 * @description: 登陆成功后执行的处理器,认证成功后返回给客户端的信息
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-06 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Component
@Slf4j
public class AuthenticationSuccessHandler extends WebFilterChainServerAuthenticationSuccessHandler
{
    @Autowired
    private MyRedisComponent redisUtil;
    
    @Override
    public Mono<Void> onAuthenticationSuccess(WebFilterExchange webFilterExchange, Authentication authentication)
    {
        ServerWebExchange exchange = webFilterExchange.getExchange();
        ServerHttpResponse response = exchange.getResponse();
        // 设置headers
        HttpHeaders httpHeaders = response.getHeaders();
        httpHeaders.add("Content-Type", "application/json; charset=UTF-8");
        httpHeaders.add("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
        byte[] dataBytes = {};
        ObjectMapper mapper = new ObjectMapper();
        try {
            UserInfo user = (UserInfo) authentication.getPrincipal();
            AuthUserDetails userDetails = new AuthUserDetails();
            userDetails.setUsername(user.getUsername());
            // userDetails.setPassword(
            // user.getPassword().substring(user.getPassword().lastIndexOf("}") + 1,
            // user.getPassword().length()));
            userDetails.setPassword(user.getPassword());
            String token = UUID.randomUUID().toString();
            user.setToken(token);
            user.setLoginTime(System.currentTimeMillis());
            user.setExpireTime(user.getLoginTime() + BobfintechContant.USER_TOKEN_TIME * 1000);
            // 缓存
            redisUtil.set(BobfintechContant.TOKEN_REDIS_KEY + user.getToken(), user, BobfintechContant.USER_TOKEN_TIME);
            // log.info("存入redis中的token为:" + redisUtil.get(BobfintechContant.TOKEN_REDIS_KEY +
            // user.getToken()));
            Token tokenRes = new Token(token, user.getLoginTime());
            ResponseData<Token> resultMap = new ResponseData<Token>(BobfintechErrorNoEnum.COM_BOBFINTECH_SUCCESS,
                tokenRes);
            dataBytes = mapper.writeValueAsBytes(resultMap);
        }
        catch (Exception ex) {
            log.error("网关认证成功后返回给客户端信息时处理失败:[{}]", ex.getMessage(), ex);
            BaseResponse responseData = ResponseData
                .out(BobfintechErrorNoEnum.COM_BOBFINTECH_SERVICE_AUTHORIZE_ERROR);
            dataBytes = responseData.toString().getBytes();
        }
        DataBuffer bodyDataBuffer = response.bufferFactory().wrap(dataBytes);
        return response.writeWith(Mono.just(bodyDataBuffer));
    }
    
}
