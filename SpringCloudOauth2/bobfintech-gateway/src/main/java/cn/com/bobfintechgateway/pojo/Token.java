package cn.com.bobfintechgateway.pojo;

import java.io.Serializable;

import lombok.Data;

@Data
public class Token implements Serializable {


    /**
     * 
     */
    private static final long serialVersionUID = 7410231617308949168L;
    private String            token;
	/** 登陆时间戳（毫秒） */
	private Long loginTime;

	public Token(String token, Long loginTime) {
		super();
		this.token = token;
		this.loginTime = loginTime;
	}

}