package cn.com.bobfintechgateway.filter;
 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import cn.com.bobfintechgateway.component.MyRedisComponent;
import cn.com.bobfintechgateway.service.SecurityUserDetailsService;
import reactor.core.publisher.Mono;
 
/**
 * @ClassName TokenFilter
 * @Desc TODO   请求认证过滤器
 * @Version 1.0
 */
@Configuration
public class GlobalCustomerFilter
{
    
    private Logger log = LoggerFactory.getLogger(GlobalCustomerFilter.class);
    
    @Autowired
    private MyRedisComponent redisUtil;
    
    @Autowired
    private SecurityUserDetailsService securityUserDetailsService;
    
    @Bean
    @Order(-1)
    public GlobalFilter firstFilter()
    {
        return (exchange, chain) -> {
            log.info("开始第二个filter >>>>>>>>>>>>>>>>>>>>>>>>");
            return chain.filter(exchange);
        };
    }
    
    @Bean
    @Order(0)
    public GlobalFilter secondFilter()
    {
        return (exchange, chain) -> {
            log.info("开始第三个filter >>>>>>>>>>>>>>>>>>>>>>>>");
            
            return chain.filter(exchange).then(Mono.fromRunnable(() -> {
                log.info("second pass filter");
            }));
        };
    }
 
}