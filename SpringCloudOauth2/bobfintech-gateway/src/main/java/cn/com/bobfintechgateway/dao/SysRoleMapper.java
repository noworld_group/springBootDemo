package cn.com.bobfintechgateway.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.bobfintech.commons.entity.user.SysRole;
import cn.com.bobfintech.commons.entity.user.SysRoleExample;

/**
 * @project:用户管理模块
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 825338623@qq.com Create 1.0
 * @copyright ©2017-2019
 */
@Mapper
public interface SysRoleMapper {
    
    /**
     * @param example example
     * @return int
     */
    int countByExample(SysRoleExample example);

    
    /**
     * @param example example
     * @return int
     */
    int deleteByExample(SysRoleExample example);

    
    /**
     * @param id id
     * @return int
     */
    int deleteByPrimaryKey(String id);

    
    /**
     * @param record record
     * @return int
     */
    int insert(SysRole record);

    
    /**
     * @param record record
     * @return int
     */
    int insertSelective(SysRole record);

    
    /**
     * @param example example
     * @return List<SysRole>
     */
    List<SysRole> selectByExample(SysRoleExample example);

    
    /**
     * @param id id
     * @return SysRole
     */
    SysRole selectByPrimaryKey(String id);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExampleSelective(@Param("record") SysRole record, @Param("example") SysRoleExample example);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExample(@Param("record") SysRole record, @Param("example") SysRoleExample example);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKeySelective(SysRole record);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKey(SysRole record);
}
/**
 * CHANGE HISTORY
* M1 2019-01-23 825338623@qq.com Create
*/