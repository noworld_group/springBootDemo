package cn.com.bobfintechgateway.controller;
 
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.pojo.BaseResponse;
import cn.com.bobfintech.commons.pojo.ResponseData;
import cn.com.bobfintech.commons.utils.IdUtils;
 
/**
 * 网关断路器
 * 
 * @author 郭鹏飞
 *
 */
@RestController
public class FallbackController {
 
    /*
     * @ClassName FallbackController
     * @Desc TODO   网关断路器
     * @Version 1.0
     */
    @RequestMapping("/fallback")
    public String fallback() {
        BaseResponse responseData = ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_SERVICE_CAN_NOT_READ);
        String idString = IdUtils.getUuid();
        System.out.println("查询到的Id为：" + idString);
        return responseData.toString();
    }
 
}