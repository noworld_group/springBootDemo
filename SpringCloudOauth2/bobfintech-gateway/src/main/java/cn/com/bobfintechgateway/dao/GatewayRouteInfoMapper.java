package cn.com.bobfintechgateway.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import cn.com.bobfintechgateway.pojo.GatewayRouteInfo;

/**
 * @project: 北银金科
 * @description: 路由Dao
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-14 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Mapper
public interface GatewayRouteInfoMapper
{
    List<GatewayRouteInfo> queryAllRoutes();
    
    
}
