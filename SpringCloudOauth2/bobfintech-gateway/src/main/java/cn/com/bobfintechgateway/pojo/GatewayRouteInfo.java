package cn.com.bobfintechgateway.pojo;

import java.util.Date;

import lombok.Data;

/**
 * @project: 北银金科
 * @description: 路由信息类
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-14 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Data
public class GatewayRouteInfo
{
    private Long id;
    
    private String serviceId;
 
    private String uri;
 
    private String predicates;
 
    private String filters;
 
    private String order;
 
    private Date createDate;
 
    private Date updateDate;
 
    private String remarks;
 
    private String delFlag;

}
