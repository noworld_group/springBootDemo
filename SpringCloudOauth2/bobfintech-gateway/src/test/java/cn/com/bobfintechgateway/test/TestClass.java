package cn.com.bobfintechgateway.test;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class TestClass
{
    public static void main(String[] args)
    {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String password = passwordEncoder.encode("admin");
        System.out.println(password);
    }
}
