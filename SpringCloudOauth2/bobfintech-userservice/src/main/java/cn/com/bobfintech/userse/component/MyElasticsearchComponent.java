package cn.com.bobfintech.userse.component;

import org.springframework.stereotype.Component;

import cn.com.bobfintech.commons.component.ElasticsearchComponent;

/**
 * @project: 北银金科
 * @description: ES组件继承类
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-13 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Component
public class MyElasticsearchComponent extends ElasticsearchComponent
{
    
}
