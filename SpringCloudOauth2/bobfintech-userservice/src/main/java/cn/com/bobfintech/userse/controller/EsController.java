package cn.com.bobfintech.userse.controller;

import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.client.core.GetSourceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.com.bobfintech.commons.enums.BobfintechErrorNoEnum;
import cn.com.bobfintech.commons.pojo.BaseResponse;
import cn.com.bobfintech.commons.pojo.ResponseData;
import cn.com.bobfintech.userse.component.MyElasticsearchComponent;
import cn.com.bobfintech.userse.config.MyEsConfig;
import cn.com.bobfintech.userse.entify.UserInfo;

/**
 * @project: 北银金科
 * @description: ES测试controller
 *               类功能简介。
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-02 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@RestController
public class EsController
{
    @Autowired
    private MyElasticsearchComponent esUtils;
    
    @Autowired
    private MyEsConfig myEsConfig;
    
    @SuppressWarnings("rawtypes")
    @RequestMapping("/testAddEs")
    @ResponseBody
    public BaseResponse testEs(@RequestParam String id, @RequestParam String userName,
                               @RequestParam String createDate,
                         @RequestParam String introduction, @RequestParam String addr)
    {
        try {
            UserInfo userInfo = new UserInfo();
            userInfo.setId(Integer.parseInt(id));
            userInfo.setUserName(userName);
            userInfo.setCreateDate(createDate);
            userInfo.setIntroduction(introduction);
            userInfo.setAddr(addr);
            esUtils.addData(myEsConfig, "testuserinfo", userInfo, id);
            System.out.println("向ES添加数据成功！");
            ResponseData out = ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_SUCCESS, "向ES添加数据成功！");
            return out;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_ES_FAIL);
        
    }
    
    @SuppressWarnings("rawtypes")
    @RequestMapping("/testIsExistEs4Syc")
    @ResponseBody
    public BaseResponse testIsExistEs4Syc(@RequestParam String id, @RequestParam String index)
    {
        try {
            boolean isExist = esUtils.existDoc4Sync(myEsConfig, id, index);
            ResponseData out = ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_SUCCESS, isExist ? "存在" : "不存在");
            return out;
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_ES_FAIL);
    }
    
    @SuppressWarnings("rawtypes")
    @RequestMapping("/testIsExistEs4Asyc")
    @ResponseBody
    public BaseResponse testIsExistEs4Asyc(@RequestParam String id, @RequestParam String index)
    {
        try {
            boolean isExist = esUtils.existDoc4Async(myEsConfig, id, index);
            ResponseData out = ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_SUCCESS, "请求成功，等待异步反馈");
            return out;
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_ES_FAIL);
    }
    
    @SuppressWarnings("rawtypes")
    @RequestMapping("/testgetSource4Syc")
    @ResponseBody
    public BaseResponse testgetSource4Syc(@RequestParam String id, @RequestParam String index)
    {
        try {
            GetSourceResponse response = esUtils.getEsSource4Sync(myEsConfig, index, id);
            ResponseData out = ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_SUCCESS, response.getSource());
            return out;
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_ES_FAIL);
    }
    
    @RequestMapping("/testGetAllIndexData")
    @ResponseBody
    public BaseResponse testGetAllIndexData(@RequestParam String index, @RequestParam Integer from,
                                            @RequestParam Integer size)
    {
        try {
            Map<String, Object> allIndexData = esUtils.getAllIndexData(myEsConfig, index, from, size);
            ResponseData out = ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_SUCCESS,
                allIndexData);
            return out;
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_ES_FAIL);
    }
    
    @RequestMapping("/testOneParamQueryByPage4Syc")
    @ResponseBody
    public BaseResponse testSearchByPage(
                                         @RequestParam String index, @RequestParam Integer from,
                                         @RequestParam Integer size, String fieldKey, String fieldVal, String sortField)
    {
        String[] indices = new String[] { index };
        try {
            Map<String, Object> allIndexData = esUtils.testQueryByPage(myEsConfig,
                indices,
                sortField,
                fieldKey,
                fieldVal, from, size);
            ResponseData out = ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_SUCCESS,
                allIndexData);
            return out;
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ResponseData.out(BobfintechErrorNoEnum.COM_BOBFINTECH_ES_FAIL);
    }
    
    @RequestMapping("/testManyParamQueryByPage4Syc")
    @ResponseBody
    public Map<String, Object> testManyParamQueryByPage4Syc(@RequestParam String id, @RequestParam String index)
    {
        try {
            GetSourceResponse response = esUtils.getEsSource4Sync(myEsConfig, index, id);
            return response.getSource();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("result", "查询失败");
        return map;
    }
}
