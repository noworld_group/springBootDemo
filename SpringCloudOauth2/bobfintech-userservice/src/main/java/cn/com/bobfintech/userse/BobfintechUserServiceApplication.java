package cn.com.bobfintech.userse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @project: 北银金科
 * @description: 中文类名。
 *               类功能简介。
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-02 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@SpringBootApplication
@EnableEurekaClient
// @ComponentScan(basePackages = {"cn.com.bobfintech"})
public class BobfintechUserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BobfintechUserServiceApplication.class, args);
    }

}
