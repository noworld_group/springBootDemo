package cn.com.bobfintech.userse.entify;

import java.io.Serializable;

import lombok.Data;

/**
 * @project: 北银金科
 * @description: 测试用户信息类
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-13 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Data
public class UserInfo implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 483679755152904667L;
    
    private Integer id;
    
    private String userName;
    
    private String province;
    
    private String addr;

    private String introduction;
    
    private String createDate;
}
