package cn.com.bobfintech.userse.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import cn.com.bobfintech.commons.config.ElasticsearchConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @project: 北银金科
 * @description: 自定义es配置
 *               类功能简介。
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-03 guopengfei@bobfintech.com.cn Create 1.0
 * @copyright ©2019-2020 北银金科，版权所有。
 */
@Configuration
@Data
@EqualsAndHashCode(callSuper = false)
public class MyEsConfig extends ElasticsearchConfig
{
    @Value("${spring.elasticsearch.rest.uris}")
    private String[] esUris;
    
    @Value("${spring.elasticsearch.rest.timeout:2000000}")
    private int timeout;
    
    @Value("${spring.elasticsearch.rest.username}")
    private String userName;
    
    @Value("${spring.elasticsearch.rest.password}")
    private String password;

}
