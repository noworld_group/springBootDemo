package cn.com.bobfintech.userse.config;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.Observable;
import java.util.Observer;

/**
 * @ClassName: test1
 * @Description: aaa
 * Author:  Jiaxi
 * Date: 2020/7/7 16:37
 */
public class TestTask extends QuartzJobBean {
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("执行test task "+System.currentTimeMillis());
    }
}