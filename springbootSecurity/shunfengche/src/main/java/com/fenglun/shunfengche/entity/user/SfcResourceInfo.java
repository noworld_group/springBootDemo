package com.fenglun.shunfengche.entity.user;

import java.util.Date;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
public class SfcResourceInfo {

    
    /**编号 */
    private String id;
    
    /**资源名称 */
    private String resourceName;
    
    /**资源图标 */
    private String resourceIcon;
    
    /**资源类型 */
    private String resourceType;
    
    /**排序 */
    private Integer orderNum;
    
    /**权限编码 */
    private String permCode;
    
    /**父级编号，顶级为-1 */
    private String parentId;
    
    /**资源级别 */
    private Integer resourceLevel;
    
    /**资源链接 */
    private String resourceUrl;
    
    /**资源状态,0:停用，1:启用 */
    private String resourceStatus;
    
    /**创建时间 */
    private Date createDatetime;
    
    /** */
    private Date updateDatetime;

    
    /**
     * @return 编号
     */
    public String getId() {
        return id;
    }
    
    /**
     * @param id 编号
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }
    
    /**
     * @return 资源名称
     */
    public String getResourceName() {
        return resourceName;
    }
    
    /**
     * @param resourceName 资源名称
     */
    public void setResourceName(String resourceName) {
        this.resourceName = resourceName == null ? null : resourceName.trim();
    }
    
    /**
     * @return 资源图标
     */
    public String getResourceIcon() {
        return resourceIcon;
    }
    
    /**
     * @param resourceIcon 资源图标
     */
    public void setResourceIcon(String resourceIcon) {
        this.resourceIcon = resourceIcon == null ? null : resourceIcon.trim();
    }
    
    /**
     * @return 资源类型
     */
    public String getResourceType() {
        return resourceType;
    }
    
    /**
     * @param resourceType 资源类型
     */
    public void setResourceType(String resourceType) {
        this.resourceType = resourceType == null ? null : resourceType.trim();
    }
    
    /**
     * @return 排序
     */
    public Integer getOrderNum() {
        return orderNum;
    }
    
    /**
     * @param orderNum 排序
     */
    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }
    
    /**
     * @return 权限编码
     */
    public String getPermCode() {
        return permCode;
    }
    
    /**
     * @param permCode 权限编码
     */
    public void setPermCode(String permCode) {
        this.permCode = permCode == null ? null : permCode.trim();
    }
    
    /**
     * @return 父级编号，顶级为-1
     */
    public String getParentId() {
        return parentId;
    }
    
    /**
     * @param parentId 父级编号，顶级为-1
     */
    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }
    
    /**
     * @return 资源级别
     */
    public Integer getResourceLevel() {
        return resourceLevel;
    }
    
    /**
     * @param resourceLevel 资源级别
     */
    public void setResourceLevel(Integer resourceLevel) {
        this.resourceLevel = resourceLevel;
    }
    
    /**
     * @return 资源链接
     */
    public String getResourceUrl() {
        return resourceUrl;
    }
    
    /**
     * @param resourceUrl 资源链接
     */
    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl == null ? null : resourceUrl.trim();
    }
    
    /**
     * @return 资源状态,0:停用，1:启用
     */
    public String getResourceStatus() {
        return resourceStatus;
    }
    
    /**
     * @param resourceStatus 资源状态,0:停用，1:启用
     */
    public void setResourceStatus(String resourceStatus) {
        this.resourceStatus = resourceStatus == null ? null : resourceStatus.trim();
    }
    
    /**
     * @return 创建时间
     */
    public Date getCreateDatetime() {
        return createDatetime == null ? null : (Date) createDatetime.clone();
    }
    
    /**
     * @param createDatetime 创建时间
     */
    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime == null ? null : (Date) createDatetime.clone();
    }
    
    /**
     * @return 
     */
    public Date getUpdateDatetime() {
        return updateDatetime == null ? null : (Date) updateDatetime.clone();
    }
    
    /**
     * @param updateDatetime 
     */
    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime == null ? null : (Date) updateDatetime.clone();
    }
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/