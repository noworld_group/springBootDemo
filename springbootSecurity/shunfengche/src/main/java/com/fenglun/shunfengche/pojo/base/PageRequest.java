package com.fenglun.shunfengche.pojo.base;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @project: 顺风车
 * @description: 分页请求 可以用作父类，子类继承后增加自定义属性
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-08-06 825338623@qq.com Create 1.0
 * @copyright ©2019-2020 顺风车，版权所有。
 */
@Data
@ApiModel(description = "分页查询请求类")
public class PageRequest implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2306405212144003406L;

	@ApiModelProperty("当前页码")
    @NotNull(message = "当前页码不能为空")
    private Integer pageNum;
    
    /**
     * 每页数量
     */
    @ApiModelProperty("每页条数")
    @NotNull(message = "每页条数不能为空")
    private Integer pageSize;
    
}
