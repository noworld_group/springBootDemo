package com.fenglun.shunfengche.pojo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Project: 便民(BeBianMin-BaJson)系统
 * @Description: 中文类名。
 * @Version 1.0.0
 * @Author
 *         <li>2021-01-18 guopengfeiheze@blog-china.cn Create 1.0
 * @Copyright ©2017-2021 BeBianMin（www.oipinche.com），版权所有。
 */
@Data
@ApiModel("角色管理-单个权限信息model")
public class RoleMgResourceResVo
{
    
    /**
     * 资源编号
     */
    @ApiModelProperty("资源编号")
    private String resourceId;
    
    /**
     * 资源名称
     */
    @ApiModelProperty("资源名称")
    private String resourceName;
    
    /**
     * 资源类型 1:菜单;2:数据;3:功能
     */
    @ApiModelProperty("资源类型")
    private String resourceType;
    
    /**
     * 权限编码
     */
    @ApiModelProperty("权限编码")
    private String permCode;
    
    /**
     * 资源级别
     */
    @ApiModelProperty("资源级别")
    private String resourceLevel;
}
