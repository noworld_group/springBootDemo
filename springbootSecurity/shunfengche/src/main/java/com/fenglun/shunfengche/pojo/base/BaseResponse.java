package com.fenglun.shunfengche.pojo.base;

import java.io.Serializable;

import com.fenglun.shunfengche.enums.ShunFengCheErrorEnums;

/**
 * 
 * @project: 顺风车
 * @description:响应数据基类
 * @version 1.0.0
 * @errorcode 错误码: 错误描述
 *
 * @author
 *         <li>2021-01-18 825338623@qq.com Create 1.0
 * 
 * @copyright ©2019-2021 顺风车，版权所有。
 */
public class BaseResponse implements Serializable {
	@Override
	public String toString() {
        return "{'code':'" + code + "','msg':'" + msg + "'}";
	}
	/**
     *
     */
    private static final long serialVersionUID = -5544502424932175589L;

    /**
     * 状态码
     */
	private String code;

	/**
	 * 响应消息
	 */
	private String msg;

	protected BaseResponse() {
	}

    protected BaseResponse(ShunFengCheErrorEnums code)
    {
        this.code = code.getErrorNo();
        this.msg = code.getErrorConsonleInfo();
	}

    protected BaseResponse(String code, String msg)
    {
        this.code = code;
        this.msg = msg;
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}


}
