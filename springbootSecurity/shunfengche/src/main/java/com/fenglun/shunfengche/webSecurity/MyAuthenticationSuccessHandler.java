package com.fenglun.shunfengche.webSecurity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fenglun.shunfengche.component.RedisComponent;
import com.fenglun.shunfengche.constant.ShunFengCheConstant;
import com.fenglun.shunfengche.entity.user.SfcResourceInfo;
import com.fenglun.shunfengche.entity.user.SfcRoleInfo;
import com.fenglun.shunfengche.entity.user.SfcUserInfo;
import com.fenglun.shunfengche.enums.ShunFengCheErrorEnums;
import com.fenglun.shunfengche.mapper.user.SfcResourceInfoExtendMapper;
import com.fenglun.shunfengche.mapper.user.SfcRoleInfoExtendMapper;
import com.fenglun.shunfengche.mapper.user.SfcUserInfoMapper;
import com.fenglun.shunfengche.pojo.base.BaseResponse;
import com.fenglun.shunfengche.pojo.base.ResponseData;
import com.fenglun.shunfengche.pojo.security.Token;
import com.fenglun.shunfengche.pojo.security.UserInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @project: 顺风车
 * @description: 登陆成功后执行的处理器, 认证成功后返回给客户端的信息
 * @version 1.0.0
 * @errorcode 错误码: 错误描述
 *
 * @author
 *         <li>2021-01-19 825338623@qq.com Create 1.0
 * 
 * @copyright ©2019-2021 顺风车，版权所有。
 */
@Component
@Slf4j
public class MyAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {


    @Autowired
    private ObjectMapper objectMapper;
	@Autowired
	private RedisComponent redisComponent;

	@Autowired
	private SfcUserInfoMapper sfcUserInfoMapper;

	@Autowired
	private SfcRoleInfoExtendMapper sfcRoleInfoExtendMapper;

	@Autowired
	private SfcResourceInfoExtendMapper sfcResourceInfoExtendMapper;

    // Authentication  封装认证信息
    @Override 
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws ServletException, IOException {
		log.info("登录成功后的处理逻辑 >>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		response.setContentType("application/json;charset=UTF-8");
		try {
			UserInfo userInfo = (UserInfo) authentication.getPrincipal();
			SfcUserInfo sfcUserInfo = sfcUserInfoMapper
					.selectByPrimaryKey(userInfo.getId());
			List<String> userStatus = Arrays.asList(
					ShunFengCheConstant.USER_STATUS_VIOLATION_DEACTIVATION,
					ShunFengCheConstant.USER_STATUS_RESIGNATION_DEACTIVATION);
			if (userStatus.contains(sfcUserInfo.getUserStatus())) {
				ResponseData<Token> resultMap = new ResponseData<Token>(
						ShunFengCheErrorEnums.COM_SUCCESS.getErrorNo(),
						ShunFengCheErrorEnums.COM_SIGN_IN_USER_HAS_STOP
								.getErrorConsonleInfo());
				response.getWriter()
						.write(objectMapper.writeValueAsString(resultMap));
			}
			sfcUserInfo.setLoginTime(new Date());
			sfcUserInfoMapper.updateByPrimaryKeySelective(sfcUserInfo);
			UserInfo userDetail = new UserInfo();
			userDetail.setUsername(sfcUserInfo.getUserName());
			userDetail.setPassword(sfcUserInfo.getUserPassword());
			userDetail.setId(sfcUserInfo.getId());

			List<SfcRoleInfo> SfcRoleInfos = sfcRoleInfoExtendMapper
					.getRoleInfoByUserId(sfcUserInfo.getId());
			userDetail.setRoleInfos(SfcRoleInfos);
			if (CollectionUtils.isNotEmpty(SfcRoleInfos)) {
				List<String> roleIds = SfcRoleInfos.stream()
						.map(SfcRoleInfo::getId).collect(Collectors.toList());
				List<SfcResourceInfo> SfcResourceInfos = sfcResourceInfoExtendMapper
						.getResourceInfoByRoleIds(roleIds);
				userDetail.setWebAuthes(SfcResourceInfos);
			}
			String token = UUID.randomUUID().toString();
			userDetail.setToken(token);
			userDetail.setLoginTime(System.currentTimeMillis());
			userDetail.setExpireTime(sfcUserInfo.getLoginTime().getTime()
					+ ShunFengCheConstant.MINUTES_5);
			List<SfcResourceInfo> webAuthes = userDetail.getWebAuthes();
			List<String> collect = new ArrayList<>();
			if (CollectionUtils.isNotEmpty(webAuthes)) {
				collect = webAuthes.stream().map(SfcResourceInfo::getPermCode)
						.distinct().collect(Collectors.toList());
				userDetail.setPermissions(collect);
			}
			// 缓存
			redisComponent.set(
					ShunFengCheConstant.TOKEN_REDIS_KEY + userDetail.getToken(),
					userDetail,
					ShunFengCheConstant.MINUTES_5);
			redisComponent.set(
					ShunFengCheConstant.TOKEN_REDIS_KEY_USER_ID
							+ userDetail.getToken(),
					userDetail.getId(), ShunFengCheConstant.MINUTES_5);
			log.info("存入redis中的token为:" + redisComponent
					.get(ShunFengCheConstant.TOKEN_REDIS_KEY
							+ userDetail.getToken()));
			Token tokenRes = new Token(token, collect,
					userDetail.getLoginTime());
			ResponseData<Token> resultMap = new ResponseData<Token>(
					ShunFengCheErrorEnums.COM_SUCCESS, tokenRes);
			response.getWriter()
					.write(objectMapper.writeValueAsString(resultMap));
		} catch (Exception ex) {
			log.error("认证成功后返回给客户端信息时处理失败:[{}]", ex.getMessage(), ex);
			BaseResponse responseData = ResponseData.out(
					ShunFengCheErrorEnums.COM_SYS_ERROR.getErrorNo(),
					ShunFengCheErrorEnums.COM_LOGIN_ERROR
							.getErrorConsonleInfo());
			response.getWriter()
					.write(objectMapper.writeValueAsString(responseData));
		}
    }

}
