package com.fenglun.shunfengche.enums;

/**
 * 
 * @project: 顺风车
 * @description: 错误编号枚举类。
 * @version 1.0.0
 * @errorcode 错误码: 错误描述
 *
 * @author
 *         <li>2021-01-18 825338623@qq.com Create 1.0
 * 
 * @copyright ©2019-2021 顺风车，版权所有。
 */
public enum ShunFengCheErrorEnums {
	/************************************通用类错误************************************/
    COM_DATABASE_ERROR("ERR-E-X-DB-BOB-0001", "数据库访问异常"), //
    COM_UNKNOWN_ERROR("ERR-E-Y-NA-BOB-0003", "未知异常"), //
	COM_ERROR("ERR-E-Y-NV-BOB-0015", "{0}"), //

    COM_SIGN_IN_USER_NAME_CAN_NOT_BE_NULL("ERR-E-Y-NV-BOB-0501", "用户名不能为空"),
    COM_SIGN_IN_USER_HAS_STOP("ERR-E-Y-NV-BOB-0502", "用户已停用！"),
	COM_SIGN_IN_USER_NAME_OR_PWD_NOT_RIGHT("ERR-E-Y-NV-BOB-0502","用户名或者密码不对"),
    COM_OAU_FAIL("ERR-E-Y-NV-BOB-0012", "鉴权失败，请重新登录"),
    COM_LOGIN_ERROR("ERR-E-Y-NV-BOB-0013", "登录异常，请联系管理员"),
	COM_SERVICE_AUTHORIZE_ERROR("ERR-E-Y-NV-BOB-0010", "服务授权异常"),
    COM_CAN_NOT_VISIT_URI("ERR-E-Y-NV-BOB-0013", "无此项访问权限！"),
	COM_USER_HAS_LOG_OUT("ERR-E-Y-NV-BOB-0013", "您已经退出登录！"),

	COM_SYS_ERROR("-222", "系统异常"), 
	COM_BUSINESS_ERROR("-1", "业务异常"), //
	COM_SUCCESS("00000", "操作成功！");

	/** 错误编码 */
	private String errorNo;
	/** 错误信息 */
	private String errorConsonleInfo;

	/**
     * 构造方法。
     *
     * @param errorNo
     *            错误编码
     * @param errorConsonleInfo
     *            错误信息
     */
	private ShunFengCheErrorEnums(String errorNo, String errorConsonleInfo)
    {
        this.errorNo = errorNo;
        this.errorConsonleInfo = errorConsonleInfo;
    }

	/**
	 * @return 错误编码
	 */
	public String getErrorNo() {
		return errorNo;
	}

	/**
	 * @return 错误信息
	 */
	public String getErrorConsonleInfo() {
		return errorConsonleInfo;
	}

	public static ShunFengCheErrorEnums get(String errorNo) {
		for (ShunFengCheErrorEnums responseCodeEnum : ShunFengCheErrorEnums
				.values()) {
			if (responseCodeEnum.getErrorNo().equals(errorNo)) {
				return responseCodeEnum;
			}
		}
		return null;
	}

}
