package com.fenglun.shunfengche.entity.user;

import java.util.Date;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
public class SfcUserInfo {

    
    /**编号 */
    private String id;
    
    /**用户名 */
    private String userName;
    
    /**密码 */
    private String userPassword;
    
    /**真实姓名，管理人员/机构可以不填 */
    private String realName;
    
    /**用户状态，1:正常，2:违规停用，3:离职停用 */
    private String userStatus;
    
    /**最近登录时间 */
    private Date loginTime;
    
    /**用户头像 */
    private String userLogo;
    
    /**用户电话 */
    private String userTel;
    
    /**身份证号 */
    private String cardIdNum;
    
    /**用户性别 */
    private String userGender;
    
    /**用户邮箱 */
    private String userEmail;
    
    /**描述信息，停用原因等信息都保存在此处 */
    private String remark;
    
    /** */
    private Date createDatetime;
    
    /** */
    private Date updateDatetime;

    
    /**
     * @return 编号
     */
    public String getId() {
        return id;
    }
    
    /**
     * @param id 编号
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }
    
    /**
     * @return 用户名
     */
    public String getUserName() {
        return userName;
    }
    
    /**
     * @param userName 用户名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }
    
    /**
     * @return 密码
     */
    public String getUserPassword() {
        return userPassword;
    }
    
    /**
     * @param userPassword 密码
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword == null ? null : userPassword.trim();
    }
    
    /**
     * @return 真实姓名，管理人员/机构可以不填
     */
    public String getRealName() {
        return realName;
    }
    
    /**
     * @param realName 真实姓名，管理人员/机构可以不填
     */
    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }
    
    /**
     * @return 用户状态，1:正常，2:违规停用，3:离职停用
     */
    public String getUserStatus() {
        return userStatus;
    }
    
    /**
     * @param userStatus 用户状态，1:正常，2:违规停用，3:离职停用
     */
    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus == null ? null : userStatus.trim();
    }
    
    /**
     * @return 最近登录时间
     */
    public Date getLoginTime() {
        return loginTime == null ? null : (Date) loginTime.clone();
    }
    
    /**
     * @param loginTime 最近登录时间
     */
    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime == null ? null : (Date) loginTime.clone();
    }
    
    /**
     * @return 用户头像
     */
    public String getUserLogo() {
        return userLogo;
    }
    
    /**
     * @param userLogo 用户头像
     */
    public void setUserLogo(String userLogo) {
        this.userLogo = userLogo == null ? null : userLogo.trim();
    }
    
    /**
     * @return 用户电话
     */
    public String getUserTel() {
        return userTel;
    }
    
    /**
     * @param userTel 用户电话
     */
    public void setUserTel(String userTel) {
        this.userTel = userTel == null ? null : userTel.trim();
    }
    
    /**
     * @return 身份证号
     */
    public String getCardIdNum() {
        return cardIdNum;
    }
    
    /**
     * @param cardIdNum 身份证号
     */
    public void setCardIdNum(String cardIdNum) {
        this.cardIdNum = cardIdNum == null ? null : cardIdNum.trim();
    }
    
    /**
     * @return 用户性别
     */
    public String getUserGender() {
        return userGender;
    }
    
    /**
     * @param userGender 用户性别
     */
    public void setUserGender(String userGender) {
        this.userGender = userGender == null ? null : userGender.trim();
    }
    
    /**
     * @return 用户邮箱
     */
    public String getUserEmail() {
        return userEmail;
    }
    
    /**
     * @param userEmail 用户邮箱
     */
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail == null ? null : userEmail.trim();
    }
    
    /**
     * @return 描述信息，停用原因等信息都保存在此处
     */
    public String getRemark() {
        return remark;
    }
    
    /**
     * @param remark 描述信息，停用原因等信息都保存在此处
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
    
    /**
     * @return 
     */
    public Date getCreateDatetime() {
        return createDatetime == null ? null : (Date) createDatetime.clone();
    }
    
    /**
     * @param createDatetime 
     */
    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime == null ? null : (Date) createDatetime.clone();
    }
    
    /**
     * @return 
     */
    public Date getUpdateDatetime() {
        return updateDatetime == null ? null : (Date) updateDatetime.clone();
    }
    
    /**
     * @param updateDatetime 
     */
    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime == null ? null : (Date) updateDatetime.clone();
    }
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/