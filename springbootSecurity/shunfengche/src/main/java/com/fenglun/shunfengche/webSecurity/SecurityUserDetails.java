package com.fenglun.shunfengche.webSecurity;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.fenglun.shunfengche.entity.user.SfcUserInfo;
import com.fenglun.shunfengche.entity.user.SfcUserInfoExample;
import com.fenglun.shunfengche.enums.ShunFengCheErrorEnums;
import com.fenglun.shunfengche.exception.BusinessException;
import com.fenglun.shunfengche.mapper.user.SfcUserInfoMapper;
import com.fenglun.shunfengche.pojo.base.BaseResponse;
import com.fenglun.shunfengche.pojo.base.ResponseData;
import com.fenglun.shunfengche.pojo.security.UserInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * @project: 顺风车
 * @description: 定义用户查找逻辑 security 的认证和授权都离不开系统中的用户，实际用户都来自db，本例中采用的是系统配置的默认用户。
 *               UserDetailsRepositoryReactiveAuthenticationManager作为security的核心认证管理器，并调用userDetailsService去查找用户，本集成环境中自定义用户查找逻辑需实现ReactiveUserDetailsService接口并覆盖findByUsername（通过用户名查找用户）方法，核心代码如下
 * @version 1.0.0
 * @errorcode 错误码: 错误描述
 * @author
 *         <li>2020-07-06 825338623@qq.com Create 1.0
 * @copyright ©2019-2020 顺风车，版权所有。
 */
@Component
@Slf4j
public class SecurityUserDetails implements UserDetailsService
{
    
    @Autowired
    private SfcUserInfoMapper sfcUserInfoMapper;
    
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        log.debug("security 查询用户信息 findByUserName , username:{}", username);
        if (StringUtils.isBlank(username)) {
            BaseResponse baseResponse = ResponseData.out(ShunFengCheErrorEnums.COM_SIGN_IN_USER_NAME_CAN_NOT_BE_NULL);
            String json = JSON.toJSONString(baseResponse);
            throw new UsernameNotFoundException(json);
        }
		SfcUserInfoExample SfcUserInfoExample = new SfcUserInfoExample();
		SfcUserInfoExample.createCriteria().andUserNameEqualTo(username);
		List<SfcUserInfo> SfcUserInfos = sfcUserInfoMapper
				.selectByExample(SfcUserInfoExample);
		if (CollectionUtils.isEmpty(SfcUserInfos)) {
			throw new BusinessException(
					ShunFengCheErrorEnums.COM_SIGN_IN_USER_NAME_CAN_NOT_BE_NULL);
        }
		SfcUserInfo sfcUserInfo = SfcUserInfos.get(0);
		UserInfo userDetail = new UserInfo();
		userDetail.setUsername(sfcUserInfo.getUserName());
		userDetail.setPassword(sfcUserInfo.getUserPassword());
		userDetail.setId(sfcUserInfo.getId());
        return userDetail;
    }
}
