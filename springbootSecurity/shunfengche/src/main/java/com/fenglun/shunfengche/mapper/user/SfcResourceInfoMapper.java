package com.fenglun.shunfengche.mapper.user;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.fenglun.shunfengche.entity.user.SfcResourceInfo;
import com.fenglun.shunfengche.entity.user.SfcResourceInfoExample;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
@Mapper
public interface SfcResourceInfoMapper {
    
    /**
     * @param example example
     * @return int
     */
    int countByExample(SfcResourceInfoExample example);

    
    /**
     * @param example example
     * @return int
     */
    int deleteByExample(SfcResourceInfoExample example);

    
    /**
     * @param id id
     * @return int
     */
    int deleteByPrimaryKey(String id);

    
    /**
     * @param record record
     * @return int
     */
    int insert(SfcResourceInfo record);

    
    /**
     * @param record record
     * @return int
     */
    int insertSelective(SfcResourceInfo record);

    
    /**
     * @param example example
     * @return List<SfcResourceInfo>
     */
    List<SfcResourceInfo> selectByExample(SfcResourceInfoExample example);

    
    /**
     * @param id id
     * @return SfcResourceInfo
     */
    SfcResourceInfo selectByPrimaryKey(String id);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExampleSelective(@Param("record") SfcResourceInfo record, @Param("example") SfcResourceInfoExample example);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExample(@Param("record") SfcResourceInfo record, @Param("example") SfcResourceInfoExample example);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKeySelective(SfcResourceInfo record);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKey(SfcResourceInfo record);
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/