package com.fenglun.shunfengche.controller.base;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.fenglun.shunfengche.component.RedisComponent;
import com.fenglun.shunfengche.constant.ShunFengCheConstant;
import com.fenglun.shunfengche.entity.user.SfcUserInfo;
import com.fenglun.shunfengche.pojo.security.UserInfo;
import com.fenglun.shunfengche.service.SfcUserService;
import com.fenglun.shunfengche.utils.StringUtils;
/**
 * 
 * @project: 顺风车
 * @description: 基础controller
 * @version 1.0.0
 * @errorcode 错误码: 错误描述
 *
 * @author
 *         <li>2021-01-20 825338623@qq.com Create 1.0
 * 
 * @copyright ©2019-2021 顺风车，版权所有。
 */
@Controller
public class BaseController {

    @Autowired
	private RedisComponent redisUtil;

	@Autowired
	private SfcUserService sfcUserService;

	/**
	 * 
	 * 获取请求头中的token
	 *
	 * @param request
	 *            请求信息
	 * @return
	 */
	public String getToken(HttpServletRequest request) {
		String token = request.getHeader(ShunFengCheConstant.CLIENT_TOKEN_KEY);
		if (StringUtils.equals(token, "undefined")) {
			return null;
		}
		return token;
	}


	/**
	 * 
	 * 获取用户真实IP地址，不使用request.getRemoteAddr();的原因是有可能用户使用了代理软件方式避免真实IP地址, 参考文章：
	 * http://developer.51cto.com/art/201111/305181.htm
	 * 
	 * 可是，如果通过了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP值，究竟哪个才是真正的用户端的真实IP呢？
	 * 答案是取X-Forwarded-For中第一个非unknown的有效IP字符串。
	 * 
	 * 如：X-Forwarded-For：192.168.1.110, 192.168.1.120, 192.168.1.130,
	 * 192.168.1.100
	 * 
	 * 用户真实IP为： 192.168.1.110
	 * 
	 * @param request
	 * @return
	 */
	public String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
    
    /**
	 * 
	 * 获取当前请求的用户信息
	 *
	 * @param request
	 *            请求信息
	 * @return
	 */
	public SfcUserInfo getRequestUser(HttpServletRequest request) {
        String token = getToken(request);
		UserInfo userInfo = (UserInfo) redisUtil
				.get(ShunFengCheConstant.TOKEN_REDIS_KEY + token);
		return sfcUserService.queryUserById(userInfo.getId());
    }
}
