package com.fenglun.shunfengche.pojo.base;

import lombok.Data;

import java.util.List;

/**
 * @project: 顺风车
 * @description: 分页返回结果
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-08-05 825338623@qq.com Create 1.0
 * @copyright ©2019-2020 顺风车，版权所有。
 */
@Data
public class PageResponse<T> {
    /**
     * 当前页码
     */
    private int pageNum;
    /**
     * 每页数量
     */
    private int pageSize;
    /**
     * 记录总数
     */
    private long totalSize;
    /**
     * 数据模型
     */
    private List<T> content;

}