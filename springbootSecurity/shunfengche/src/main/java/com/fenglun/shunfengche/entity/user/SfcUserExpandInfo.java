package com.fenglun.shunfengche.entity.user;

import java.util.Date;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
public class SfcUserExpandInfo {

    
    /**编号 */
    private String id;
    
    /**用户编号 */
    private String userId;
    
    /**身份证正面 */
    private String userIdCard1;
    
    /**身份证反面 */
    private String userIdCard2;
    
    /**创建时间 */
    private Date createDatetime;
    
    /**修改时间 */
    private Date updateDatetime;

    
    /**
     * @return 编号
     */
    public String getId() {
        return id;
    }
    
    /**
     * @param id 编号
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }
    
    /**
     * @return 用户编号
     */
    public String getUserId() {
        return userId;
    }
    
    /**
     * @param userId 用户编号
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }
    
    /**
     * @return 身份证正面
     */
    public String getUserIdCard1() {
        return userIdCard1;
    }
    
    /**
     * @param userIdCard1 身份证正面
     */
    public void setUserIdCard1(String userIdCard1) {
        this.userIdCard1 = userIdCard1 == null ? null : userIdCard1.trim();
    }
    
    /**
     * @return 身份证反面
     */
    public String getUserIdCard2() {
        return userIdCard2;
    }
    
    /**
     * @param userIdCard2 身份证反面
     */
    public void setUserIdCard2(String userIdCard2) {
        this.userIdCard2 = userIdCard2 == null ? null : userIdCard2.trim();
    }
    
    /**
     * @return 创建时间
     */
    public Date getCreateDatetime() {
        return createDatetime == null ? null : (Date) createDatetime.clone();
    }
    
    /**
     * @param createDatetime 创建时间
     */
    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime == null ? null : (Date) createDatetime.clone();
    }
    
    /**
     * @return 修改时间
     */
    public Date getUpdateDatetime() {
        return updateDatetime == null ? null : (Date) updateDatetime.clone();
    }
    
    /**
     * @param updateDatetime 修改时间
     */
    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime == null ? null : (Date) updateDatetime.clone();
    }
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/