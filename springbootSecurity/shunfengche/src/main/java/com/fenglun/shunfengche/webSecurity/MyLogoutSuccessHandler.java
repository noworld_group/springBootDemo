package com.fenglun.shunfengche.webSecurity;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fenglun.shunfengche.enums.ShunFengCheErrorEnums;
import com.fenglun.shunfengche.pojo.base.ResponseData;
import com.fenglun.shunfengche.pojo.security.Token;

import lombok.extern.slf4j.Slf4j;
/**
 * 
 * @project: 顺风车
 * @description: 退出登录后的处理
 * @version 1.0.0
 * @errorcode 错误码: 错误描述
 *
 * @author
 *         <li>2021-01-19 825338623@qq.com Create 1.0
 * 
 * @copyright ©2019-2021 顺风车，版权所有。
 */
@Slf4j
@Component
public class MyLogoutSuccessHandler implements LogoutSuccessHandler {

	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public void onLogoutSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		// TODO 是否需要在这里处理掉redis中的token
		log.info("自定义退出成功后的逻辑>>>>>>>>>>>>>>>>>");
		ResponseData<Token> resultMap = new ResponseData<Token>(
				ShunFengCheErrorEnums.COM_SUCCESS.getErrorNo(),
				ShunFengCheErrorEnums.COM_USER_HAS_LOG_OUT
						.getErrorConsonleInfo());
		response.getWriter().write(objectMapper.writeValueAsString(resultMap));
	}

}
