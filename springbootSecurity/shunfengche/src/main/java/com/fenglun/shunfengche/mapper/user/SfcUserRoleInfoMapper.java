package com.fenglun.shunfengche.mapper.user;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.fenglun.shunfengche.entity.user.SfcUserRoleInfo;
import com.fenglun.shunfengche.entity.user.SfcUserRoleInfoExample;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
@Mapper
public interface SfcUserRoleInfoMapper {
    
    /**
     * @param example example
     * @return int
     */
    int countByExample(SfcUserRoleInfoExample example);

    
    /**
     * @param example example
     * @return int
     */
    int deleteByExample(SfcUserRoleInfoExample example);

    
    /**
     * @param id id
     * @return int
     */
    int deleteByPrimaryKey(String id);

    
    /**
     * @param record record
     * @return int
     */
    int insert(SfcUserRoleInfo record);

    
    /**
     * @param record record
     * @return int
     */
    int insertSelective(SfcUserRoleInfo record);

    
    /**
     * @param example example
     * @return List<SfcUserRoleInfo>
     */
    List<SfcUserRoleInfo> selectByExample(SfcUserRoleInfoExample example);

    
    /**
     * @param id id
     * @return SfcUserRoleInfo
     */
    SfcUserRoleInfo selectByPrimaryKey(String id);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExampleSelective(@Param("record") SfcUserRoleInfo record, @Param("example") SfcUserRoleInfoExample example);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExample(@Param("record") SfcUserRoleInfo record, @Param("example") SfcUserRoleInfoExample example);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKeySelective(SfcUserRoleInfo record);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKey(SfcUserRoleInfo record);
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/