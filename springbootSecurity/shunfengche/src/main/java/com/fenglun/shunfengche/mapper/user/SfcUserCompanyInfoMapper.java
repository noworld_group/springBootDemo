package com.fenglun.shunfengche.mapper.user;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.fenglun.shunfengche.entity.user.SfcUserCompanyInfo;
import com.fenglun.shunfengche.entity.user.SfcUserCompanyInfoExample;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
@Mapper
public interface SfcUserCompanyInfoMapper {
    
    /**
     * @param example example
     * @return int
     */
    int countByExample(SfcUserCompanyInfoExample example);

    
    /**
     * @param example example
     * @return int
     */
    int deleteByExample(SfcUserCompanyInfoExample example);

    
    /**
     * @param id id
     * @return int
     */
    int deleteByPrimaryKey(String id);

    
    /**
     * @param record record
     * @return int
     */
    int insert(SfcUserCompanyInfo record);

    
    /**
     * @param record record
     * @return int
     */
    int insertSelective(SfcUserCompanyInfo record);

    
    /**
     * @param example example
     * @return List<SfcUserCompanyInfo>
     */
    List<SfcUserCompanyInfo> selectByExample(SfcUserCompanyInfoExample example);

    
    /**
     * @param id id
     * @return SfcUserCompanyInfo
     */
    SfcUserCompanyInfo selectByPrimaryKey(String id);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExampleSelective(@Param("record") SfcUserCompanyInfo record, @Param("example") SfcUserCompanyInfoExample example);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExample(@Param("record") SfcUserCompanyInfo record, @Param("example") SfcUserCompanyInfoExample example);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKeySelective(SfcUserCompanyInfo record);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKey(SfcUserCompanyInfo record);
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/