package com.fenglun.shunfengche.webSecurity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;

import lombok.extern.slf4j.Slf4j;

/***

@project: 顺风车
 * @description: 中文类名。 类功能简介。
 * @version 1.0.0
 * @errorcode 错误码: 错误描述
 * @author
 * <li>2021-01-19 825338623@qq.com Create 1.0
 * @copyright ©2019-2021 顺风车，版权所有。
 */
 @Configuration // https://cloud.tencent.com/developer/article/1772894
 @EnableWebSecurity
 @Slf4j
// @EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
 public class TestWebSecurity extends WebSecurityConfigurerAdapter
 {

 private static final String[] EX_CLUDED_AUTH_PAGES = { "/auth/login", "/auth/logout",
 "/route/**", "/v2/api-docs",
 "/configuration/ui", "/swagger-resources",
 "/configuration/security", "/swagger-ui.html", "/webjars/**",
 "/swagger-resources/configuration/ui",
 "/swagge‌​r-ui.html" };

 /** 处理成功后的逻辑 */
 @Autowired
 private MyAuthenticationSuccessHandler myAuthenticationSuccessHandler;

 @Autowired
 private MyLogoutSuccessHandler myLogoutSuccessHandler;

 @Autowired
 private MyLogoutHandler myLogoutHandler;

 /**
 * 登录校验失败的处理逻辑
 */
 @Autowired
 private MyAuthenctiationFailureHandler myAuthenctiationFailureHandler;

 @Autowired
 private SecurityUserDetails securityUserDetails;

 @Autowired
 private AuthenticationEntryPoint authenticationEntryPoint;

 // 控制请求访问权限
 @Override
 protected void configure(HttpSecurity http) throws Exception
 {
 log.info("进入security 配置>>>>>>>>>>>>");
 // 定制请求授权规则
 http.authorizeRequests().antMatchers(EX_CLUDED_AUTH_PAGES).permitAll().anyRequest().authenticated();
 // 开启自动配置的登录功能
 // 1、login请求来到登录页面
 // 2、重定向/login?Error表示登录失败
 // 3、设置转到我们自己的登录界面
 // 4、自定义的登录界面要发送post请求，action需要为/login，字段要匹配这里的
 http.formLogin().usernameParameter("username").passwordParameter("password").loginPage("/auth/login")
 .successHandler(myAuthenticationSuccessHandler) // 登录成功的处理逻辑
 .failureHandler(myAuthenctiationFailureHandler);// 登录失败的处理逻辑
 // 开启自动配置的注销功能
 // 1、访问logout表示用户注销，清空session
 // 2、默认注销成功会返回login?logout页面
 // 3、设置注销成功来到首页
 http.logout().logoutUrl("/auth/logout").addLogoutHandler(myLogoutHandler)
 .logoutSuccessHandler(myLogoutSuccessHandler);

 // 必须支持跨域
 http.csrf().disable();
 // 异常处理(权限拒绝、登录失效等)
 http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint);// 匿名用户访问无权限资源时的异常处理
 // 开启记住我功能
 http.rememberMe().rememberMeParameter("remember");
 }

 @Bean
 public PasswordEncoder passwordEncoder()
 {
 return new BCryptPasswordEncoder();
 }

 @Override
 @Autowired
 public void configure(AuthenticationManagerBuilder builder) throws Exception
 {
 builder.userDetailsService(securityUserDetails).passwordEncoder(passwordEncoder());
 }
 }
