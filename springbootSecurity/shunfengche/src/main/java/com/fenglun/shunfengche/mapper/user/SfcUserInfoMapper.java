package com.fenglun.shunfengche.mapper.user;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.fenglun.shunfengche.entity.user.SfcUserInfo;
import com.fenglun.shunfengche.entity.user.SfcUserInfoExample;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
@Mapper
public interface SfcUserInfoMapper {
    
    /**
     * @param example example
     * @return int
     */
    int countByExample(SfcUserInfoExample example);

    
    /**
     * @param example example
     * @return int
     */
    int deleteByExample(SfcUserInfoExample example);

    
    /**
     * @param id id
     * @return int
     */
    int deleteByPrimaryKey(String id);

    
    /**
     * @param record record
     * @return int
     */
    int insert(SfcUserInfo record);

    
    /**
     * @param record record
     * @return int
     */
    int insertSelective(SfcUserInfo record);

    
    /**
     * @param example example
     * @return List<SfcUserInfo>
     */
    List<SfcUserInfo> selectByExample(SfcUserInfoExample example);

    
    /**
     * @param id id
     * @return SfcUserInfo
     */
    SfcUserInfo selectByPrimaryKey(String id);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExampleSelective(@Param("record") SfcUserInfo record, @Param("example") SfcUserInfoExample example);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExample(@Param("record") SfcUserInfo record, @Param("example") SfcUserInfoExample example);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKeySelective(SfcUserInfo record);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKey(SfcUserInfo record);
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/