package com.fenglun.shunfengche.constant;

/**
 * @project: 顺风车
 * @description: 常量类。
 * @version 1.0.0
 * @errorcode 错误码: 错误描述
 * @author
 *         <li>2021-01-18 825338623@qq.com Create 1.0
 * @copyright ©2019-2021 顺风车，版权所有。
 */
public class ShunFengCheConstant
{
    
    /**
     * token的KEY
     */
	public static final String CLIENT_TOKEN_KEY = "X-Token";
    // 存入redis的token key
    public static final String TOKEN_REDIS_KEY = "innovation:login:";
    // 存入redis的token-userId key
    public static final String TOKEN_REDIS_KEY_USER_ID = "innovation:login-id:";
    
    // 登录后续约超时时间，单位秒
    public static final long USER_TOKEN_TIME = 3600;
    // 请求时间戳与当前时间戳比较 单位 分钟
    public static final long MINUTES_5 = USER_TOKEN_TIME * 1000L;
    
    // 用户状态，1:正常，2:违规停用，3:离职停用
    public static final String USER_STATUS_NORMAL                   = "1";
    public static final String USER_STATUS_VIOLATION_DEACTIVATION   = "2";
    public static final String USER_STATUS_RESIGNATION_DEACTIVATION = "3";
    
}
