package com.fenglun.shunfengche.entity.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fenglun.shunfengche.enums.ShunFengCheErrorEnums;
import com.fenglun.shunfengche.exception.BusinessException;
import com.fenglun.shunfengche.utils.StringUtils;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
public class SfcUserExpandInfoExample {

    
    /**orderByClause */
    protected String orderByClause;
    
    /**distinct */
    protected boolean distinct;
    
    /**oredCriteria */
    protected List<Criteria> oredCriteria;

    
    /**
     * 构造方法 
     */
    public SfcUserExpandInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }
    
    /**
     * @param orderByClause orderByClause
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }
    
    /**
     * @return String
     */
    public String getOrderByClause() {
        return orderByClause;
    }
    
    /**
     * @param distinct distinct
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }
    
    /**
     * @return boolean
     */
    public boolean isDistinct() {
        return distinct;
    }
    
    /**
     * @return List<Criteria>
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }
    
    /**
     * @param criteria criteria
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }
    
    /**
     * @return Criteria
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }
    
    /**
     * @return Criteria
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }
    
    /**
     * @return Criteria
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }
    
    /**
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * @project:用户管理模块
     * @description:null
     * @version 1.0.0
     * @errorcode
     *            错误码: 错误描述
     * @author
     *         <li>2019-01-23 superFei Create 1.0
     */
    protected abstract static class GeneratedCriteria {

        
        /**criteria */
        protected List<Criterion> criteria;

        
        /**
         * 构造方法 
         */
        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }
        
        /**
         * @return boolean
         */
        public boolean isValid() {
            return criteria.size() > 0;
        }
        
        /**
         * @return List<Criterion>
         */
        public List<Criterion> getAllCriteria() {
            return criteria;
        }
        
        /**
         * @return List<Criterion>
         */
        public List<Criterion> getCriteria() {
            return criteria;
        }
        
        /**
         * @param condition condition
         */
        protected void addCriterion(String condition) {
            if (condition == null) {
				throw new BusinessException(
						ShunFengCheErrorEnums.COM_ERROR.getErrorNo(),
						StringUtils.formatStr(
								ShunFengCheErrorEnums.COM_ERROR
										.getErrorConsonleInfo(),
								"Value for condition cannot be null"));
            }
            criteria.add(new Criterion(condition));
        }
        
        /**
         * @param condition condition
         * @param value value
         * @param property property
         */
        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
				throw new BusinessException(
						ShunFengCheErrorEnums.COM_ERROR.getErrorNo(),
						StringUtils.formatStr(
								ShunFengCheErrorEnums.COM_ERROR
										.getErrorConsonleInfo(),
								"Value for " + property + " cannot be null"));
            }
            criteria.add(new Criterion(condition, value));
        }
        
        /**
         * @param condition condition
         * @param value1 value1
         * @param value2 value2
         * @param property property
         */
        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
				throw new BusinessException(
						ShunFengCheErrorEnums.COM_ERROR.getErrorNo(),
						StringUtils.formatStr(
								ShunFengCheErrorEnums.COM_ERROR
										.getErrorConsonleInfo(),
								"Between values for " + property
										+ " cannot be null"));
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
        /**
         * @return Criteria
         */
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserIdCard1IsNull() {
            addCriterion("USER_ID_CARD_1 is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserIdCard1IsNotNull() {
            addCriterion("USER_ID_CARD_1 is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard1EqualTo(String value) {
            addCriterion("USER_ID_CARD_1 =", value, "userIdCard1");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard1NotEqualTo(String value) {
            addCriterion("USER_ID_CARD_1 <>", value, "userIdCard1");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard1GreaterThan(String value) {
            addCriterion("USER_ID_CARD_1 >", value, "userIdCard1");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard1GreaterThanOrEqualTo(String value) {
            addCriterion("USER_ID_CARD_1 >=", value, "userIdCard1");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard1LessThan(String value) {
            addCriterion("USER_ID_CARD_1 <", value, "userIdCard1");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard1LessThanOrEqualTo(String value) {
            addCriterion("USER_ID_CARD_1 <=", value, "userIdCard1");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard1Like(String value) {
            addCriterion("USER_ID_CARD_1 like", value, "userIdCard1");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard1NotLike(String value) {
            addCriterion("USER_ID_CARD_1 not like", value, "userIdCard1");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserIdCard1In(List<String> values) {
            addCriterion("USER_ID_CARD_1 in", values, "userIdCard1");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserIdCard1NotIn(List<String> values) {
            addCriterion("USER_ID_CARD_1 not in", values, "userIdCard1");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserIdCard1Between(String value1, String value2) {
            addCriterion("USER_ID_CARD_1 between", value1, value2, "userIdCard1");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserIdCard1NotBetween(String value1, String value2) {
            addCriterion("USER_ID_CARD_1 not between", value1, value2, "userIdCard1");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserIdCard2IsNull() {
            addCriterion("USER_ID_CARD_2 is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserIdCard2IsNotNull() {
            addCriterion("USER_ID_CARD_2 is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard2EqualTo(String value) {
            addCriterion("USER_ID_CARD_2 =", value, "userIdCard2");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard2NotEqualTo(String value) {
            addCriterion("USER_ID_CARD_2 <>", value, "userIdCard2");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard2GreaterThan(String value) {
            addCriterion("USER_ID_CARD_2 >", value, "userIdCard2");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard2GreaterThanOrEqualTo(String value) {
            addCriterion("USER_ID_CARD_2 >=", value, "userIdCard2");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard2LessThan(String value) {
            addCriterion("USER_ID_CARD_2 <", value, "userIdCard2");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard2LessThanOrEqualTo(String value) {
            addCriterion("USER_ID_CARD_2 <=", value, "userIdCard2");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard2Like(String value) {
            addCriterion("USER_ID_CARD_2 like", value, "userIdCard2");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserIdCard2NotLike(String value) {
            addCriterion("USER_ID_CARD_2 not like", value, "userIdCard2");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserIdCard2In(List<String> values) {
            addCriterion("USER_ID_CARD_2 in", values, "userIdCard2");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserIdCard2NotIn(List<String> values) {
            addCriterion("USER_ID_CARD_2 not in", values, "userIdCard2");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserIdCard2Between(String value1, String value2) {
            addCriterion("USER_ID_CARD_2 between", value1, value2, "userIdCard2");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserIdCard2NotBetween(String value1, String value2) {
            addCriterion("USER_ID_CARD_2 not between", value1, value2, "userIdCard2");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andCreateDatetimeIsNull() {
            addCriterion("CREATE_DATETIME is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andCreateDatetimeIsNotNull() {
            addCriterion("CREATE_DATETIME is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreateDatetimeEqualTo(Date value) {
            addCriterion("CREATE_DATETIME =", value, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreateDatetimeNotEqualTo(Date value) {
            addCriterion("CREATE_DATETIME <>", value, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreateDatetimeGreaterThan(Date value) {
            addCriterion("CREATE_DATETIME >", value, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreateDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATE_DATETIME >=", value, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreateDatetimeLessThan(Date value) {
            addCriterion("CREATE_DATETIME <", value, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreateDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("CREATE_DATETIME <=", value, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andCreateDatetimeIn(List<Date> values) {
            addCriterion("CREATE_DATETIME in", values, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andCreateDatetimeNotIn(List<Date> values) {
            addCriterion("CREATE_DATETIME not in", values, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andCreateDatetimeBetween(Date value1, Date value2) {
            addCriterion("CREATE_DATETIME between", value1, value2, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andCreateDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("CREATE_DATETIME not between", value1, value2, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUpdateDatetimeIsNull() {
            addCriterion("UPDATE_DATETIME is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUpdateDatetimeIsNotNull() {
            addCriterion("UPDATE_DATETIME is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDatetimeEqualTo(Date value) {
            addCriterion("UPDATE_DATETIME =", value, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDatetimeNotEqualTo(Date value) {
            addCriterion("UPDATE_DATETIME <>", value, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDatetimeGreaterThan(Date value) {
            addCriterion("UPDATE_DATETIME >", value, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("UPDATE_DATETIME >=", value, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDatetimeLessThan(Date value) {
            addCriterion("UPDATE_DATETIME <", value, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("UPDATE_DATETIME <=", value, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUpdateDatetimeIn(List<Date> values) {
            addCriterion("UPDATE_DATETIME in", values, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUpdateDatetimeNotIn(List<Date> values) {
            addCriterion("UPDATE_DATETIME not in", values, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUpdateDatetimeBetween(Date value1, Date value2) {
            addCriterion("UPDATE_DATETIME between", value1, value2, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUpdateDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("UPDATE_DATETIME not between", value1, value2, "updateDatetime");
            return (Criteria) this;
        }
    }
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/

    /**
     * @project:用户管理模块
     * @description:null
     * @version 1.0.0
     * @errorcode
     *            错误码: 错误描述
     * @author
     *         <li>2019-01-23 superFei Create 1.0
     */
    public static class Criteria extends GeneratedCriteria {


        
        /**
         * 构造方法 
         */
        protected Criteria() {
            super();
        }
    }
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/

    /**
     * @project:用户管理模块
     * @description:null
     * @version 1.0.0
     * @errorcode
     *            错误码: 错误描述
     * @author
     *         <li>2019-01-23 superFei Create 1.0
     */
    public static class Criterion {

        
        /**condition */
        private String condition;
        
        /**value */
        private Object value;
        
        /**secondValue */
        private Object secondValue;
        
        /**noValue */
        private boolean noValue;
        
        /**singleValue */
        private boolean singleValue;
        
        /**betweenValue */
        private boolean betweenValue;
        
        /**listValue */
        private boolean listValue;
        
        /**typeHandler */
        private String typeHandler;

        
        /**
         * @return String
         */
        public String getCondition() {
            return condition;
        }
        
        /**
         * @return Object
         */
        public Object getValue() {
            return value;
        }
        
        /**
         * @return Object
         */
        public Object getSecondValue() {
            return secondValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isNoValue() {
            return noValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isSingleValue() {
            return singleValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isBetweenValue() {
            return betweenValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isListValue() {
            return listValue;
        }
        
        /**
         * @return String
         */
        public String getTypeHandler() {
            return typeHandler;
        }
        
        /**
         * 构造方法 
         * @param condition condition
         */
        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         * @param typeHandler typeHandler
         */
        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         */
        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         * @param secondValue secondValue
         * @param typeHandler typeHandler
         */
        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         * @param secondValue secondValue
         */
        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/