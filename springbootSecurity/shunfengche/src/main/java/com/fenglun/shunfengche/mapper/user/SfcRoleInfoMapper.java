package com.fenglun.shunfengche.mapper.user;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.fenglun.shunfengche.entity.user.SfcRoleInfo;
import com.fenglun.shunfengche.entity.user.SfcRoleInfoExample;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
@Mapper
public interface SfcRoleInfoMapper {
    
    /**
     * @param example example
     * @return int
     */
    int countByExample(SfcRoleInfoExample example);

    
    /**
     * @param example example
     * @return int
     */
    int deleteByExample(SfcRoleInfoExample example);

    
    /**
     * @param id id
     * @return int
     */
    int deleteByPrimaryKey(String id);

    
    /**
     * @param record record
     * @return int
     */
    int insert(SfcRoleInfo record);

    
    /**
     * @param record record
     * @return int
     */
    int insertSelective(SfcRoleInfo record);

    
    /**
     * @param example example
     * @return List<SfcRoleInfo>
     */
    List<SfcRoleInfo> selectByExample(SfcRoleInfoExample example);

    
    /**
     * @param id id
     * @return SfcRoleInfo
     */
    SfcRoleInfo selectByPrimaryKey(String id);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExampleSelective(@Param("record") SfcRoleInfo record, @Param("example") SfcRoleInfoExample example);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExample(@Param("record") SfcRoleInfo record, @Param("example") SfcRoleInfoExample example);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKeySelective(SfcRoleInfo record);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKey(SfcRoleInfo record);
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/