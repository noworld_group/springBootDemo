package com.fenglun.shunfengche.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fenglun.shunfengche.entity.user.SfcUserInfo;
import com.fenglun.shunfengche.mapper.user.SfcUserInfoMapper;
import com.fenglun.shunfengche.service.SfcUserService;

/**
 * 
 * @project: 顺风车
 * @description: 中文类名。 类功能简介。
 * @version 1.0.0
 * @errorcode 错误码: 错误描述
 *
 * @author
 *         <li>2021-01-20 825338623@qq.com Create 1.0
 * 
 * @copyright ©2019-2021 顺风车，版权所有。
 */
@Service("sfcUserService")
public class SfcUserServiceImpl implements SfcUserService {

	@Autowired
	private SfcUserInfoMapper sfcUserInfoMapper;

	@Override
	public SfcUserInfo queryUserById(String id) {
		return sfcUserInfoMapper.selectByPrimaryKey(id);
	}

}
