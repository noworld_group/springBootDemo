package com.fenglun.shunfengche.aop;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fenglun.shunfengche.enums.ShunFengCheErrorEnums;
import com.fenglun.shunfengche.exception.BusinessException;
import com.fenglun.shunfengche.pojo.base.BaseResponse;
import com.fenglun.shunfengche.pojo.base.ResponseData;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @project: 顺风车
 * @description: 全局异常拦截处理类。
 * @version 1.0.0
 * @errorcode 错误码: 错误描述
 *
 * @author
 *         <li>2021-01-20 825338623@qq.com Create 1.0
 * 
 * @copyright ©2019-2021 顺风车，版权所有。
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
	// private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 
	 * 接口入参校验异常处理(1.针对application / json请求参数校验)
	 *
	 * @param e
	 * @return
	 */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
	public BaseResponse handleMethodArgumentNotValidException(
			MethodArgumentNotValidException e) {
        BindingResult result = e.getBindingResult();
        return this.getHandleExceptionResult(result);
    }


	/**
	 * 
	 * 接口入参校验异常处理(2.针对form表单请求参数校验)
	 *
	 * @param e
	 * @return
	 */
    @ExceptionHandler(BindException.class)
    @ResponseBody
	public BaseResponse handleBindException(BindException e) {
        BindingResult result = e.getBindingResult();
        return this.getHandleExceptionResult(result);
    }


	/**
	 * 
	 * 异常处理封装返回结果
	 *
	 * @param result
	 *            错误信息
	 * @return
	 */
	public BaseResponse getHandleExceptionResult(BindingResult result) {
        FieldError error = result.getFieldError();
        String defaultMessage = error.getDefaultMessage();
		return ResponseData.out(ShunFengCheErrorEnums.COM_SYS_ERROR,
				defaultMessage);
    }


    /**
	 * 
	 * 接口入参校验异常处理（3.针对对URL中请求参数校验）
	 *
	 * @param e
	 * @return
	 */
    @ResponseBody
    @ExceptionHandler(ConstraintViolationException.class)
	public BaseResponse handleValidationException(
			ConstraintViolationException e) {
		return ResponseData.out(ShunFengCheErrorEnums.COM_SYS_ERROR,
				e.getMessage());
    }

	/**
	 * 
	 * 功能描述
	 *
	 * @param e
	 * @return
	 */
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    @ExceptionHandler(AccessDeniedException.class)
	public BaseResponse handleAccessDeniedException(AccessDeniedException e) {
		return ResponseData.out(ShunFengCheErrorEnums.COM_SYS_ERROR,
				HttpStatus.FORBIDDEN.getReasonPhrase());
    }

    /**
     * @param e
     * @return
     * @throws
     * @Title: handleException
     * @Description: 后台系统异常拦截
     * @author LiPeng
     * @date 2019年11月1日 上午11:25:43
     */
    @ResponseBody
    @ExceptionHandler(Exception.class)
	public BaseResponse handleException(Exception e) {
		log.error("后台拦截异常Exception：错误详细信息：", e);
		return ResponseData.out(ShunFengCheErrorEnums.COM_SYS_ERROR,
				e.getMessage());
    }

	/**
	 * 
	 * 功能描述
	 *
	 * @param e
	 * @return
	 */
    @ResponseBody
    @ExceptionHandler(BusinessException.class)
	public BaseResponse handleBusinessException(BusinessException e) {
		log.error("后台拦截异常BusinessException：错误详细信息：", e);
		return ResponseData.out(ShunFengCheErrorEnums.COM_BUSINESS_ERROR,
				e.getMessage());
    }
}
