package com.fenglun.shunfengche.pojo.user;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Project: 便民(BeBianMin-BaJson)系统
 * @Description: 角色管理-资源树model
 * @Version 1.0.0
 * @Author
 *         <li>2021-01-18 guopengfeiheze@blog-china.cn Create 1.0
 * @Copyright ©2017-2021 BeBianMin（www.oipinche.com），版权所有。
 */
@Data
@ApiModel("角色管理-资源树model")
public class ResourceTreeNodeVo
{
    
    /**
     * 资源编号
     */
    @ApiModelProperty("资源编号")
    private String resourceId;
    
    /**
     * 资源名称
     */
    @ApiModelProperty("资源名称")
    private String resourceName;
    
    /**
     * 资源头像
     */
    @ApiModelProperty("资源头像")
    private String resourceIcon;
    
    /**
     * 资源类型 1:菜单;2:数据;3:功能
     */
    @ApiModelProperty("资源类型")
    private String resourceType;
    
    /**
     * 权限编码
     */
    @ApiModelProperty("权限编码")
    private String permCode;
    
    /**
     * 父节点
     */
    @ApiModelProperty("父节点")
    private String parentId;
    
    /**
     * 资源级别
     */
    @ApiModelProperty("资源级别")
    private String resourceLevel;
    
    /**
     * 资源url
     */
    @ApiModelProperty("资源url")
    private String resourceUrl;
    
    /**
     * 资源状态
     */
    @ApiModelProperty("资源状态")
    private String resourceStatus;
    
    /**
     * 排序字段
     */
    @ApiModelProperty("排序字段")
    private Integer orderNum;
    
    /**
     * 子资源列表
     */
    @ApiModelProperty("子资源列表")
    private List<ResourceTreeNodeVo> childList;
    
}
