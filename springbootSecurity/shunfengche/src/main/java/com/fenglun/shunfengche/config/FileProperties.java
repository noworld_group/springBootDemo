package com.fenglun.shunfengche.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * @project: 顺风车
 * @description: 中文类名。
 *               类功能简介。
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-10-19 825338623@qq.com Create 1.0
 * @copyright ©2019-2020 顺风车，版权所有。
 */
@Component
@Data
public class FileProperties {

	@Value("${data.dir}")
    private String basePath;
}
