package com.fenglun.shunfengche.entity.user;

import java.util.Date;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
public class SfcUserCompanyInfo {

    
    /**编号 */
    private String id;
    
    /**用户编号 */
    private String userId;
    
    /**公司编号 */
    private String companyId;
    
    /**公司证明1 */
    private String companyCard1;
    
    /**公司证明2 */
    private String companyCard2;
    
    /**公司证明3 */
    private String companyCard3;
    
    /**公司邮箱账号 */
    private String companyEmail;
    
    /**工号 */
    private String jobNumber;
    
    /**创建时间 */
    private Date createDatetime;
    
    /**修改时间 */
    private Date updateDatetime;

    
    /**
     * @return 编号
     */
    public String getId() {
        return id;
    }
    
    /**
     * @param id 编号
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }
    
    /**
     * @return 用户编号
     */
    public String getUserId() {
        return userId;
    }
    
    /**
     * @param userId 用户编号
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }
    
    /**
     * @return 公司编号
     */
    public String getCompanyId() {
        return companyId;
    }
    
    /**
     * @param companyId 公司编号
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }
    
    /**
     * @return 公司证明1
     */
    public String getCompanyCard1() {
        return companyCard1;
    }
    
    /**
     * @param companyCard1 公司证明1
     */
    public void setCompanyCard1(String companyCard1) {
        this.companyCard1 = companyCard1 == null ? null : companyCard1.trim();
    }
    
    /**
     * @return 公司证明2
     */
    public String getCompanyCard2() {
        return companyCard2;
    }
    
    /**
     * @param companyCard2 公司证明2
     */
    public void setCompanyCard2(String companyCard2) {
        this.companyCard2 = companyCard2 == null ? null : companyCard2.trim();
    }
    
    /**
     * @return 公司证明3
     */
    public String getCompanyCard3() {
        return companyCard3;
    }
    
    /**
     * @param companyCard3 公司证明3
     */
    public void setCompanyCard3(String companyCard3) {
        this.companyCard3 = companyCard3 == null ? null : companyCard3.trim();
    }
    
    /**
     * @return 公司邮箱账号
     */
    public String getCompanyEmail() {
        return companyEmail;
    }
    
    /**
     * @param companyEmail 公司邮箱账号
     */
    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail == null ? null : companyEmail.trim();
    }
    
    /**
     * @return 工号
     */
    public String getJobNumber() {
        return jobNumber;
    }
    
    /**
     * @param jobNumber 工号
     */
    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber == null ? null : jobNumber.trim();
    }
    
    /**
     * @return 创建时间
     */
    public Date getCreateDatetime() {
        return createDatetime == null ? null : (Date) createDatetime.clone();
    }
    
    /**
     * @param createDatetime 创建时间
     */
    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime == null ? null : (Date) createDatetime.clone();
    }
    
    /**
     * @return 修改时间
     */
    public Date getUpdateDatetime() {
        return updateDatetime == null ? null : (Date) updateDatetime.clone();
    }
    
    /**
     * @param updateDatetime 修改时间
     */
    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime == null ? null : (Date) updateDatetime.clone();
    }
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/