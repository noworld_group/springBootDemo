package com.fenglun.shunfengche.entity.user;

import java.util.Date;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
public class SfcRoleResourceInfo {

    
    /**编号 */
    private String id;
    
    /**角色编号 */
    private String roleId;
    
    /**资源编号 */
    private String resourceId;
    
    /**创建时间 */
    private Date createDatetime;
    
    /**修改时间 */
    private Date updateDatetime;

    
    /**
     * @return 编号
     */
    public String getId() {
        return id;
    }
    
    /**
     * @param id 编号
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }
    
    /**
     * @return 角色编号
     */
    public String getRoleId() {
        return roleId;
    }
    
    /**
     * @param roleId 角色编号
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }
    
    /**
     * @return 资源编号
     */
    public String getResourceId() {
        return resourceId;
    }
    
    /**
     * @param resourceId 资源编号
     */
    public void setResourceId(String resourceId) {
        this.resourceId = resourceId == null ? null : resourceId.trim();
    }
    
    /**
     * @return 创建时间
     */
    public Date getCreateDatetime() {
        return createDatetime == null ? null : (Date) createDatetime.clone();
    }
    
    /**
     * @param createDatetime 创建时间
     */
    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime == null ? null : (Date) createDatetime.clone();
    }
    
    /**
     * @return 修改时间
     */
    public Date getUpdateDatetime() {
        return updateDatetime == null ? null : (Date) updateDatetime.clone();
    }
    
    /**
     * @param updateDatetime 修改时间
     */
    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime == null ? null : (Date) updateDatetime.clone();
    }
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/