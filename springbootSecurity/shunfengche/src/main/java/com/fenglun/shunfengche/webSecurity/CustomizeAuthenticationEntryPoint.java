package com.fenglun.shunfengche.webSecurity;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fenglun.shunfengche.enums.ShunFengCheErrorEnums;
import com.fenglun.shunfengche.pojo.base.BaseResponse;
import com.fenglun.shunfengche.pojo.base.ResponseData;

/**
 * @Project: 便民(BeBianMin-BaJson)系统
 * @Description: 统一拦截鉴权失败处理
 * @Version 1.0.0
 * @Author
 *         <li>2021-01-19 guopengfeiheze@blog-china.cn Create 1.0
 * @Copyright ©2017-2021 BeBianMin（www.oipinche.com），版权所有。
 */
@Component
public class CustomizeAuthenticationEntryPoint implements AuthenticationEntryPoint
{
    
    @Autowired
    private ObjectMapper objectMapper;

	// @Autowired
	// private HandlerExceptionResolver resolver;
    /**
     * @see org.springframework.security.web.AuthenticationEntryPoint#commence(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse,
     *      org.springframework.security.core.AuthenticationException)
     */
    @Override
	public void commence(HttpServletRequest request,
			HttpServletResponse response,
                         AuthenticationException arg2) throws IOException, ServletException
    {
		// resolver.resolveException(request, response, null,
		// new BusinessException(ShunFengCheErrorEnums.COM_OAU_FAIL));
		// throw new BusinessException(ShunFengCheErrorEnums.COM_OAU_FAIL);
		response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		response.setContentType("application/json;charset=UTF-8");
		BaseResponse responseData = ResponseData
				.out(ShunFengCheErrorEnums.COM_BUSINESS_ERROR.getErrorNo(),
						ShunFengCheErrorEnums.COM_OAU_FAIL
								.getErrorConsonleInfo());
		response.getWriter()
				.write(objectMapper.writeValueAsString(responseData));
    }
}
