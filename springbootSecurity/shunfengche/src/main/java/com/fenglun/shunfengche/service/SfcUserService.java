package com.fenglun.shunfengche.service;

import com.fenglun.shunfengche.entity.user.SfcUserInfo;
/**
 * 
 * @project: 顺风车
 * @description: 用户管理service
 * @version 1.0.0
 * @errorcode 错误码: 错误描述
 *
 * @author
 *         <li>2021-01-20 825338623@qq.com Create 1.0
 * 
 * @copyright ©2019-2021 顺风车，版权所有。
 */
public interface SfcUserService {

	SfcUserInfo queryUserById(String id);

}
