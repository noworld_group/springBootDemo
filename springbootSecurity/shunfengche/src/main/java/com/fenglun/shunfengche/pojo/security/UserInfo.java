package com.fenglun.shunfengche.pojo.security;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fenglun.shunfengche.entity.user.SfcResourceInfo;
import com.fenglun.shunfengche.entity.user.SfcRoleInfo;

import lombok.Data;

/**
 * @project: 顺风车
 * @description: 用户信息Security
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-07 825338623@qq.com Create 1.0
 * @copyright ©2019-2020 顺风车，版权所有。
 */
@Data
public class UserInfo implements UserDetails
{

    private static final long         serialVersionUID = -5536977220434963292L;
    private String                    id;
    private String                    username;
	private List<SfcRoleInfo> roleInfos;
    @JsonIgnore
    private String                    password;
    @JsonIgnore
    private String                    token;
    /** 登陆时间戳（毫秒） */
    private Long                      loginTime;
    /** 过期时间戳 */
    private Long                      expireTime;
	private List<SfcResourceInfo> webAuthes;
	/**
	 * 权限
	 */
	private List<String> permissions;

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        if (CollectionUtils.isEmpty(webAuthes)) {
            return null;
        }
        return webAuthes
            .parallelStream()
            .map(p -> new SimpleGrantedAuthority(p.getId())).collect(Collectors.toSet());
    }

    // 账户是否未过期
    @JsonIgnore
    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }

    // 账户是否未锁定
    @JsonIgnore
    @Override
    public boolean isAccountNonLocked()
    {
        return true;
    }

    // 密码是否未过期
    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    // 账户是否激活
    @JsonIgnore
    @Override
    public boolean isEnabled()
    {
        return true;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

	public List<SfcRoleInfo> getRoleInfos() {
        return roleInfos;
    }

	public void setRoleInfos(List<SfcRoleInfo> roleInfos) {
        this.roleInfos = roleInfos;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Long loginTime) {
        this.loginTime = loginTime;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

	public List<SfcResourceInfo> getWebAuthes() {
        return webAuthes;
    }

	public void setWebAuthes(List<SfcResourceInfo> webAuthes) {
        this.webAuthes = webAuthes;
    }

	public List<String> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<String> permissions) {
		this.permissions = permissions;
	}
}
