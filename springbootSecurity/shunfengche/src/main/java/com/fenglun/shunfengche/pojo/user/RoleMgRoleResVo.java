package com.fenglun.shunfengche.pojo.user;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Project: 便民(BeBianMin-BaJson)系统
 * @Description: 角色管理-角色信息model
 * @Version 1.0.0
 * @Author
 *         <li>2021-01-18 guopengfeiheze@blog-china.cn Create 1.0
 * @Copyright ©2017-2021 BeBianMin（www.oipinche.com），版权所有。
 */
@Data
@ApiModel("角色管理-角色信息model")
public class RoleMgRoleResVo
{
    
    /**
     * 角色ID
     */
    @ApiModelProperty("角色ID")
    private String id;
    
    /**
     * 角色名称
     */
    @ApiModelProperty("角色名称")
    private String roleName;
    
    /**
     * 角色状态 启用停用 0:停用;1：启用
     */
    @ApiModelProperty("角色状态")
    private String roleStatus;
    
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String roleDesc;
    
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private Date createdDt;
    
    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    private Date updateDt;
    
    /**
     * 删除状态 0：未删除，1：已删除
     */
    @ApiModelProperty("删除状态")
    private String deleted;
    
    /**
     * 版本号
     */
    @ApiModelProperty("版本号")
    private Long version;
    
    /**
     * 公司机构编号
     */
    @ApiModelProperty("公司机构编号")
    private String companyId;
    
    /**
     * 角色标识
     */
    @ApiModelProperty("角色标识")
    private String roleCode;
    
    /**
     * 数据权限 1-本人，2-本机构（本部门），3-本机构及下级机构（所有）
     */
    @ApiModelProperty("数据权限")
    private String dataPermission;
    
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private String createdDtStr;
    
    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    private String updateDtStr;
    
    /**
     * 菜单列表 - 注：菜单
     */
    @ApiModelProperty("菜单列表")
    private List<RoleMgResourceResVo> menuList;
    
    /**
     * 菜单列表 - 注：菜单
     */
    @ApiModelProperty("菜单列表")
    private String menuListStr;
    
    /**
     * 已有权限
     */
    @ApiModelProperty("已有权限")
    private List<List<String>> resourceId;
    
    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getRoleName()
    {
        return roleName;
    }
    
    public void setRoleName(String roleName)
    {
        this.roleName = roleName;
    }
    
    public String getRoleStatus()
    {
        return roleStatus;
    }
    
    public void setRoleStatus(String roleStatus)
    {
        this.roleStatus = roleStatus;
    }
    
    public String getRoleDesc()
    {
        return roleDesc;
    }
    
    public void setRoleDesc(String roleDesc)
    {
        this.roleDesc = roleDesc;
    }
    
    public Date getCreatedDt()
    {
        return createdDt;
    }
    
    public void setCreatedDt(Date createdDt)
    {
        this.createdDt = createdDt;
    }
    
    public Date getUpdateDt()
    {
        return updateDt;
    }
    
    public void setUpdateDt(Date updateDt)
    {
        this.updateDt = updateDt;
    }
    
    public String getDeleted()
    {
        return deleted;
    }
    
    public void setDeleted(String deleted)
    {
        this.deleted = deleted;
    }
    
    public Long getVersion()
    {
        return version;
    }
    
    public void setVersion(Long version)
    {
        this.version = version;
    }
    
    public String getCompanyId()
    {
        return companyId;
    }
    
    public void setCompanyId(String companyId)
    {
        this.companyId = companyId;
    }
    
    public String getRoleCode()
    {
        return roleCode;
    }
    
    public void setRoleCode(String roleCode)
    {
        this.roleCode = roleCode;
    }
    
    public String getDataPermission()
    {
        return dataPermission;
    }
    
    public void setDataPermission(String dataPermission)
    {
        this.dataPermission = dataPermission;
    }
    
    public String getCreatedDtStr()
    {
        return createdDtStr;
    }
    
    public void setCreatedDtStr(String createdDtStr)
    {
        this.createdDtStr = createdDtStr;
    }
    
    public String getUpdateDtStr()
    {
        return updateDtStr;
    }
    
    public void setUpdateDtStr(String updateDtStr)
    {
        this.updateDtStr = updateDtStr;
    }
    
    public List<RoleMgResourceResVo> getMenuList()
    {
        return menuList;
    }
    
    public void setMenuList(List<RoleMgResourceResVo> menuList)
    {
        this.menuList = menuList;
    }
    
    public String getMenuListStr()
    {
        return menuListStr;
    }
    
    public void setMenuListStr(String menuListStr)
    {
        this.menuListStr = menuListStr;
    }
    
    public List<List<String>> getResourceId()
    {
        return resourceId;
    }
    
    public void setResourceId(List<List<String>> resourceId)
    {
        this.resourceId = resourceId;
    }
}
