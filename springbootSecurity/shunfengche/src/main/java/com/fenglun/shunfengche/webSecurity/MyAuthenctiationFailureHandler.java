package com.fenglun.shunfengche.webSecurity;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fenglun.shunfengche.enums.ShunFengCheErrorEnums;
import com.fenglun.shunfengche.pojo.base.ResponseData;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @project: 顺风车
 * @description: 登录失败的处理器
 * @version 1.0.0
 * @errorcode 错误码: 错误描述
 *
 * @author
 *         <li>2021-01-19 825338623@qq.com Create 1.0
 * 
 * @copyright ©2019-2021 顺风车，版权所有。
 */
@Component
@Slf4j
public class MyAuthenctiationFailureHandler extends SimpleUrlAuthenticationFailureHandler {


    @Autowired
    private ObjectMapper objectMapper;

    // AuthenticationException 认证过程中产生的异常
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException exception) throws IOException, ServletException {
		logger.info("登录失败后的处理逻辑>>>>>>>>>>>>>>");
		String errorMsg = exception.getMessage().toString();
		String dataBytes;
		if (errorMsg.contains("Invalid Credentials")
				|| errorMsg.contains("Bad credentials")) {
			ResponseData<String> resultMap = new ResponseData<String>(
					ShunFengCheErrorEnums.COM_BUSINESS_ERROR.getErrorNo(),
					ShunFengCheErrorEnums.COM_SIGN_IN_USER_NAME_OR_PWD_NOT_RIGHT
							.getErrorConsonleInfo());
			dataBytes = objectMapper.writeValueAsString(resultMap);
		} else {
			log.error("注意，没有捕获到的异常>>>>>>>>>>>>>>>>", errorMsg);
			ResponseData<String> resultMap = new ResponseData<String>(
					ShunFengCheErrorEnums.COM_SYS_ERROR.getErrorNo(),
					errorMsg);
			dataBytes = objectMapper.writeValueAsString(resultMap);
		}
		response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		response.setContentType("application/json;charset=UTF-8");
		response.getWriter().write(dataBytes);
    }

}
