package com.fenglun.shunfengche.mapper.user;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.fenglun.shunfengche.entity.user.SfcResourceInfo;

/**
 * @project: 顺风车
 * @description: 自定义资源mapper。
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-10-15 825338623@qq.com Create 1.0
 * @copyright ©2019-2020 顺风车，版权所有。
 */
@Mapper
public interface SfcResourceInfoExtendMapper {
    
	public List<SfcResourceInfo> getResourceInfoByRoleIds(
			@Param("roleIds") List<String> roleIds);

}
