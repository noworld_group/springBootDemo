package com.fenglun.shunfengche.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fenglun.shunfengche.controller.base.BaseController;
import com.fenglun.shunfengche.entity.user.SfcUserInfo;
import com.fenglun.shunfengche.enums.ShunFengCheErrorEnums;
import com.fenglun.shunfengche.pojo.base.BaseResponse;
import com.fenglun.shunfengche.pojo.base.ResponseData;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("/test")
@Api(value = "测试管理模块", tags = "测试模块")
public class TestController extends BaseController {

	
	
	@PostMapping("/listOrientations")
	@ApiOperation("分页查询岗位方向列表")
	@ResponseBody
	public BaseResponse listOrientations(HttpServletRequest request) {
		String ip = getIpAddr(request);
		System.out.println(ip);
		String token = getToken(request);
		System.out.println(token);
		SfcUserInfo sfcUserInfo = getRequestUser(request);
		System.out.println(sfcUserInfo.getUserName());

		return ResponseData.out(ShunFengCheErrorEnums.COM_SUCCESS,
				"dd");
	}
}
