package com.fenglun.shunfengche.webSecurity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import com.fenglun.shunfengche.component.RedisComponent;
import com.fenglun.shunfengche.constant.ShunFengCheConstant;
import com.fenglun.shunfengche.pojo.security.UserInfo;
import com.fenglun.shunfengche.utils.StringUtils;

import lombok.extern.slf4j.Slf4j;
/**
 * 
 * @project: 顺风车
 * @description: 自定义退出登录逻辑
 * @version 1.0.0
 * @errorcode 错误码: 错误描述
 *
 * @author
 *         <li>2021-01-19 825338623@qq.com Create 1.0
 * 
 * @copyright ©2019-2021 顺风车，版权所有。
 */
@Slf4j
@Component
public class MyLogoutHandler implements LogoutHandler {

	@Autowired
	private RedisComponent redisUtil;

	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) {
		log.info("进入自定义退出登录的接口");
		response.setContentType("application/json;charset=UTF-8");
		String token = request.getHeader(ShunFengCheConstant.CLIENT_TOKEN_KEY);
		if (StringUtils.equals(token, "undefined")) {
			return;
		}
		UserInfo userInfo = (UserInfo) redisUtil.get(ShunFengCheConstant.TOKEN_REDIS_KEY + token);
		if (userInfo != null) {
			redisUtil.del(ShunFengCheConstant.TOKEN_REDIS_KEY + token);
        }
		log.info("token: {}  退出登录成功！", token);
	}


}
