package com.fenglun.shunfengche.pojo.security;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
/**
 * 
 * @project: 顺风车
 * @description: token信息。
 * @version 1.0.0
 * @errorcode 错误码: 错误描述
 *
 * @author
 *         <li>2021-01-18 825338623@qq.com Create 1.0
 * 
 * @copyright ©2019-2021 顺风车，版权所有。
 */
@Data
public class Token implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 7410231617308949168L;
    private String            token;

    private List<String> permissions;
	/** 登陆时间戳（毫秒） */
	private Long loginTime;

	public Token(String token, Long loginTime) {
		super();
		this.token = token;
		this.loginTime = loginTime;
	}

	public Token(String token, List<String> permissions, Long loginTime) {
		this.token = token;
		this.permissions = permissions;
		this.loginTime = loginTime;
	}
}
