package com.fenglun.shunfengche.mapper.user;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.fenglun.shunfengche.entity.user.SfcUserExpandInfo;
import com.fenglun.shunfengche.entity.user.SfcUserExpandInfoExample;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
@Mapper
public interface SfcUserExpandInfoMapper {
    
    /**
     * @param example example
     * @return int
     */
    int countByExample(SfcUserExpandInfoExample example);

    
    /**
     * @param example example
     * @return int
     */
    int deleteByExample(SfcUserExpandInfoExample example);

    
    /**
     * @param id id
     * @return int
     */
    int deleteByPrimaryKey(String id);

    
    /**
     * @param record record
     * @return int
     */
    int insert(SfcUserExpandInfo record);

    
    /**
     * @param record record
     * @return int
     */
    int insertSelective(SfcUserExpandInfo record);

    
    /**
     * @param example example
     * @return List<SfcUserExpandInfo>
     */
    List<SfcUserExpandInfo> selectByExample(SfcUserExpandInfoExample example);

    
    /**
     * @param id id
     * @return SfcUserExpandInfo
     */
    SfcUserExpandInfo selectByPrimaryKey(String id);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExampleSelective(@Param("record") SfcUserExpandInfo record, @Param("example") SfcUserExpandInfoExample example);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExample(@Param("record") SfcUserExpandInfo record, @Param("example") SfcUserExpandInfoExample example);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKeySelective(SfcUserExpandInfo record);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKey(SfcUserExpandInfo record);
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/