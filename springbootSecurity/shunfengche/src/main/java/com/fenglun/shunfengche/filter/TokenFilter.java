package com.fenglun.shunfengche.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import com.fenglun.shunfengche.component.RedisComponent;
import com.fenglun.shunfengche.constant.ShunFengCheConstant;
import com.fenglun.shunfengche.entity.user.SfcResourceInfo;
import com.fenglun.shunfengche.entity.user.SfcRoleInfo;
import com.fenglun.shunfengche.entity.user.SfcUserInfo;
import com.fenglun.shunfengche.enums.ShunFengCheErrorEnums;
import com.fenglun.shunfengche.exception.BusinessException;
import com.fenglun.shunfengche.mapper.user.SfcResourceInfoExtendMapper;
import com.fenglun.shunfengche.mapper.user.SfcRoleInfoExtendMapper;
import com.fenglun.shunfengche.mapper.user.SfcUserInfoMapper;
import com.fenglun.shunfengche.pojo.base.ResponseData;
import com.fenglun.shunfengche.pojo.security.UserInfo;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * @project: 顺风车
 * @description: Token处理器
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-07 825338623@qq.com Create 1.0
 * @copyright ©2019-2020 顺风车，版权所有。
 */
@Component
@Slf4j
public class TokenFilter implements WebFilter
{
    
    @Autowired
    private RedisComponent redisUtil;
    
    @Value("${permission}")
    private boolean permission;
    
    @Autowired
	private SfcUserInfoMapper sfcUserInfoMapper;

	@Autowired
	private SfcRoleInfoExtendMapper sfcRoleInfoExtendMapper;

	@Autowired
	private SfcResourceInfoExtendMapper sfcResourceInfoExtendMapper;
    
    /**
     * 根据客户端传递的token，更新redis用户信息
     * 
     * @param exchange
     *            the current server exchange
     * @param chain
     *            provides a way to delegate to the next filter
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain)
    {
		log.info(
				"进入全局拦截器 >>>>>>>>>>>>>>>>>>>>>>>>");
        ServerHttpRequest request = exchange.getRequest();
        String token = request.getHeaders().getFirst(ShunFengCheConstant.CLIENT_TOKEN_KEY);
        String requestUrl = request.getURI().getPath();
        log.info("requestUrl:{}", requestUrl);
        if (StringUtils.isNotBlank(token)) {
            UserInfo userInfo = (UserInfo) redisUtil.get(ShunFengCheConstant.TOKEN_REDIS_KEY + token);
            if (userInfo != null) {
                log.info("查询到的用户信息,token：[{}],用户名:[{}],用户密码:[{}]", ShunFengCheConstant.TOKEN_REDIS_KEY + token,
                    userInfo.getUsername(), userInfo.getPassword());
                long expireTime = userInfo.getExpireTime();
                long currentTime = System.currentTimeMillis();
                if (expireTime - currentTime <= ShunFengCheConstant.MINUTES_5) {

					SfcUserInfo sfcUserInfo = sfcUserInfoMapper
							.selectByPrimaryKey(userInfo.getId());
					List<String> userStatus = Arrays.asList(
							ShunFengCheConstant.USER_STATUS_VIOLATION_DEACTIVATION,
							ShunFengCheConstant.USER_STATUS_RESIGNATION_DEACTIVATION);
					if (userStatus.contains(sfcUserInfo.getUserStatus())) {
						// ResponseData<Token> resultMap = new
						// ResponseData<Token>(
						// ShunFengCheErrorEnums.COM_SUCCESS.getErrorNo(),
						// ShunFengCheErrorEnums.COM_SIGN_IN_USER_HAS_STOP
						// .getErrorConsonleInfo());
						// response.getWriter().write(
						// objectMapper.writeValueAsString(resultMap));
						return Mono.error(
								new BusinessException(
										ShunFengCheErrorEnums.COM_BUSINESS_ERROR
												.getErrorNo(),
										ShunFengCheErrorEnums.COM_SIGN_IN_USER_HAS_STOP
												.getErrorConsonleInfo()));
					}
					sfcUserInfo.setLoginTime(new Date());
					sfcUserInfoMapper.updateByPrimaryKeySelective(sfcUserInfo);
					UserInfo userDetail = new UserInfo();
					userDetail.setUsername(sfcUserInfo.getUserName());
					userDetail.setPassword(sfcUserInfo.getUserPassword());
					userDetail.setId(sfcUserInfo.getId());

					List<SfcRoleInfo> SfcRoleInfos = sfcRoleInfoExtendMapper
							.getRoleInfoByUserId(sfcUserInfo.getId());
					userDetail.setRoleInfos(SfcRoleInfos);
					if (CollectionUtils.isNotEmpty(SfcRoleInfos)) {
						List<String> roleIds = SfcRoleInfos.stream()
								.map(SfcRoleInfo::getId)
								.collect(Collectors.toList());
						List<SfcResourceInfo> SfcResourceInfos = sfcResourceInfoExtendMapper
								.getResourceInfoByRoleIds(roleIds);
						userDetail.setWebAuthes(SfcResourceInfos);
					}
					userDetail.setLoginTime(System.currentTimeMillis());
					userDetail.setExpireTime(sfcUserInfo.getLoginTime()
							.getTime()
							+ ShunFengCheConstant.MINUTES_5);
					List<SfcResourceInfo> webAuthes = userDetail.getWebAuthes();
					List<String> collect = new ArrayList<>();
					if (CollectionUtils.isNotEmpty(webAuthes)) {
						collect = webAuthes.stream()
								.map(SfcResourceInfo::getPermCode).distinct()
								.collect(Collectors.toList());
						userDetail.setPermissions(collect);
					}

                    // 更新缓存的用户信息
					redisUtil.set(ShunFengCheConstant.TOKEN_REDIS_KEY + token,
							userDetail, ShunFengCheConstant.MINUTES_5);
					redisUtil.set(
							ShunFengCheConstant.TOKEN_REDIS_KEY_USER_ID
									+ userDetail.getToken(),
							userDetail.getId(),
							ShunFengCheConstant.MINUTES_5);
                    ServerHttpResponse response = exchange.getResponse();
                    HttpHeaders httpHeaders = response.getHeaders();
                    httpHeaders.add("Content-Type", "application/json; charset=UTF-8");
                    // httpHeaders.add("Cache-Control", "no-store, no-cache, must-revalidate,
                    // max-age=0");
                    httpHeaders.add("Access-Control-Allow-Origin", "*");
                    httpHeaders.add("Access-Control-Allow-Methods", "*");
					httpHeaders.add("Access-Control-Max-Age",
							"" + ShunFengCheConstant.MINUTES_5);
                    httpHeaders.add("Access-Control-Allow-Headers", "*");
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
							userDetail.getUsername(), null,
							userDetail.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
                // 权限验证
                if (permission) {
                    log.info("需要做权限校验！");
                    List<SfcResourceInfo> bcResourceInfos = userInfo.getWebAuthes();
                    if (CollectionUtils.isEmpty(bcResourceInfos)) {
                        // 该用户没有任何权限，直接返回
                        ServerHttpResponse response = exchange.getResponse();
                        HttpHeaders httpHeaders = response.getHeaders();
                        httpHeaders.add("Content-Type", "application/json; charset=UTF-8");
                        // httpHeaders.add("Cache-Control", "no-store, no-cache, must-revalidate,
                        // max-age=0");
                        httpHeaders.add("Access-Control-Allow-Origin", "*");
                        httpHeaders.add("Access-Control-Allow-Methods", "*");
                        httpHeaders.add("Access-Control-Max-Age", "3600");
                        httpHeaders.add("Access-Control-Allow-Headers", "*");
                        byte[] bits = ResponseData.out(ShunFengCheErrorEnums.COM_CAN_NOT_VISIT_URI).toString().getBytes();
                        DataBuffer buffer = response.bufferFactory().wrap(bits);
                        return response.writeWith(Mono.just(buffer));
                        // return Mono.error(new BadCredentialsException(
                        // ResponseData.out(ShunFengCheEnums.COM_SHUN_FENG_CHE_CAN_NOT_VISIT_URI).toString()));
                    }
                    else {
                        boolean isPass = false;
                        for (SfcResourceInfo bcResourceInfo : bcResourceInfos) {
                            if (StringUtils.equals(bcResourceInfo.getPermCode(), requestUrl)) {
                                isPass = true;
                                break;
                            }
                        }
                        if (!isPass) {
                            // 该用户没有任何权限，直接返回
                            ServerHttpResponse response = exchange.getResponse();
                            HttpHeaders httpHeaders = response.getHeaders();
                            httpHeaders.add("Content-Type", "application/json; charset=UTF-8");
                            // httpHeaders.add("Cache-Control", "no-store, no-cache,
                            // must-revalidate, max-age=0");
                            httpHeaders.add("Access-Control-Allow-Origin", "*");
                            httpHeaders.add("Access-Control-Allow-Methods", "*");
                            httpHeaders.add("Access-Control-Max-Age", "3600");
                            httpHeaders.add("Access-Control-Allow-Headers", "*");
                            byte[] bits = ResponseData.out(ShunFengCheErrorEnums.COM_CAN_NOT_VISIT_URI).toString()
                                .getBytes();
                            DataBuffer buffer = response.bufferFactory().wrap(bits);
                            return response.writeWith(Mono.just(buffer));
                        }
                        else {
                            ServerHttpResponse response = exchange.getResponse();
                            HttpHeaders httpHeaders = response.getHeaders();
                            httpHeaders.add("Content-Type", "application/json; charset=UTF-8");
                            // httpHeaders.add("Cache-Control", "no-store, no-cache,
                            // must-revalidate, max-age=0");
                            httpHeaders.add("Access-Control-Allow-Origin", "*");
                            httpHeaders.add("Access-Control-Allow-Methods", "*");
                            httpHeaders.add("Access-Control-Max-Age", "3600");
                            httpHeaders.add("Access-Control-Allow-Headers", "*");
                            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                                userInfo.getUsername(), null, userInfo.getAuthorities());
                            SecurityContextHolder.getContext().setAuthentication(authentication);
                        }
                    }
                }
                
            }
        }
        return chain.filter(exchange);
    }
    
}
