package com.fenglun.shunfengche.entity.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fenglun.shunfengche.enums.ShunFengCheErrorEnums;
import com.fenglun.shunfengche.exception.BusinessException;
import com.fenglun.shunfengche.utils.StringUtils;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
public class SfcUserInfoExample {

    
    /**orderByClause */
    protected String orderByClause;
    
    /**distinct */
    protected boolean distinct;
    
    /**oredCriteria */
    protected List<Criteria> oredCriteria;

    
    /**
     * 构造方法 
     */
    public SfcUserInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }
    
    /**
     * @param orderByClause orderByClause
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }
    
    /**
     * @return String
     */
    public String getOrderByClause() {
        return orderByClause;
    }
    
    /**
     * @param distinct distinct
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }
    
    /**
     * @return boolean
     */
    public boolean isDistinct() {
        return distinct;
    }
    
    /**
     * @return List<Criteria>
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }
    
    /**
     * @param criteria criteria
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }
    
    /**
     * @return Criteria
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }
    
    /**
     * @return Criteria
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }
    
    /**
     * @return Criteria
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }
    
    /**
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * @project:用户管理模块
     * @description:null
     * @version 1.0.0
     * @errorcode
     *            错误码: 错误描述
     * @author
     *         <li>2019-01-23 superFei Create 1.0
     */
    protected abstract static class GeneratedCriteria {

        
        /**criteria */
        protected List<Criterion> criteria;

        
        /**
         * 构造方法 
         */
        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }
        
        /**
         * @return boolean
         */
        public boolean isValid() {
            return criteria.size() > 0;
        }
        
        /**
         * @return List<Criterion>
         */
        public List<Criterion> getAllCriteria() {
            return criteria;
        }
        
        /**
         * @return List<Criterion>
         */
        public List<Criterion> getCriteria() {
            return criteria;
        }
        
        /**
         * @param condition condition
         */
        protected void addCriterion(String condition) {
            if (condition == null) {
				throw new BusinessException(
						ShunFengCheErrorEnums.COM_ERROR.getErrorNo(),
						StringUtils.formatStr(
								ShunFengCheErrorEnums.COM_ERROR
										.getErrorConsonleInfo(),
								"Value for condition cannot be null"));
            }
            criteria.add(new Criterion(condition));
        }
        
        /**
         * @param condition condition
         * @param value value
         * @param property property
         */
        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
				throw new BusinessException(
						ShunFengCheErrorEnums.COM_ERROR.getErrorNo(),
						StringUtils.formatStr(
								ShunFengCheErrorEnums.COM_ERROR
										.getErrorConsonleInfo(),
								"Value for " + property + " cannot be null"));
            }
            criteria.add(new Criterion(condition, value));
        }
        
        /**
         * @param condition condition
         * @param value1 value1
         * @param value2 value2
         * @param property property
         */
        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
				throw new BusinessException(
						ShunFengCheErrorEnums.COM_ERROR.getErrorNo(),
						StringUtils.formatStr(
								ShunFengCheErrorEnums.COM_ERROR
										.getErrorConsonleInfo(),
								"Between values for " + property
										+ " cannot be null"));
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
        /**
         * @return Criteria
         */
        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserNameIsNull() {
            addCriterion("USER_NAME is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserNameIsNotNull() {
            addCriterion("USER_NAME is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameEqualTo(String value) {
            addCriterion("USER_NAME =", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("USER_NAME <>", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("USER_NAME >", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("USER_NAME >=", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameLessThan(String value) {
            addCriterion("USER_NAME <", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("USER_NAME <=", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameLike(String value) {
            addCriterion("USER_NAME like", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserNameNotLike(String value) {
            addCriterion("USER_NAME not like", value, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserNameIn(List<String> values) {
            addCriterion("USER_NAME in", values, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("USER_NAME not in", values, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("USER_NAME between", value1, value2, "userName");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("USER_NAME not between", value1, value2, "userName");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserPasswordIsNull() {
            addCriterion("USER_PASSWORD is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserPasswordIsNotNull() {
            addCriterion("USER_PASSWORD is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserPasswordEqualTo(String value) {
            addCriterion("USER_PASSWORD =", value, "userPassword");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserPasswordNotEqualTo(String value) {
            addCriterion("USER_PASSWORD <>", value, "userPassword");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserPasswordGreaterThan(String value) {
            addCriterion("USER_PASSWORD >", value, "userPassword");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("USER_PASSWORD >=", value, "userPassword");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserPasswordLessThan(String value) {
            addCriterion("USER_PASSWORD <", value, "userPassword");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserPasswordLessThanOrEqualTo(String value) {
            addCriterion("USER_PASSWORD <=", value, "userPassword");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserPasswordLike(String value) {
            addCriterion("USER_PASSWORD like", value, "userPassword");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserPasswordNotLike(String value) {
            addCriterion("USER_PASSWORD not like", value, "userPassword");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserPasswordIn(List<String> values) {
            addCriterion("USER_PASSWORD in", values, "userPassword");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserPasswordNotIn(List<String> values) {
            addCriterion("USER_PASSWORD not in", values, "userPassword");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserPasswordBetween(String value1, String value2) {
            addCriterion("USER_PASSWORD between", value1, value2, "userPassword");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserPasswordNotBetween(String value1, String value2) {
            addCriterion("USER_PASSWORD not between", value1, value2, "userPassword");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andRealNameIsNull() {
            addCriterion("REAL_NAME is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andRealNameIsNotNull() {
            addCriterion("REAL_NAME is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameEqualTo(String value) {
            addCriterion("REAL_NAME =", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameNotEqualTo(String value) {
            addCriterion("REAL_NAME <>", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameGreaterThan(String value) {
            addCriterion("REAL_NAME >", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameGreaterThanOrEqualTo(String value) {
            addCriterion("REAL_NAME >=", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameLessThan(String value) {
            addCriterion("REAL_NAME <", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameLessThanOrEqualTo(String value) {
            addCriterion("REAL_NAME <=", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameLike(String value) {
            addCriterion("REAL_NAME like", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRealNameNotLike(String value) {
            addCriterion("REAL_NAME not like", value, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andRealNameIn(List<String> values) {
            addCriterion("REAL_NAME in", values, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andRealNameNotIn(List<String> values) {
            addCriterion("REAL_NAME not in", values, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andRealNameBetween(String value1, String value2) {
            addCriterion("REAL_NAME between", value1, value2, "realName");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andRealNameNotBetween(String value1, String value2) {
            addCriterion("REAL_NAME not between", value1, value2, "realName");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserStatusIsNull() {
            addCriterion("USER_STATUS is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserStatusIsNotNull() {
            addCriterion("USER_STATUS is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserStatusEqualTo(String value) {
            addCriterion("USER_STATUS =", value, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserStatusNotEqualTo(String value) {
            addCriterion("USER_STATUS <>", value, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserStatusGreaterThan(String value) {
            addCriterion("USER_STATUS >", value, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserStatusGreaterThanOrEqualTo(String value) {
            addCriterion("USER_STATUS >=", value, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserStatusLessThan(String value) {
            addCriterion("USER_STATUS <", value, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserStatusLessThanOrEqualTo(String value) {
            addCriterion("USER_STATUS <=", value, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserStatusLike(String value) {
            addCriterion("USER_STATUS like", value, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserStatusNotLike(String value) {
            addCriterion("USER_STATUS not like", value, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserStatusIn(List<String> values) {
            addCriterion("USER_STATUS in", values, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserStatusNotIn(List<String> values) {
            addCriterion("USER_STATUS not in", values, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserStatusBetween(String value1, String value2) {
            addCriterion("USER_STATUS between", value1, value2, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserStatusNotBetween(String value1, String value2) {
            addCriterion("USER_STATUS not between", value1, value2, "userStatus");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andLoginTimeIsNull() {
            addCriterion("LOGIN_TIME is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andLoginTimeIsNotNull() {
            addCriterion("LOGIN_TIME is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andLoginTimeEqualTo(Date value) {
            addCriterion("LOGIN_TIME =", value, "loginTime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andLoginTimeNotEqualTo(Date value) {
            addCriterion("LOGIN_TIME <>", value, "loginTime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andLoginTimeGreaterThan(Date value) {
            addCriterion("LOGIN_TIME >", value, "loginTime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andLoginTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("LOGIN_TIME >=", value, "loginTime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andLoginTimeLessThan(Date value) {
            addCriterion("LOGIN_TIME <", value, "loginTime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andLoginTimeLessThanOrEqualTo(Date value) {
            addCriterion("LOGIN_TIME <=", value, "loginTime");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andLoginTimeIn(List<Date> values) {
            addCriterion("LOGIN_TIME in", values, "loginTime");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andLoginTimeNotIn(List<Date> values) {
            addCriterion("LOGIN_TIME not in", values, "loginTime");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andLoginTimeBetween(Date value1, Date value2) {
            addCriterion("LOGIN_TIME between", value1, value2, "loginTime");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andLoginTimeNotBetween(Date value1, Date value2) {
            addCriterion("LOGIN_TIME not between", value1, value2, "loginTime");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserLogoIsNull() {
            addCriterion("USER_LOGO is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserLogoIsNotNull() {
            addCriterion("USER_LOGO is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserLogoEqualTo(String value) {
            addCriterion("USER_LOGO =", value, "userLogo");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserLogoNotEqualTo(String value) {
            addCriterion("USER_LOGO <>", value, "userLogo");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserLogoGreaterThan(String value) {
            addCriterion("USER_LOGO >", value, "userLogo");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserLogoGreaterThanOrEqualTo(String value) {
            addCriterion("USER_LOGO >=", value, "userLogo");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserLogoLessThan(String value) {
            addCriterion("USER_LOGO <", value, "userLogo");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserLogoLessThanOrEqualTo(String value) {
            addCriterion("USER_LOGO <=", value, "userLogo");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserLogoLike(String value) {
            addCriterion("USER_LOGO like", value, "userLogo");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserLogoNotLike(String value) {
            addCriterion("USER_LOGO not like", value, "userLogo");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserLogoIn(List<String> values) {
            addCriterion("USER_LOGO in", values, "userLogo");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserLogoNotIn(List<String> values) {
            addCriterion("USER_LOGO not in", values, "userLogo");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserLogoBetween(String value1, String value2) {
            addCriterion("USER_LOGO between", value1, value2, "userLogo");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserLogoNotBetween(String value1, String value2) {
            addCriterion("USER_LOGO not between", value1, value2, "userLogo");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserTelIsNull() {
            addCriterion("USER_TEL is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserTelIsNotNull() {
            addCriterion("USER_TEL is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserTelEqualTo(String value) {
            addCriterion("USER_TEL =", value, "userTel");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserTelNotEqualTo(String value) {
            addCriterion("USER_TEL <>", value, "userTel");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserTelGreaterThan(String value) {
            addCriterion("USER_TEL >", value, "userTel");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserTelGreaterThanOrEqualTo(String value) {
            addCriterion("USER_TEL >=", value, "userTel");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserTelLessThan(String value) {
            addCriterion("USER_TEL <", value, "userTel");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserTelLessThanOrEqualTo(String value) {
            addCriterion("USER_TEL <=", value, "userTel");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserTelLike(String value) {
            addCriterion("USER_TEL like", value, "userTel");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserTelNotLike(String value) {
            addCriterion("USER_TEL not like", value, "userTel");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserTelIn(List<String> values) {
            addCriterion("USER_TEL in", values, "userTel");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserTelNotIn(List<String> values) {
            addCriterion("USER_TEL not in", values, "userTel");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserTelBetween(String value1, String value2) {
            addCriterion("USER_TEL between", value1, value2, "userTel");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserTelNotBetween(String value1, String value2) {
            addCriterion("USER_TEL not between", value1, value2, "userTel");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andCardIdNumIsNull() {
            addCriterion("CARD_ID_NUM is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andCardIdNumIsNotNull() {
            addCriterion("CARD_ID_NUM is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCardIdNumEqualTo(String value) {
            addCriterion("CARD_ID_NUM =", value, "cardIdNum");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCardIdNumNotEqualTo(String value) {
            addCriterion("CARD_ID_NUM <>", value, "cardIdNum");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCardIdNumGreaterThan(String value) {
            addCriterion("CARD_ID_NUM >", value, "cardIdNum");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCardIdNumGreaterThanOrEqualTo(String value) {
            addCriterion("CARD_ID_NUM >=", value, "cardIdNum");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCardIdNumLessThan(String value) {
            addCriterion("CARD_ID_NUM <", value, "cardIdNum");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCardIdNumLessThanOrEqualTo(String value) {
            addCriterion("CARD_ID_NUM <=", value, "cardIdNum");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCardIdNumLike(String value) {
            addCriterion("CARD_ID_NUM like", value, "cardIdNum");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCardIdNumNotLike(String value) {
            addCriterion("CARD_ID_NUM not like", value, "cardIdNum");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andCardIdNumIn(List<String> values) {
            addCriterion("CARD_ID_NUM in", values, "cardIdNum");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andCardIdNumNotIn(List<String> values) {
            addCriterion("CARD_ID_NUM not in", values, "cardIdNum");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andCardIdNumBetween(String value1, String value2) {
            addCriterion("CARD_ID_NUM between", value1, value2, "cardIdNum");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andCardIdNumNotBetween(String value1, String value2) {
            addCriterion("CARD_ID_NUM not between", value1, value2, "cardIdNum");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserGenderIsNull() {
            addCriterion("USER_GENDER is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserGenderIsNotNull() {
            addCriterion("USER_GENDER is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserGenderEqualTo(String value) {
            addCriterion("USER_GENDER =", value, "userGender");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserGenderNotEqualTo(String value) {
            addCriterion("USER_GENDER <>", value, "userGender");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserGenderGreaterThan(String value) {
            addCriterion("USER_GENDER >", value, "userGender");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserGenderGreaterThanOrEqualTo(String value) {
            addCriterion("USER_GENDER >=", value, "userGender");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserGenderLessThan(String value) {
            addCriterion("USER_GENDER <", value, "userGender");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserGenderLessThanOrEqualTo(String value) {
            addCriterion("USER_GENDER <=", value, "userGender");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserGenderLike(String value) {
            addCriterion("USER_GENDER like", value, "userGender");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserGenderNotLike(String value) {
            addCriterion("USER_GENDER not like", value, "userGender");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserGenderIn(List<String> values) {
            addCriterion("USER_GENDER in", values, "userGender");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserGenderNotIn(List<String> values) {
            addCriterion("USER_GENDER not in", values, "userGender");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserGenderBetween(String value1, String value2) {
            addCriterion("USER_GENDER between", value1, value2, "userGender");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserGenderNotBetween(String value1, String value2) {
            addCriterion("USER_GENDER not between", value1, value2, "userGender");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserEmailIsNull() {
            addCriterion("user_email is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUserEmailIsNotNull() {
            addCriterion("user_email is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserEmailEqualTo(String value) {
            addCriterion("user_email =", value, "userEmail");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserEmailNotEqualTo(String value) {
            addCriterion("user_email <>", value, "userEmail");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserEmailGreaterThan(String value) {
            addCriterion("user_email >", value, "userEmail");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserEmailGreaterThanOrEqualTo(String value) {
            addCriterion("user_email >=", value, "userEmail");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserEmailLessThan(String value) {
            addCriterion("user_email <", value, "userEmail");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserEmailLessThanOrEqualTo(String value) {
            addCriterion("user_email <=", value, "userEmail");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserEmailLike(String value) {
            addCriterion("user_email like", value, "userEmail");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUserEmailNotLike(String value) {
            addCriterion("user_email not like", value, "userEmail");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserEmailIn(List<String> values) {
            addCriterion("user_email in", values, "userEmail");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUserEmailNotIn(List<String> values) {
            addCriterion("user_email not in", values, "userEmail");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserEmailBetween(String value1, String value2) {
            addCriterion("user_email between", value1, value2, "userEmail");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUserEmailNotBetween(String value1, String value2) {
            addCriterion("user_email not between", value1, value2, "userEmail");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andCreateDatetimeIsNull() {
            addCriterion("CREATE_DATETIME is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andCreateDatetimeIsNotNull() {
            addCriterion("CREATE_DATETIME is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreateDatetimeEqualTo(Date value) {
            addCriterion("CREATE_DATETIME =", value, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreateDatetimeNotEqualTo(Date value) {
            addCriterion("CREATE_DATETIME <>", value, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreateDatetimeGreaterThan(Date value) {
            addCriterion("CREATE_DATETIME >", value, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreateDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATE_DATETIME >=", value, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreateDatetimeLessThan(Date value) {
            addCriterion("CREATE_DATETIME <", value, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andCreateDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("CREATE_DATETIME <=", value, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andCreateDatetimeIn(List<Date> values) {
            addCriterion("CREATE_DATETIME in", values, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andCreateDatetimeNotIn(List<Date> values) {
            addCriterion("CREATE_DATETIME not in", values, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andCreateDatetimeBetween(Date value1, Date value2) {
            addCriterion("CREATE_DATETIME between", value1, value2, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andCreateDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("CREATE_DATETIME not between", value1, value2, "createDatetime");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUpdateDatetimeIsNull() {
            addCriterion("UPDATE_DATETIME is null");
            return (Criteria) this;
        }
        
        /**
         * @return Criteria
         */
        public Criteria andUpdateDatetimeIsNotNull() {
            addCriterion("UPDATE_DATETIME is not null");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDatetimeEqualTo(Date value) {
            addCriterion("UPDATE_DATETIME =", value, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDatetimeNotEqualTo(Date value) {
            addCriterion("UPDATE_DATETIME <>", value, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDatetimeGreaterThan(Date value) {
            addCriterion("UPDATE_DATETIME >", value, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("UPDATE_DATETIME >=", value, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDatetimeLessThan(Date value) {
            addCriterion("UPDATE_DATETIME <", value, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value value
         * @return Criteria
         */
        public Criteria andUpdateDatetimeLessThanOrEqualTo(Date value) {
            addCriterion("UPDATE_DATETIME <=", value, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUpdateDatetimeIn(List<Date> values) {
            addCriterion("UPDATE_DATETIME in", values, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param values values
         * @return Criteria
         */
        public Criteria andUpdateDatetimeNotIn(List<Date> values) {
            addCriterion("UPDATE_DATETIME not in", values, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUpdateDatetimeBetween(Date value1, Date value2) {
            addCriterion("UPDATE_DATETIME between", value1, value2, "updateDatetime");
            return (Criteria) this;
        }
        
        /**
         * @param value1 value1
         * @param value2 value2
         * @return Criteria
         */
        public Criteria andUpdateDatetimeNotBetween(Date value1, Date value2) {
            addCriterion("UPDATE_DATETIME not between", value1, value2, "updateDatetime");
            return (Criteria) this;
        }
    }
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/

    /**
     * @project:用户管理模块
     * @description:null
     * @version 1.0.0
     * @errorcode
     *            错误码: 错误描述
     * @author
     *         <li>2019-01-23 superFei Create 1.0
     */
    public static class Criteria extends GeneratedCriteria {


        
        /**
         * 构造方法 
         */
        protected Criteria() {
            super();
        }
    }
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/

    /**
     * @project:用户管理模块
     * @description:null
     * @version 1.0.0
     * @errorcode
     *            错误码: 错误描述
     * @author
     *         <li>2019-01-23 superFei Create 1.0
     */
    public static class Criterion {

        
        /**condition */
        private String condition;
        
        /**value */
        private Object value;
        
        /**secondValue */
        private Object secondValue;
        
        /**noValue */
        private boolean noValue;
        
        /**singleValue */
        private boolean singleValue;
        
        /**betweenValue */
        private boolean betweenValue;
        
        /**listValue */
        private boolean listValue;
        
        /**typeHandler */
        private String typeHandler;

        
        /**
         * @return String
         */
        public String getCondition() {
            return condition;
        }
        
        /**
         * @return Object
         */
        public Object getValue() {
            return value;
        }
        
        /**
         * @return Object
         */
        public Object getSecondValue() {
            return secondValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isNoValue() {
            return noValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isSingleValue() {
            return singleValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isBetweenValue() {
            return betweenValue;
        }
        
        /**
         * @return boolean
         */
        public boolean isListValue() {
            return listValue;
        }
        
        /**
         * @return String
         */
        public String getTypeHandler() {
            return typeHandler;
        }
        
        /**
         * 构造方法 
         * @param condition condition
         */
        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         * @param typeHandler typeHandler
         */
        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         */
        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         * @param secondValue secondValue
         * @param typeHandler typeHandler
         */
        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }
        
        /**
         * 构造方法 
         * @param condition condition
         * @param value value
         * @param secondValue secondValue
         */
        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/