package com.fenglun.shunfengche.entity.user;

import java.util.Date;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
public class SfcRoleInfo {

    
    /**编号 */
    private String id;
    
    /**角色编码 */
    private String roleCode;
    
    /**角色名称 */
    private String roleName;
    
    /**角色状态,0:停用,1:启用 */
    private String roleStatus;
    
    /**角色描述 */
    private String roleDesc;
    
    /**创建时间 */
    private Date createDatetime;
    
    /**修改时间 */
    private Date updateDatetime;

    
    /**
     * @return 编号
     */
    public String getId() {
        return id;
    }
    
    /**
     * @param id 编号
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }
    
    /**
     * @return 角色编码
     */
    public String getRoleCode() {
        return roleCode;
    }
    
    /**
     * @param roleCode 角色编码
     */
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode == null ? null : roleCode.trim();
    }
    
    /**
     * @return 角色名称
     */
    public String getRoleName() {
        return roleName;
    }
    
    /**
     * @param roleName 角色名称
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }
    
    /**
     * @return 角色状态,0:停用,1:启用
     */
    public String getRoleStatus() {
        return roleStatus;
    }
    
    /**
     * @param roleStatus 角色状态,0:停用,1:启用
     */
    public void setRoleStatus(String roleStatus) {
        this.roleStatus = roleStatus == null ? null : roleStatus.trim();
    }
    
    /**
     * @return 角色描述
     */
    public String getRoleDesc() {
        return roleDesc;
    }
    
    /**
     * @param roleDesc 角色描述
     */
    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc == null ? null : roleDesc.trim();
    }
    
    /**
     * @return 创建时间
     */
    public Date getCreateDatetime() {
        return createDatetime == null ? null : (Date) createDatetime.clone();
    }
    
    /**
     * @param createDatetime 创建时间
     */
    public void setCreateDatetime(Date createDatetime) {
        this.createDatetime = createDatetime == null ? null : (Date) createDatetime.clone();
    }
    
    /**
     * @return 修改时间
     */
    public Date getUpdateDatetime() {
        return updateDatetime == null ? null : (Date) updateDatetime.clone();
    }
    
    /**
     * @param updateDatetime 修改时间
     */
    public void setUpdateDatetime(Date updateDatetime) {
        this.updateDatetime = updateDatetime == null ? null : (Date) updateDatetime.clone();
    }
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/