package com.fenglun.shunfengche.pojo.base;

import com.fenglun.shunfengche.enums.ShunFengCheErrorEnums;

/**
 * @project: 顺风车
 * @description: 响应数据体
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2020-07-03 825338623@qq.com Create 1.0
 * @copyright ©2019-2020 顺风车，版权所有。
 */
public class ResponseData<T> extends BaseResponse
{

    /**
     * 数据
     */
    private T data;
    
    public ResponseData()
    {
        super();
    }

	public ResponseData(ShunFengCheErrorEnums code)
    {
        super(code);
    }

	public ResponseData(ShunFengCheErrorEnums code, T data)
    {
        super(code);
        this.data = data;
    }
    
    public ResponseData(String code, String msg)
    {
        super(code,msg);
    }

    /**
     * 对外开放基础响应体已供调用，可用于增、删、改接口操作
     */
	public static BaseResponse out(ShunFengCheErrorEnums code)
    {
        return new BaseResponse(code);
    }
    
    /**
     * 对外开放基础响应体已供调用，可用于增、删、改接口操作
     */
    public static BaseResponse out(String code, String msg)
    {
        return new BaseResponse(code, msg);
    }

    /**
     * 对外开放数据响应体已供调用，可用于查询数据实用，引用了范型设计，支持各种数据类型
     */
	public static <T> ResponseData<T> out(ShunFengCheErrorEnums code, T data)
    {
        return new ResponseData<>(code, data);
    }

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
    
}
