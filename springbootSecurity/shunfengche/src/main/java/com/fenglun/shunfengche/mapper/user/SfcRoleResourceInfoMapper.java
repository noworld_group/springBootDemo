package com.fenglun.shunfengche.mapper.user;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.fenglun.shunfengche.entity.user.SfcRoleResourceInfo;
import com.fenglun.shunfengche.entity.user.SfcRoleResourceInfoExample;

/**
 * @project:用户管理模块
 * @description:null
 * @version 1.0.0
 * @errorcode
 *            错误码: 错误描述
 * @author
 *         <li>2019-01-23 superFei Create 1.0
 */
@Mapper
public interface SfcRoleResourceInfoMapper {
    
    /**
     * @param example example
     * @return int
     */
    int countByExample(SfcRoleResourceInfoExample example);

    
    /**
     * @param example example
     * @return int
     */
    int deleteByExample(SfcRoleResourceInfoExample example);

    
    /**
     * @param id id
     * @return int
     */
    int deleteByPrimaryKey(String id);

    
    /**
     * @param record record
     * @return int
     */
    int insert(SfcRoleResourceInfo record);

    
    /**
     * @param record record
     * @return int
     */
    int insertSelective(SfcRoleResourceInfo record);

    
    /**
     * @param example example
     * @return List<SfcRoleResourceInfo>
     */
    List<SfcRoleResourceInfo> selectByExample(SfcRoleResourceInfoExample example);

    
    /**
     * @param id id
     * @return SfcRoleResourceInfo
     */
    SfcRoleResourceInfo selectByPrimaryKey(String id);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExampleSelective(@Param("record") SfcRoleResourceInfo record, @Param("example") SfcRoleResourceInfoExample example);

    
    /**
     * @param record record
     * @param example example
     * @return int
     */
    int updateByExample(@Param("record") SfcRoleResourceInfo record, @Param("example") SfcRoleResourceInfoExample example);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKeySelective(SfcRoleResourceInfo record);

    
    /**
     * @param record record
     * @return int
     */
    int updateByPrimaryKey(SfcRoleResourceInfo record);
}
/**
 * CHANGE HISTORY
* M1 2020-08-23 superFei Create
*/