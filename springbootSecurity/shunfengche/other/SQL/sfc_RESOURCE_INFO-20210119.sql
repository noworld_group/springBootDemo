/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50729
Source Host           : localhost:3306
Source Database       : shunfengche

Target Server Type    : MYSQL
Target Server Version : 50729
File Encoding         : 65001

Date: 2021-01-19 17:51:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sfc_resource_info`
-- ----------------------------
DROP TABLE IF EXISTS `sfc_resource_info`;
CREATE TABLE `sfc_resource_info` (
  `ID` varchar(36) NOT NULL COMMENT '编号',
  `RESOURCE_NAME` varchar(64) NOT NULL COMMENT '资源名称',
  `RESOURCE_ICON` varchar(100) DEFAULT NULL COMMENT '资源图标',
  `RESOURCE_TYPE` varchar(2) NOT NULL COMMENT '资源类型',
  `ORDER_NUM` int(2) NOT NULL COMMENT '排序',
  `PERM_CODE` varchar(100) NOT NULL COMMENT '权限编码',
  `PARENT_ID` varchar(36) NOT NULL COMMENT '父级编号，顶级为-1',
  `RESOURCE_LEVEL` int(2) NOT NULL COMMENT '资源级别',
  `RESOURCE_URL` varchar(100) DEFAULT NULL COMMENT '资源链接',
  `RESOURCE_STATUS` varchar(2) NOT NULL COMMENT '资源状态,0:停用，1:启用',
  `CREATE_DATETIME` timestamp NOT NULL DEFAULT '2021-01-19 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_DATETIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sfc_resource_info
-- ----------------------------

-- ----------------------------
-- Table structure for `sfc_role_info`
-- ----------------------------
DROP TABLE IF EXISTS `sfc_role_info`;
CREATE TABLE `sfc_role_info` (
  `ID` varchar(36) NOT NULL COMMENT '编号',
  `ROLE_CODE` varchar(50) NOT NULL COMMENT '角色编码',
  `ROLE_NAME` varchar(50) NOT NULL COMMENT '角色名称',
  `ROLE_STATUS` varchar(2) NOT NULL COMMENT '角色状态,0:停用,1:启用',
  `ROLE_DESC` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `CREATE_DATETIME` timestamp NOT NULL DEFAULT '2021-01-19 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_DATETIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sfc_role_info
-- ----------------------------

-- ----------------------------
-- Table structure for `sfc_role_resource_info`
-- ----------------------------
DROP TABLE IF EXISTS `sfc_role_resource_info`;
CREATE TABLE `sfc_role_resource_info` (
  `ID` varchar(36) NOT NULL COMMENT '编号',
  `ROLE_ID` varchar(36) NOT NULL COMMENT '角色编号',
  `RESOURCE_ID` varchar(36) NOT NULL COMMENT '资源编号',
  `CREATE_DATETIME` timestamp NOT NULL DEFAULT '2021-01-19 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_DATETIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sfc_role_resource_info
-- ----------------------------

-- ----------------------------
-- Table structure for `sfc_user_company_info`
-- ----------------------------
DROP TABLE IF EXISTS `sfc_user_company_info`;
CREATE TABLE `sfc_user_company_info` (
  `id` varchar(36) NOT NULL COMMENT '编号',
  `user_id` varchar(36) NOT NULL COMMENT '用户编号',
  `COMPANY_ID` varchar(36) NOT NULL COMMENT '公司编号',
  `COMPANY_CARD_1` varchar(100) DEFAULT NULL COMMENT '公司证明1',
  `COMPANY_CARD_2` varchar(100) DEFAULT NULL COMMENT '公司证明2',
  `COMPANY_CARD_3` varchar(100) DEFAULT NULL COMMENT '公司证明3',
  `company_email` varchar(100) DEFAULT NULL COMMENT '公司邮箱账号',
  `JOB_NUMBER` varchar(50) DEFAULT NULL COMMENT '工号',
  `CREATE_DATETIME` timestamp NOT NULL DEFAULT '2021-01-19 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_DATETIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sfc_user_company_info
-- ----------------------------

-- ----------------------------
-- Table structure for `sfc_user_expand_info`
-- ----------------------------
DROP TABLE IF EXISTS `sfc_user_expand_info`;
CREATE TABLE `sfc_user_expand_info` (
  `id` varchar(36) NOT NULL COMMENT '编号',
  `user_id` varchar(36) NOT NULL COMMENT '用户编号',
  `USER_ID_CARD_1` varchar(100) DEFAULT NULL COMMENT '身份证正面',
  `USER_ID_CARD_2` varchar(100) DEFAULT NULL COMMENT '身份证反面',
  `CREATE_DATETIME` timestamp NOT NULL DEFAULT '2021-01-19 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_DATETIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sfc_user_expand_info
-- ----------------------------

-- ----------------------------
-- Table structure for `sfc_user_info`
-- ----------------------------
DROP TABLE IF EXISTS `sfc_user_info`;
CREATE TABLE `sfc_user_info` (
  `id` varchar(36) NOT NULL COMMENT '编号',
  `USER_NAME` varchar(100) NOT NULL COMMENT '用户名',
  `USER_PASSWORD` varchar(128) NOT NULL COMMENT '密码',
  `REAL_NAME` varchar(100) DEFAULT NULL COMMENT '真实姓名，管理人员/机构可以不填',
  `USER_STATUS` varchar(2) NOT NULL COMMENT '用户状态，1:正常，2:违规停用，3:离职停用',
  `LOGIN_TIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最近登录时间',
  `USER_LOGO` varchar(100) DEFAULT NULL COMMENT '用户头像',
  `USER_TEL` varchar(20) DEFAULT NULL COMMENT '用户电话',
  `CARD_ID_NUM` varchar(64) DEFAULT NULL COMMENT '身份证号',
  `USER_GENDER` varchar(2) DEFAULT NULL COMMENT '用户性别',
  `user_email` varchar(100) DEFAULT NULL COMMENT '用户邮箱',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述信息，停用原因等信息都保存在此处',
  `CREATE_DATETIME` timestamp NOT NULL DEFAULT '2021-01-19 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `UPDATE_DATETIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sfc_user_info
-- ----------------------------
INSERT INTO `sfc_user_info` VALUES ('1', 'admin', '$2a$10$htdlj6041WLWRlOpiICN8.aS0daVzQDsLw/qcf43CRMuVo5fFY4j.', '超级管理员', '1', '2021-01-19 17:48:27', null, null, null, null, null, null, '2021-01-19 00:00:00', null);

-- ----------------------------
-- Table structure for `sfc_user_role_info`
-- ----------------------------
DROP TABLE IF EXISTS `sfc_user_role_info`;
CREATE TABLE `sfc_user_role_info` (
  `ID` varchar(36) NOT NULL COMMENT '编号',
  `ROLE_ID` varchar(36) NOT NULL COMMENT '角色编号',
  `USER_ID` varchar(36) NOT NULL COMMENT '用户编号',
  `CREATE_DATETIME` timestamp NOT NULL DEFAULT '2021-01-19 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_DATETIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sfc_user_role_info
-- ----------------------------
